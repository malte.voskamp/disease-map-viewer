# Disease Map Viewer

Follow the steps to install an instance of the Disease map viewer. After the installation you will be able to explore an exemplary text mined Cystic Fibrosis Disease map within the viewer. Naturally, you will also be able to view your own text mined data with the viewer and/or you can text mine plain textual data with the given KNIME text mining algorithm.

## Installation 

### 1. Installation CandActBase

To create your own instance of the disease map viewer, you need a working basic [CandActBase](https://candactcftr.ams.med.uni-goettingen.de/) instance.

Please follow the instructions on the [CandActBase Gitlab-Wiki page](https://gitlab.gwdg.de/mnieter1/CandActBase/-/wikis/home) to install your instance.

It is not necessary to include any database related data into the instance, you just need the basic CandActBase framework and the Grails Server structure.


### 2. Include Viewer by adding and exchanging all files in the CandActBase Differences Folder

To include the Disease map into your CandActBase instance simply include or overwrite the files from the CandActBase_differences folder into the directory structure.

Make sure to include and install all dependencies.


## Usage 

### 3. Start the disease map viewer

After you included the Disease Map Viewer into your CandActBase instance, start the instance by executing the following line in your terminal after you set your working directory in the respective CandActBase folder: `grails run-app --port=8888`.

Your Disease map viewer should now be accessible by the Url 'http://localhost:8888/Cytoscape'.

You should be able to see an exemplary text mined Disease Map for Cystic Fibrosis, and all functions described in the publication should be usable for you.

### 4. Functionality of the Viewer

#### Browse your datasets with the Viewer
With the cytoscape instance you can explore your dataset with moving and zooming. It is possible to move singular genes and proteins, but also whole compartments with the viewer. The edges move accordingly.


#### Filter the edges
Another way to get an understanding of your data is to filter the edges depending on multiple categories.

![image.png](./figures/filtering_options.png)

You can filter your edges depending on the categorization of the verbs from the text mining algorithim (first block of filtering options), the annotation of edges in the review process (second block) or you can choose to display only edges where the text mining categorized all the verbs from the respective text to the same, coherent biological functionality (only activating, only inhibitng etc.).

The first picture shows the map with all edges visible, while the second picture only shows edges that have been marked as "accepted" in the review process and have been categorized coherent.

![image.png](./figures/all_filtering_on.png)
![image.png](./figures/filtering_edges.png)

#### inspect single edges

If you want to inspect the textual data behind a single edge more in detail, simply click on one edge. You then can see which sentences have been used to identify the edge, with the verbs being marked in red that have been used to categorize the edge.

![image.png](./figures/CFTR_AMP_edge.png)

For the above example the interaction between CFTR and AMP has been marked "accepted" in the review process. For the second publication where the interactions has been identified in, the abstract has been loaded and displayed. For this publication, the identifying sentence has been marked in yellow and the verbs in red.

#### review process

To review the edges you can iterate through all edges and annotate them with the buttons on the right-bottom corner. When you assign a review status to an interaction, it will be displayed when inspecting an edge (see previous figure).

![image.png](./figures/review_buttons.png)

To download the data with all the updated review status, you can click the button "Download data".

![image.png](./figures/download_data.png) 


You can then choose if you want to download the current map object as a JSON (this includes the current review status) or download a simplistic csv file to look at the review data in a more simpler way. An example of the datastructure of the CSV file is given with the file 'data/diseasemap_review.csv'.
daten herunterladen

#### cytic fibrosis timeline as an additional example

To show how text mining can be used to create knowledge from plain textual data, we created a cystic fibrosis timeline, which displayed the differences of the CF science in previous decades. To do that, we downloaded abstracts with the downloader 'get_PubMed_abstracts' and specified the parameters to only include publications from certain 5 year timeframes. We then text mined this textual data and created disease maps for every timeframe. Theses different disease maps can be viewed also in your disease map instance, by clicking on the bottom-left buttons:

![image.png](./figures/timeline_buttons.png) 

Below are two examples of the differences of disease maps create from literatue published in the timeframes before 1990 and between 2010 - 2015:

Disease map from literature before 1990:

![image.png](./figures/timeframe_1990.png) 

Disease map from literature between 2010 and 2015:

![image.png](./figures/timeframe_2015.png) 

When you click through the different timeframes you can estimate how much more research on Cystic Fibrosis has been done compared to previous timeframes. 

Further research regarding research progress and complexity can be done using text mining and disease maps.



### 5. Parse or create your own text mined disease map

To include your own text mining data or create a fresh disease map from text mining plain textual data, follow either step 5.1 if you have alreade text mined datasets or step 5.2 if you want to text mine new textual data from scratch using our own text mining workflow written in KNIME.

#### 5.1 Parsing your own text mining data

To bring your text mined data into the needed JSON format the pyhton parser 'cytoscapeJSON.ipynp' can be used, it is written as a jupyter notebook to ensure accessibility.

You can easily alter and execute the Jupyter notebook in their [Web Application](https://jupyter.org/), with no further installation needed.

Simply exchange the files in the python script with your text mining dataset and make sure to have the same data format. Afterwards the script will parse the data and creata a JSON file that can be included in the viewer (described in step 6).

As an input you would need a csv file similar to the node_to_node_network.csv file (Moreover, files with localisations and verificational interactions can be added, have to be in the csv format similar to allnode_with_condensend_location.csv [for location] and verification_reactions.json [for verification]).

Then the output as JSON (similar to cytoscape_JSON.json) can be saved in the grails-app/assets/Javascripts/ folder and can be refered to in the grails-app/assets/Javascripts/cytoscape.js, which functions as the main JavaScript file for the viewer.

#### 5.2 Text mining plain text with our KNIME workflow

In this project we include the text mining workflow we designed in KNIME, with which we exemplary created the recreation of a Cystic Fibrosis Disease Map and a timeline for the change of focus in Cystic Fibrosis research.

The text mining algorithm can be found with the filename textmining_schleife.knime.

With this KNIME workflow you can text mine your own textual data, given its the fitting input format, or you can use our abstrat downloader get_PubMed_abstracts/, where you can specifiy parameters for a PubMed search and use this as a dataset for the text mining algorithm.


### 6. View text mined data in the Disease Map Viewer

To view the parsed JSON files of text mined data in the viewer, add the file into the 'grails-app/assets/javascripts/' folder and name the JSON file as a JS variable.

![image.png](./figures/variable_name_JSON.png)

You can then reference the variable in the grails-app/assets/javascripts/cytoscape.js file in Line 42:
 

![image.png](./figures/JSON_reference.png)

After you restart the instanc (as described in step 3), your data should be visible as a disease map in the viewer.
