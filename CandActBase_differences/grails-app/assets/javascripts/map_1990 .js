var map_1990 = [
  {
    "data": {
      "id": "c0",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Extracellular",
      "class": "compartment",
      "parent": ""
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c1",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "PM",
      "class": "compartment",
      "parent": "c0"
    },
    "position": {
      "x": 50,
      "y": 50
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c2",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Cytosol",
      "class": "compartment",
      "parent": "c0"
    },
    "position": {
      "x": 50,
      "y": 105
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c3",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Golgi",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 275,
      "y": 150
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c4",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "ER",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 275,
      "y": 450
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c5",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Nucleus",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 200,
      "y": 740
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c6",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Mitochondrion",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 80,
      "y": 550
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c7",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Endosome",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 450,
      "y": 100
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c8",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "undefined",
      "class": "compartment",
      "parent": ""
    },
    "position": {
      "x": 1000,
      "y": 0
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "AMP",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "AMP",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 150,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "COPI",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "COPI",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 250,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ATP",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ATP",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 350,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CFTR",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "CFTR",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1100,
      "y": 70
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "Cl-",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "Cl-",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 70
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CFTRAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AMP",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "stimulate increase",
          "categorical_verbs": "neutral activate",
          "sentences": "Agents that increase intracellular cAMP stimulated 125I efflux in PLJ-CFTR clones but not PLJ clones.",
          "PMID": 1698126
        },
        "1": {
          "Verbs": "indicate confer",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our findings indicate that expression of the normal CFTR gene confers cAMP-dependent Cl channel regulation on CF epithelial cells.",
          "PMID": 1698126
        },
        "2": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "3": {
          "Verbs": "demonstrate is be phosporylate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Here we demonstrate that CFTR is a membrane-associated glycoprotein  that can be phosporylated in vitro by cAMP-dependent protein kinase.",
          "PMID": 1699127
        },
        "4": {
          "Verbs": "include mutate gives",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Important examples include the dystrophin protein which, when mutated, gives rise to either Duchenne or Becker muscular dystrophy [Koenig, M., Hoffman, E. P., Bertelson, C. J., Monaco, A. P., Feener, C. and Kunkel, L. M. (1987) Cell 50, 509-517; Monaco, A. P., Bertelson, C. J., Liechti-Gallati, S. & Kunkel, L. M. (1988) Genomics 2, 90-95; Koenig, M., Monaco, A. P. & Kunkel, L. M. (1988) Cell 53, 219-228] and the cystic fibrosis transmembrane conductance regulator (CFTR) [Riordan, J. R., Rommens, J. M., Kerem, B.-S., Alon, N., Rozmahel, R., Grzelczak, Z., Zielenski, J., Lok, S., Plavsic, N., Chou, J.-L., Drumm, M. L., Ianuzzi, M. C., Collins, F. S. & Tsui, L.-C. (1989) Science 245, 1066-1073].",
          "PMID": 2269271
        },
        "5": {
          "Verbs": "found mutate are be conserv lie is believe form",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "The mutated amino acids are found to be conserved in both rodents and amphibians and lie in a region of CFTR that is believed to form a channel in the membrane.",
          "PMID": 2344617
        },
        "6": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "This study analyses distribution patterns of the delta F508 mutation of the\ncystic fibrosis transmembrane conductance regulator gene (CFTR) gene and the\ncystic fibrosis (CF)-linked marker loci MET, D7S23, D7S399, and D7S8 in a sample \nof 167 (116 complete) CF families from Bohemia and Moravia (Czechoslovakia).",
          "PMID": 2210755
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AMP",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "stimulate increase",
          "categorical_verbs": "neutral activate",
          "sentences": "Agents that increase intracellular cAMP stimulated 125I efflux in PLJ-CFTR clones but not PLJ clones.",
          "PMID": 1698126
        },
        "1": {
          "Verbs": "indicate confer",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our findings indicate that expression of the normal CFTR gene confers cAMP-dependent Cl channel regulation on CF epithelial cells.",
          "PMID": 1698126
        },
        "2": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "3": {
          "Verbs": "demonstrate is be phosporylate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Here we demonstrate that CFTR is a membrane-associated glycoprotein  that can be phosporylated in vitro by cAMP-dependent protein kinase.",
          "PMID": 1699127
        },
        "4": {
          "Verbs": "include mutate gives",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Important examples include the dystrophin protein which, when mutated, gives rise to either Duchenne or Becker muscular dystrophy [Koenig, M., Hoffman, E. P., Bertelson, C. J., Monaco, A. P., Feener, C. and Kunkel, L. M. (1987) Cell 50, 509-517; Monaco, A. P., Bertelson, C. J., Liechti-Gallati, S. & Kunkel, L. M. (1988) Genomics 2, 90-95; Koenig, M., Monaco, A. P. & Kunkel, L. M. (1988) Cell 53, 219-228] and the cystic fibrosis transmembrane conductance regulator (CFTR) [Riordan, J. R., Rommens, J. M., Kerem, B.-S., Alon, N., Rozmahel, R., Grzelczak, Z., Zielenski, J., Lok, S., Plavsic, N., Chou, J.-L., Drumm, M. L., Ianuzzi, M. C., Collins, F. S. & Tsui, L.-C. (1989) Science 245, 1066-1073].",
          "PMID": 2269271
        },
        "5": {
          "Verbs": "found mutate are be conserv lie is believe form",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "The mutated amino acids are found to be conserved in both rodents and amphibians and lie in a region of CFTR that is believed to form a channel in the membrane.",
          "PMID": 2344617
        },
        "6": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "This study analyses distribution patterns of the delta F508 mutation of the\ncystic fibrosis transmembrane conductance regulator gene (CFTR) gene and the\ncystic fibrosis (CF)-linked marker loci MET, D7S23, D7S399, and D7S8 in a sample \nof 167 (116 complete) CF families from Bohemia and Moravia (Czechoslovakia).",
          "PMID": 2210755
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AMP",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "stimulate increase",
          "categorical_verbs": "neutral activate",
          "sentences": "Agents that increase intracellular cAMP stimulated 125I efflux in PLJ-CFTR clones but not PLJ clones.",
          "PMID": 1698126
        },
        "1": {
          "Verbs": "indicate confer",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our findings indicate that expression of the normal CFTR gene confers cAMP-dependent Cl channel regulation on CF epithelial cells.",
          "PMID": 1698126
        },
        "2": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "3": {
          "Verbs": "demonstrate is be phosporylate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Here we demonstrate that CFTR is a membrane-associated glycoprotein  that can be phosporylated in vitro by cAMP-dependent protein kinase.",
          "PMID": 1699127
        },
        "4": {
          "Verbs": "include mutate gives",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Important examples include the dystrophin protein which, when mutated, gives rise to either Duchenne or Becker muscular dystrophy [Koenig, M., Hoffman, E. P., Bertelson, C. J., Monaco, A. P., Feener, C. and Kunkel, L. M. (1987) Cell 50, 509-517; Monaco, A. P., Bertelson, C. J., Liechti-Gallati, S. & Kunkel, L. M. (1988) Genomics 2, 90-95; Koenig, M., Monaco, A. P. & Kunkel, L. M. (1988) Cell 53, 219-228] and the cystic fibrosis transmembrane conductance regulator (CFTR) [Riordan, J. R., Rommens, J. M., Kerem, B.-S., Alon, N., Rozmahel, R., Grzelczak, Z., Zielenski, J., Lok, S., Plavsic, N., Chou, J.-L., Drumm, M. L., Ianuzzi, M. C., Collins, F. S. & Tsui, L.-C. (1989) Science 245, 1066-1073].",
          "PMID": 2269271
        },
        "5": {
          "Verbs": "found mutate are be conserv lie is believe form",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "The mutated amino acids are found to be conserved in both rodents and amphibians and lie in a region of CFTR that is believed to form a channel in the membrane.",
          "PMID": 2344617
        },
        "6": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "This study analyses distribution patterns of the delta F508 mutation of the\ncystic fibrosis transmembrane conductance regulator gene (CFTR) gene and the\ncystic fibrosis (CF)-linked marker loci MET, D7S23, D7S399, and D7S8 in a sample \nof 167 (116 complete) CF families from Bohemia and Moravia (Czechoslovakia).",
          "PMID": 2210755
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AMP",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "stimulate increase",
          "categorical_verbs": "neutral activate",
          "sentences": "Agents that increase intracellular cAMP stimulated 125I efflux in PLJ-CFTR clones but not PLJ clones.",
          "PMID": 1698126
        },
        "1": {
          "Verbs": "indicate confer",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our findings indicate that expression of the normal CFTR gene confers cAMP-dependent Cl channel regulation on CF epithelial cells.",
          "PMID": 1698126
        },
        "2": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "3": {
          "Verbs": "demonstrate is be phosporylate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Here we demonstrate that CFTR is a membrane-associated glycoprotein  that can be phosporylated in vitro by cAMP-dependent protein kinase.",
          "PMID": 1699127
        },
        "4": {
          "Verbs": "include mutate gives",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Important examples include the dystrophin protein which, when mutated, gives rise to either Duchenne or Becker muscular dystrophy [Koenig, M., Hoffman, E. P., Bertelson, C. J., Monaco, A. P., Feener, C. and Kunkel, L. M. (1987) Cell 50, 509-517; Monaco, A. P., Bertelson, C. J., Liechti-Gallati, S. & Kunkel, L. M. (1988) Genomics 2, 90-95; Koenig, M., Monaco, A. P. & Kunkel, L. M. (1988) Cell 53, 219-228] and the cystic fibrosis transmembrane conductance regulator (CFTR) [Riordan, J. R., Rommens, J. M., Kerem, B.-S., Alon, N., Rozmahel, R., Grzelczak, Z., Zielenski, J., Lok, S., Plavsic, N., Chou, J.-L., Drumm, M. L., Ianuzzi, M. C., Collins, F. S. & Tsui, L.-C. (1989) Science 245, 1066-1073].",
          "PMID": 2269271
        },
        "5": {
          "Verbs": "found mutate are be conserv lie is believe form",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "The mutated amino acids are found to be conserved in both rodents and amphibians and lie in a region of CFTR that is believed to form a channel in the membrane.",
          "PMID": 2344617
        },
        "6": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "This study analyses distribution patterns of the delta F508 mutation of the\ncystic fibrosis transmembrane conductance regulator gene (CFTR) gene and the\ncystic fibrosis (CF)-linked marker loci MET, D7S23, D7S399, and D7S8 in a sample \nof 167 (116 complete) CF families from Bohemia and Moravia (Czechoslovakia).",
          "PMID": 2210755
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AMP",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "stimulate increase",
          "categorical_verbs": "neutral activate",
          "sentences": "Agents that increase intracellular cAMP stimulated 125I efflux in PLJ-CFTR clones but not PLJ clones.",
          "PMID": 1698126
        },
        "1": {
          "Verbs": "indicate confer",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our findings indicate that expression of the normal CFTR gene confers cAMP-dependent Cl channel regulation on CF epithelial cells.",
          "PMID": 1698126
        },
        "2": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "3": {
          "Verbs": "demonstrate is be phosporylate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Here we demonstrate that CFTR is a membrane-associated glycoprotein  that can be phosporylated in vitro by cAMP-dependent protein kinase.",
          "PMID": 1699127
        },
        "4": {
          "Verbs": "include mutate gives",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Important examples include the dystrophin protein which, when mutated, gives rise to either Duchenne or Becker muscular dystrophy [Koenig, M., Hoffman, E. P., Bertelson, C. J., Monaco, A. P., Feener, C. and Kunkel, L. M. (1987) Cell 50, 509-517; Monaco, A. P., Bertelson, C. J., Liechti-Gallati, S. & Kunkel, L. M. (1988) Genomics 2, 90-95; Koenig, M., Monaco, A. P. & Kunkel, L. M. (1988) Cell 53, 219-228] and the cystic fibrosis transmembrane conductance regulator (CFTR) [Riordan, J. R., Rommens, J. M., Kerem, B.-S., Alon, N., Rozmahel, R., Grzelczak, Z., Zielenski, J., Lok, S., Plavsic, N., Chou, J.-L., Drumm, M. L., Ianuzzi, M. C., Collins, F. S. & Tsui, L.-C. (1989) Science 245, 1066-1073].",
          "PMID": 2269271
        },
        "5": {
          "Verbs": "found mutate are be conserv lie is believe form",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "The mutated amino acids are found to be conserved in both rodents and amphibians and lie in a region of CFTR that is believed to form a channel in the membrane.",
          "PMID": 2344617
        },
        "6": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "This study analyses distribution patterns of the delta F508 mutation of the\ncystic fibrosis transmembrane conductance regulator gene (CFTR) gene and the\ncystic fibrosis (CF)-linked marker loci MET, D7S23, D7S399, and D7S8 in a sample \nof 167 (116 complete) CF families from Bohemia and Moravia (Czechoslovakia).",
          "PMID": 2210755
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AMP",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "stimulate increase",
          "categorical_verbs": "neutral activate",
          "sentences": "Agents that increase intracellular cAMP stimulated 125I efflux in PLJ-CFTR clones but not PLJ clones.",
          "PMID": 1698126
        },
        "1": {
          "Verbs": "indicate confer",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our findings indicate that expression of the normal CFTR gene confers cAMP-dependent Cl channel regulation on CF epithelial cells.",
          "PMID": 1698126
        },
        "2": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "3": {
          "Verbs": "demonstrate is be phosporylate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Here we demonstrate that CFTR is a membrane-associated glycoprotein  that can be phosporylated in vitro by cAMP-dependent protein kinase.",
          "PMID": 1699127
        },
        "4": {
          "Verbs": "include mutate gives",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Important examples include the dystrophin protein which, when mutated, gives rise to either Duchenne or Becker muscular dystrophy [Koenig, M., Hoffman, E. P., Bertelson, C. J., Monaco, A. P., Feener, C. and Kunkel, L. M. (1987) Cell 50, 509-517; Monaco, A. P., Bertelson, C. J., Liechti-Gallati, S. & Kunkel, L. M. (1988) Genomics 2, 90-95; Koenig, M., Monaco, A. P. & Kunkel, L. M. (1988) Cell 53, 219-228] and the cystic fibrosis transmembrane conductance regulator (CFTR) [Riordan, J. R., Rommens, J. M., Kerem, B.-S., Alon, N., Rozmahel, R., Grzelczak, Z., Zielenski, J., Lok, S., Plavsic, N., Chou, J.-L., Drumm, M. L., Ianuzzi, M. C., Collins, F. S. & Tsui, L.-C. (1989) Science 245, 1066-1073].",
          "PMID": 2269271
        },
        "5": {
          "Verbs": "found mutate are be conserv lie is believe form",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "The mutated amino acids are found to be conserved in both rodents and amphibians and lie in a region of CFTR that is believed to form a channel in the membrane.",
          "PMID": 2344617
        },
        "6": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "This study analyses distribution patterns of the delta F508 mutation of the\ncystic fibrosis transmembrane conductance regulator gene (CFTR) gene and the\ncystic fibrosis (CF)-linked marker loci MET, D7S23, D7S399, and D7S8 in a sample \nof 167 (116 complete) CF families from Bohemia and Moravia (Czechoslovakia).",
          "PMID": 2210755
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "COPIAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "COPI",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "COPI",
      "portTarget": "AMP",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "Cl-AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Cl-",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "Cl-",
      "portTarget": "AMP",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "1": {
          "Verbs": "result were examine use",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "The resulting heterokaryons were examined for restoration of cAMP-activated Cl- transport using an optical assay of Cl- permeability.",
          "PMID": 1701965
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "Cl-AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Cl-",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "Cl-",
      "portTarget": "AMP",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "1": {
          "Verbs": "result were examine use",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "The resulting heterokaryons were examined for restoration of cAMP-activated Cl- transport using an optical assay of Cl- permeability.",
          "PMID": 1701965
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AMPCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "CFTR",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express were use transduce derive",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Amphotropic retroviruses were used to transduce a functional cystic fibrosis transmembrane conductance regulator (CFTR) cDNA into CFPAC-1, a pancreatic adenocarcinoma cell line derived from a patient with CF that stably expresses the chloride transport abnormalities characteristic of CF.",
          "PMID": 1698126
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ATPCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "CFTR",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "interact are thought exist include term fold has",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "Several functional regions are thought to exist in  the CFTR protein, including two areas for ATP-binding, termed nucleotide-binding  folds (NBFs), a regulatory (R) region that has many possible sites for phosphorylation by protein kinases A and C, and two hydrophobic regions that probably interact with cell membranes.",
          "PMID": 1695717
        },
        "1": {
          "Verbs": "cause are conserv",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Three of these mutations cause amino-acid substitutions at residues that are highly conserved among the CFTR protein, the multiple-drug-resistance proteins and ATP-binding membrane-associated transport proteins.",
          "PMID": 1695717
        },
        "2": {
          "Verbs": "reveal provide is",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "These mutations reveal a functionally important region in the CFTR protein and provide further evidence that CFTR is a member of the family of  ATP-dependent transport proteins.",
          "PMID": 1695717
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ATPCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "CFTR",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "interact are thought exist include term fold has",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "Several functional regions are thought to exist in  the CFTR protein, including two areas for ATP-binding, termed nucleotide-binding  folds (NBFs), a regulatory (R) region that has many possible sites for phosphorylation by protein kinases A and C, and two hydrophobic regions that probably interact with cell membranes.",
          "PMID": 1695717
        },
        "1": {
          "Verbs": "cause are conserv",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Three of these mutations cause amino-acid substitutions at residues that are highly conserved among the CFTR protein, the multiple-drug-resistance proteins and ATP-binding membrane-associated transport proteins.",
          "PMID": 1695717
        },
        "2": {
          "Verbs": "reveal provide is",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "These mutations reveal a functionally important region in the CFTR protein and provide further evidence that CFTR is a member of the family of  ATP-dependent transport proteins.",
          "PMID": 1695717
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ATPCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "CFTR",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "interact are thought exist include term fold has",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "Several functional regions are thought to exist in  the CFTR protein, including two areas for ATP-binding, termed nucleotide-binding  folds (NBFs), a regulatory (R) region that has many possible sites for phosphorylation by protein kinases A and C, and two hydrophobic regions that probably interact with cell membranes.",
          "PMID": 1695717
        },
        "1": {
          "Verbs": "cause are conserv",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Three of these mutations cause amino-acid substitutions at residues that are highly conserved among the CFTR protein, the multiple-drug-resistance proteins and ATP-binding membrane-associated transport proteins.",
          "PMID": 1695717
        },
        "2": {
          "Verbs": "reveal provide is",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "These mutations reveal a functionally important region in the CFTR protein and provide further evidence that CFTR is a member of the family of  ATP-dependent transport proteins.",
          "PMID": 1695717
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRCOPI",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "COPI",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "COPI",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "Cl-COPI",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Cl-",
      "target": "COPI",
      "bendPointPositions": [],
      "portSource": "Cl-",
      "portTarget": "COPI",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AMPCl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "Cl-",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "is manifest result",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Cystic fibrosis (CF) is a common lethal genetic disease that manifests itself in  airway and other epithelial cells as defective chloride ion absorption and secretion, resulting at least in part from a defect in a cyclic AMP-regulated, outwardly-rectifying Cl- channel in the apical surface.",
          "PMID": 1699127
        },
        "1": {
          "Verbs": "underly has been propose involve",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "The biochemical defect that underlies the genetic disorder cystic fibrosis (CF) has been proposed to involve an altered regulation of epithelial Cl- permeability by agents such as adenosine 3',5'-cyclic monophosphate (cAMP).",
          "PMID": 1701965
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AMPCl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "Cl-",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "is manifest result",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Cystic fibrosis (CF) is a common lethal genetic disease that manifests itself in  airway and other epithelial cells as defective chloride ion absorption and secretion, resulting at least in part from a defect in a cyclic AMP-regulated, outwardly-rectifying Cl- channel in the apical surface.",
          "PMID": 1699127
        },
        "1": {
          "Verbs": "underly has been propose involve",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "The biochemical defect that underlies the genetic disorder cystic fibrosis (CF) has been proposed to involve an altered regulation of epithelial Cl- permeability by agents such as adenosine 3',5'-cyclic monophosphate (cAMP).",
          "PMID": 1701965
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRCl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "Cl-",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "1": {
          "Verbs": "correct",
          "categorical_verbs": "undefined",
          "sentences": "Expression of CFTR, but not of a mutant form of CFTR (delta F508), corrected the Cl- channel defect.",
          "PMID": 1699126
        },
        "2": {
          "Verbs": "demonstrates is",
          "categorical_verbs": "undefined undefined",
          "sentences": "Correction of the phenotypic defect demonstrates a causal relationship between mutations in the CFTR gene and  defective Cl- transport which is the hallmark of the disease.",
          "PMID": 1699126
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRCl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "Cl-",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "1": {
          "Verbs": "correct",
          "categorical_verbs": "undefined",
          "sentences": "Expression of CFTR, but not of a mutant form of CFTR (delta F508), corrected the Cl- channel defect.",
          "PMID": 1699126
        },
        "2": {
          "Verbs": "demonstrates is",
          "categorical_verbs": "undefined undefined",
          "sentences": "Correction of the phenotypic defect demonstrates a causal relationship between mutations in the CFTR gene and  defective Cl- transport which is the hallmark of the disease.",
          "PMID": 1699126
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRCl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "Cl-",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "1": {
          "Verbs": "correct",
          "categorical_verbs": "undefined",
          "sentences": "Expression of CFTR, but not of a mutant form of CFTR (delta F508), corrected the Cl- channel defect.",
          "PMID": 1699126
        },
        "2": {
          "Verbs": "demonstrates is",
          "categorical_verbs": "undefined undefined",
          "sentences": "Correction of the phenotypic defect demonstrates a causal relationship between mutations in the CFTR gene and  defective Cl- transport which is the hallmark of the disease.",
          "PMID": 1699126
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AMP",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "stimulate increase",
          "categorical_verbs": "neutral activate",
          "sentences": "Agents that increase intracellular cAMP stimulated 125I efflux in PLJ-CFTR clones but not PLJ clones.",
          "PMID": 1698126
        },
        "1": {
          "Verbs": "indicate confer",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our findings indicate that expression of the normal CFTR gene confers cAMP-dependent Cl channel regulation on CF epithelial cells.",
          "PMID": 1698126
        },
        "2": {
          "Verbs": "express was culture assess use",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) was expressed in cultured cystic fibrosis airway epithelial cells and Cl- channel activation assessed in single cells using a fluorescence microscopic assay and the patch-clamp technique.",
          "PMID": 1699126
        },
        "3": {
          "Verbs": "demonstrate is be phosporylate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Here we demonstrate that CFTR is a membrane-associated glycoprotein  that can be phosporylated in vitro by cAMP-dependent protein kinase.",
          "PMID": 1699127
        },
        "4": {
          "Verbs": "include mutate gives",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Important examples include the dystrophin protein which, when mutated, gives rise to either Duchenne or Becker muscular dystrophy [Koenig, M., Hoffman, E. P., Bertelson, C. J., Monaco, A. P., Feener, C. and Kunkel, L. M. (1987) Cell 50, 509-517; Monaco, A. P., Bertelson, C. J., Liechti-Gallati, S. & Kunkel, L. M. (1988) Genomics 2, 90-95; Koenig, M., Monaco, A. P. & Kunkel, L. M. (1988) Cell 53, 219-228] and the cystic fibrosis transmembrane conductance regulator (CFTR) [Riordan, J. R., Rommens, J. M., Kerem, B.-S., Alon, N., Rozmahel, R., Grzelczak, Z., Zielenski, J., Lok, S., Plavsic, N., Chou, J.-L., Drumm, M. L., Ianuzzi, M. C., Collins, F. S. & Tsui, L.-C. (1989) Science 245, 1066-1073].",
          "PMID": 2269271
        },
        "5": {
          "Verbs": "found mutate are be conserv lie is believe form",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "The mutated amino acids are found to be conserved in both rodents and amphibians and lie in a region of CFTR that is believed to form a channel in the membrane.",
          "PMID": 2344617
        },
        "6": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "This study analyses distribution patterns of the delta F508 mutation of the\ncystic fibrosis transmembrane conductance regulator gene (CFTR) gene and the\ncystic fibrosis (CF)-linked marker loci MET, D7S23, D7S399, and D7S8 in a sample \nof 167 (116 complete) CF families from Bohemia and Moravia (Czechoslovakia).",
          "PMID": 2210755
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  }
]