window.onload = function () {
  var cy = cytoscape({

    container: document.getElementById('cy'),
  
    layout: { name: 'preset' },
    style: sbgnStylesheet(cytoscape)
      .selector(".highlighted")
        .css({
          'background-color': '#61bffc',
        })
        
        .selector(".activate")
          .css({
          "line-color":"green"
        })  
        .selector(".neutral")
        .css({
        "line-color":"blue"
      })
        .selector(".inhibit")
          .css({
          "line-color":"red"
        })
        .selector(".undefined")
        .css({
          "line-color":"gray"
        })
        .selector(".various")
        .css({
          "line-color":"brown"
        })
        .selector("edge")
          .css({
            "width":'data(width)'
          })
          .selector(".edge_highlighted")
          .css({
          'width': '10px',
          'line-color': '#61bffc'
        }),
    elements: sbgn_JSON_doubles
    
  });

  cy.edges().forEach(function(edge) {
    var edge_classes_tmp = edge.classes()
    var index_tmp = edge_classes_tmp.indexOf("unverified")
    var index_tmp_2 = edge_classes_tmp.indexOf("verified")

    if(index_tmp > -1) {
      edge_classes_tmp.splice(index_tmp,1)
    }
    
    if(index_tmp_2 > -1) {
      edge_classes_tmp.splice(index_tmp_2,1)
    }

    if(edge_classes_tmp.length > 1) {
      edge.addClass("various")
    }
  })

  //cy.toolbar()
  cy.on('tap', 'node', function(evt){
    var node = evt.target;
    console.log( 'tapped ' + node.id() );
    console.log(node)
  });


  cy.on('tap', 'edge', function(evt){
    var edge = evt.target;
    edge_click(edge)
  })
  
  function edge_click(edge) {
    let nodes = cy.$('node.highlighted'); 
    nodes.forEach(node => { 
     
      node.removeClass('highlighted');
    })

    let edges = cy.$('edge.edge_highlighted'); 
    edges.forEach(edge => { 
      
      edge.removeClass('edge_highlighted');
    })

    var accept_edge_button = document.getElementById("accept_edge")
    var decline_edge_button = document.getElementById("decline_edge")
    var further_inspetion_edge_button = document.getElementById("further_inspetion_edge")

    accept_edge_button.disabled = false
    decline_edge_button.disabled = false
    further_inspetion_edge_button.disabled = false

    var edge_data = edge.data()
    console.log(edge.classes())
    var edge_references = edge_data["references"]
    var edge_style = edge.style()
    var source_node = edge.source()
    var target_node = edge.target()

    console.log("edge")
    console.log(edge)
    source_node.addClass("highlighted")
    target_node.addClass("highlighted")
    edge.addClass("edge_highlighted")
    
    var view_div = document.getElementById("view")

    //create edge statusbar
    var status_div = document.getElementById("review_status")

    var review_status = edge_data['review_status']
    status_div.innerHTML = review_status
    status_div.className = review_status

    var start_node_div = document.getElementById("start_node_div")
    start_node_div.innerHTML = "<b>" + source_node.id() + "</b>"

    var edge_div = document.getElementById("edge_div")
    edge_div.style.color = edge_style["line-color"]

    var target_node_div = document.getElementById("target_node_div")
    target_node_div.innerHTML = "<b>" + target_node.id() + "</b>"



    var info_status = document.getElementById("info_status")

    info_status.innerHTML = "This interaction has been found in <b>" + edge_data["width"] + "</b> distinct publications."


    var info_div = document.getElementById("info_div")
    info_div.innerHTML = ""

    // get distinct PMIDS
    var pmid_distinct = []
    for(reference of Object.values(edge_references)) {
      if(!pmid_distinct.includes(reference["PMID"])) {
        pmid_distinct.push(reference["PMID"])
      }
    }

    //console.log(pmid_distinct)

    for(pmid of pmid_distinct) {
      var pmid_div = document.createElement("div")
      pmid_div.id = pmid + "_div"

      var header_div = document.createElement("div")
      header_div.innerHTML = "<br><b>" + pmid + "</b>"
      header_div.id = pmid + "_header"
      header_div.classList.add("pmid_header")
      
      var body_div = document.createElement("div")
      body_div.id = pmid + "_body"
      body_div.classList.add("pmid_body")
      

      var abstract_button = document.createElement("button")
      abstract_button.id = pmid
      abstract_button.innerHTML = "show abstract"
      abstract_button.classList.add("show")
      abstract_button.classList.add("btn")
      abstract_button.classList.add("btn-info")
  
      abstract_button.onclick = function() {
        var current_pmid = this.id
        var current_body_div = document.getElementById(current_pmid + "_body")
        var current_head_div = document.getElementById(current_pmid + "_header")

        if(this.classList.contains("show")) {
          var loading_div = document.createElement("div")
          loading_div.id = current_pmid + "_loading"
          loading_div.classList.add("loading")
          loading_div.innerHTML = "loading..."
          
          current_head_div.append(loading_div)
          
          $.ajax({
            url: "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id="+current_pmid+"&retmode=json&tool=CandActKNIME_tool&email=malte.voskamp@stud.uni-goettingen.de",
            
            success: function(result) {
              
              
              var paperinfo = result.result[current_pmid]
                    
      
              $.ajax({
              
                url: "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id="+current_pmid+"&retmode=text&rettype=abstract&tool=CandActKNIME_tool&email=malte.voskamp@stud.uni-goettingen.de",
                success: function(result2) {
              
                  console.log("URL")
                  console.log("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id="+current_pmid+"&retmode=text&rettype=abstract&tool=CandActKNIME_tool&email=malte.voskamp@stud.uni-goettingen.de")
                  
                  loading_div.remove()
                  
                  result2 = result2.split("\n").join(" ");
                  for(reference of Object.values(edge_references)) {
                    var reference_tmp = reference["sentences"]

                    if(reference["PMID"] == current_pmid) {
                      result2 = result2.replace(reference_tmp,"<span class = highlight_sentence>"+reference_tmp+"</span>")
                    
                      var verblist = reference["Verbs"].split(" ") 
                      for(verb of verblist) {
                        verb = " "+verb

                        var sentence_index = result2.indexOf(reference_tmp)
                        console.log(sentence_index)
                        if(sentence_index != -1) {
                          var sentence_end_index = sentence_index + reference_tmp.length

                          var result2_before = result2.substring(0,sentence_index)
                          var result2_after = result2.substring(sentence_end_index)
  
  
                          reference_tmp = reference_tmp.replace(verb, "<span class = highlight>" + verb + "</span>")
  
                          result2 = result2_before + reference_tmp + result2_after
                        }
                      }
                    }
                  }
                  current_body_div.innerHTML = result2
                  if(paperinfo.elocationid != "") {
                    var doi = paperinfo.elocationid.replace("doi: ","")
                    var doilink = paperinfo.elocationid.replace("doi: ", "http://doi.org/")
                    current_body_div.innerHTML += "<b> DOI: </b> <a href='" + doilink + "'>" + doi +"</a>"
                  }

              
              }, error: function(error2) {
                console.log(error2)
                reference_div.innerHTML += "</br>"
                reference_div.innerHTML += "There has been an error with loading the abstract of PubMEd ID: <b>" + current_pmid + "</b>"
              }
      
              })
                
            },error: function(error) {
              console.log(error) 
      
              current_body_div.innerHTML += "</br>"
              current_body_div.innerHTML += "There has been an error loading more Information for the Paper with the PubMed ID: <b>" + current_pmid +" </b>"
            }
          });

          this.classList.add("hide")
          this.classList.remove("show")
          this.innerHTML = "hide abstract"
        }else if (this.classList.contains("hide")) {
          current_body_div.innerHTML = ""
          
          for(reference of Object.values(edge_references)) {
            if(reference["PMID"] == current_pmid) {
              var reference_div = document.createElement("div")
              reference_div.classList.add("pmid_sentence")
              reference_div.innerHTML = reference["sentences"] + "<br><br>"
              var reference_verbs = reference["Verbs"].split(" ")
              reference_verbs.forEach(function(verb, index) {
                verb = " "+verb
                reference_div.innerHTML = reference_div.innerHTML.replace(verb,"<span class = highlight>"+verb+"</span>")
              })
              current_body_div.append(reference_div)
            }
          }

          this.classList.add("show")
          this.classList.remove("hide")
          this.innerHTML = "show abstract"
        }
      }

      pmid_div.append(header_div)
      pmid_div.append(body_div)
      header_div.append(abstract_button)
      info_div.append(pmid_div)
    }

    for(reference of Object.values(edge_references)) {
      var body_div = document.getElementById(reference["PMID"] + "_body")
      console.log(body_div.innerHTML)
      console.log(body_div.textContent)
      console.log(body_div.innerHTML.indexOf(reference["sentences"]))

      var reference_div = document.createElement("div")
      reference_div.classList.add("pmid_sentence")
      reference_div.innerHTML = reference["sentences"] + "<br><br>"
      var reference_verbs = reference["Verbs"].split(" ")
      reference_verbs.forEach(function(verb, index) {
        verb = " "+verb
        reference_div.innerHTML = reference_div.innerHTML.replace(verb,"<span class = highlight>"+verb+"</span>")
      })
      if(body_div.textContent.indexOf(reference_div.textContent) == -1) {
        body_div.append(reference_div)
      }

      
    }
    
    
    
  };

  var accept_edge_button = document.getElementById("accept_edge")

  accept_edge_button.onclick = function () {
    var selected_edge = cy.$('edge.edge_highlighted')[0]
    var selected_edge_data = selected_edge.data()
    var selected_edge_id = selected_edge_data["id"]
    console.log(selected_edge_id)
    console.log(sbgn_JSON_doubles)

    selected_edge_data['review_status'] = "accepted"

    for (var i = 0; i < sbgn_JSON_doubles.length; i++) {
      if (sbgn_JSON_doubles[i]['data']["id"] === selected_edge_id) {
        sbgn_JSON_doubles[i]['data']["review_status"] = "accepted";
        console.log(i)
        break;
      }
    }

    console.log(sbgn_JSON_doubles)

    var status_div_temp = document.getElementById("review_status")
    status_div_temp.innerHTML = "accepted"
    status_div_temp.className = "accepted";
  }
  
  var further_inspetion_edge_button = document.getElementById("further_inspetion_edge")

  further_inspetion_edge_button.onclick = function () {
    var selected_edge = cy.$('edge.edge_highlighted')[0]
    var selected_edge_data = selected_edge.data()
    var selected_edge_id = selected_edge_data["id"]

    //change status in Graph and in JSON
    selected_edge_data['review_status'] = "further_inspection"

    for (var i = 0; i < sbgn_JSON_doubles.length; i++) {
      if (sbgn_JSON_doubles[i]['data']["id"] === selected_edge_id) {
        sbgn_JSON_doubles[i]['data']["review_status"] = "further_inspection";
        console.log(i)
        break;
      }
    }

    var status_div_temp = document.getElementById("review_status")
    status_div_temp.innerHTML = "further_inspection"
    status_div_temp.className = "further_inspection"

  }

  var decline_edge_button = document.getElementById("decline_edge")

  decline_edge_button.onclick = function () {
    var selected_edge = cy.$('edge.edge_highlighted')[0]
    var selected_edge_data = selected_edge.data()
    var selected_edge_id = selected_edge_data["id"]

    //change status in Graph and in JSON
    selected_edge_data['review_status'] = "declined"

    for (var i = 0; i < sbgn_JSON_doubles.length; i++) {
      if (sbgn_JSON_doubles[i]['data']["id"] === selected_edge_id) {
        sbgn_JSON_doubles[i]['data']["review_status"] = "declined";
        console.log(i)
        break;
      }
    }


    var status_div_temp = document.getElementById("review_status")
    status_div_temp.innerHTML = "declined"
    status_div_temp.className = "declined"
  }

  var next_edge_button = document.getElementById("next_edge")

  next_edge_button.onclick = function () {
    
    let edges_to_review = cy.filter(function(element, i){
      return element.isEdge() && element.data('review_status') == "to_review";
    })
    
    let first_edge = edges_to_review[0]
    edge_click(first_edge)
    console.log(edges_to_review.length)
  
  }
  

  var download_JSON_button = document.getElementById("download_JSON")
  download_JSON_button.onclick = function() {

    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8, '
    + encodeURIComponent(JSON.stringify(sbgn_JSON_doubles)));
    element.setAttribute('download', "diseasemap.json");
  
    document.body.appendChild(element);
    element.click();
  
    document.body.removeChild(element);
  }

  var download_CSV_button = document.getElementById("download_CSV")
  download_CSV_button.onclick = function() {
    //create downloadable CSV with egde; PMID, classification and review status
    var csv_data = [["interaction_partner1", "interaction_partner2","PMID","review_status","classification"]]
    
    for (var i = 0; i < sbgn_JSON_doubles.length; i++) {
      if (sbgn_JSON_doubles[i]['group'] === "edges") {
        var edge = sbgn_JSON_doubles[i]
        var edge_data = edge["data"]
        var edge_distinct_PMIDS = []

        for(reference of Object.values(edge_data["references"])) {
          
          if(!edge_distinct_PMIDS.includes(reference["PMID"])) {
            edge_distinct_PMIDS.push(reference["PMID"])
            
          }
        }
  
        var edge_PMID_string = edge_distinct_PMIDS.join(" ")
        var edge_classification = edge["classes"]
        edge_classification.pop()
        var edge_classification_string = edge_classification.join(" ")
        var edge_array = [edge_data["source"],edge_data["target"],edge_PMID_string, edge_data["review_status"],edge_classification_string]
        csv_data.push(edge_array)
      }
    }

    console.log()
    let csv_string = "";

    csv_data.forEach(function(rowArray) {
        let row = rowArray.join(",");
        csv_string += row + "\r\n";
    });


    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/csv;charset=utf-8, '
    + encodeURIComponent(csv_string));
    element.setAttribute('download', "diseasemap_review.csv");
  
    document.body.appendChild(element);
    element.click();
  
    document.body.removeChild(element);

  }

  var checkbox_active = document.querySelector("input[id=check_active]");

  checkbox_active.addEventListener('change', function() {
    var legend_active_row = document.getElementById("legend_active_row")
    if (this.checked) {
      let edges = cy.$('edge.activate'); 
      legend_active_row.style.backgroundColor = ""
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    } else {
      let edges = cy.$('edge.activate'); 
      legend_active_row.style.backgroundColor = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    }
  });

  var checkbox_inhibit = document.querySelector("input[id=check_inhibit]");

  checkbox_inhibit.addEventListener('change', function() {
    var legend_inhibit_row = document.getElementById("legend_inhibit_row")
    if (this.checked) {
      let edges = cy.$('edge.inhibit'); 
      legend_inhibit_row.style.backgroundColor = ""
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    } else {
      let edges = cy.$('edge.inhibit'); 
      legend_inhibit_row.style.backgroundColor = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    }
  });

  var checkbox_neutral = document.querySelector("input[id=check_neutral]");

  checkbox_neutral.addEventListener('change', function() {
    var legend_neutral_row = document.getElementById("legend_neutral_row")
    if (this.checked) {
      let edges = cy.$('edge.neutral'); 
      legend_neutral_row.style.backgroundColor = ""
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    } else {
      let edges = cy.$('edge.neutral'); 
      legend_neutral_row.style.backgroundColor = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    }
  });

  var checkbox_undefined = document.querySelector("input[id=check_undefined]");

  checkbox_undefined.addEventListener('change', function() {
    var legend_undefined_row = document.getElementById("legend_undefined_row")
    if (this.checked) {
      let edges = cy.$('edge.undefined'); 
      legend_undefined_row.style.backgroundColor = ""
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    } else {
      let edges = cy.$('edge.undefined'); 
      legend_undefined_row.style.backgroundColor = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    }
  });

  var checkbox_accepted = document.querySelector("input[id=check_accepted]");

  checkbox_accepted.addEventListener('change', function() {
    var legend_accepted_row = document.getElementById("legend_accepted_row")
    // get all edges with property of accepted; maybe even change the objects in the 
    
    if (this.checked) {
      let edges = cy.$('edge[review_status = "accepted"]'); 
      legend_accepted_row.style.backgroundColor = "transparent"
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    } else {
      let edges = cy.$('edge[review_status = "accepted"]'); 
      legend_accepted_row.style.backgroundColor = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    }
  });

  var checkbox_declined = document.querySelector("input[id=check_declined]");

  checkbox_declined.addEventListener('change', function() {
    var legend_declined_row = document.getElementById("legend_declined_row")
    // get all edges with property of accepted; maybe even change the objects in the 
    
    if (this.checked) {
      let edges = cy.$('edge[review_status = "declined"]'); 
      legend_declined_row.style.backgroundColor = "transparent"
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    } else {
      let edges = cy.$('edge[review_status = "declined"]'); 
      legend_declined_row.style.backgroundColor = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    }
  });

  var checkbox_further_inspection = document.querySelector("input[id=check_further_inspection]");

  checkbox_further_inspection.addEventListener('change', function() {
    var legend_further_inspection_row = document.getElementById("legend_further_inspection_row")
    // get all edges with property of accepted; maybe even change the objects in the 
    
    if (this.checked) {
      let edges = cy.$('edge[review_status = "further_inspection"]'); 
      legend_further_inspection_row.style.backgroundColor = "transparent"
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    } else {
      let edges = cy.$('edge[review_status = "further_inspection"]'); 
      legend_further_inspection_row.style.backgroundColor = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    }
  });


  var checkbox_to_review = document.querySelector("input[id=check_to_review]");

  checkbox_to_review.addEventListener('change', function() {
    var legend_to_review_row = document.getElementById("legend_to_review_row")
    // get all edges with property of accepted; maybe even change the objects in the 
    
    if (this.checked) {
      let edges = cy.$('edge[review_status = "to_review"]'); 
      legend_to_review_row.style.backgroundColor = "transparent"
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    } else {
      let edges = cy.$('edge[review_status = "to_review"]'); 
      legend_to_review_row.style.backgroundColor = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    }
  });

  
  var checkbox_coherent = document.querySelector("input[id=check_coherent]");

  checkbox_coherent.addEventListener('change', function() {
    var legend_coherent_row = document.getElementById("legend_coherent_row")
    
    if (this.checked) {
      let edges = cy.$('edge.various'); 
      legend_coherent_row.style["background-color"] = "transparent"
      edges.forEach(edge => { 
        edge.style("visibility","hidden")
      })
    } else {
      let edges = cy.$('edge'); 
      legend_coherent_row.style["background-color"] = "gray"
      edges.forEach(edge => { 
        edge.style("visibility","visible")
      })
    }
  });

  var slider = document.getElementsByName("edge_reference_range")[0]
  var sliderDiv = document.getElementsByTagName("output")[0]

  // get the max and min of edge width

  var edge_widths = []


  cy.edges().forEach(function(edge) {
    edge_widths.push(edge.data().width)
  })

  slider.setAttribute("min", Math.min.apply(Math, edge_widths) );
  slider.setAttribute("max", Math.max.apply(Math, edge_widths) +1 );

  slider.onchange = function() {
    var slider_value = this.value
    sliderDiv.innerHTML = slider_value

    cy.filter(function(element, i){
      return element.isEdge() && element.data('width') < slider_value;
    }).style("visibility","hidden");

    cy.filter(function(element, i){
      return element.isEdge() && element.data('width') > slider_value - 1;
    }).style("visibility","visible");
  }

  var button_reset = document.getElementById("reset_edges")
  button_reset.onclick = function() {
    
    slider.value = 1
    sliderDiv.innerHTML = "1"

  
    var event = new Event('change');

    if(!checkbox_active.checked) {
      checkbox_active.checked = "true"
      checkbox_active.dispatchEvent(event);
    }

    if(!checkbox_inhibit.checked) {
      checkbox_inhibit.checked = "true"
      checkbox_inhibit.dispatchEvent(event);
    }

    if(!checkbox_undefined.checked) {
      checkbox_undefined.checked = "true"
      checkbox_undefined.dispatchEvent(event);
    }

    if(!checkbox_neutral.checked) {
      checkbox_neutral.checked = "true"
      checkbox_neutral.dispatchEvent(event);
    }

    if(checkbox_verified.checked) {
      checkbox_verified.click()
    }
    
    if(checkbox_unverified.checked) {
      checkbox_unverified.click()
    }

    var edges = cy.$("edge")
    edges.forEach(edge => { 
      edge.style("visibility","visible")
    })
    
    

  }

  var button_complete = document.getElementById("button_complete")
  button_complete.onclick = function() {
    cy.remove(cy.elements())
    cy.add(sbgn_JSON_doubles)
    var labellist = button_complete.parentElement.parentElement.getElementsByTagName("label")
    for(var i = 0; i < labellist.length; i++) {
      labellist[i].classList.remove("active")
    }
    button_complete.parentElement.classList.add("active")
  }

  var button_1990 = document.getElementById("button_1990")
  button_1990.onclick = function() {
    cy.remove(cy.elements())
    cy.add(map_1990)
    var labellist = button_1990.parentElement.parentElement.getElementsByTagName("label")
    for(var i = 0; i < labellist.length; i++) {
      labellist[i].classList.remove("active")
    }
    button_1990.parentElement.classList.add("active")
  }

  var button_1995 = document.getElementById("button_1995")
  button_1995.onclick = function() {
    cy.remove(cy.elements())
    cy.add(map_1995)
    var labellist = button_1995.parentElement.parentElement.getElementsByTagName("label")
    for(var i = 0; i < labellist.length; i++) {
      labellist[i].classList.remove("active")
    }
    button_1995.parentElement.classList.add("active")

  }

  var button_2000 = document.getElementById("button_2000")
  button_2000.onclick = function() {
    cy.remove(cy.elements())
    cy.add(map_2000)
    var labellist = button_2000.parentElement.parentElement.getElementsByTagName("label")
    for(var i = 0; i < labellist.length; i++) {
      labellist[i].classList.remove("active")
    }
    button_2000.parentElement.classList.add("active")
  }
  var button_2005 = document.getElementById("button_2005")
  button_2005.onclick = function() {
    cy.remove(cy.elements())
    cy.add(map_2005)
    var labellist = button_2005.parentElement.parentElement.getElementsByTagName("label")
    for(var i = 0; i < labellist.length; i++) {
      labellist[i].classList.remove("active")
    }
    button_2005.parentElement.classList.add("active")
  }

  var button_2010 = document.getElementById("button_2010")
  button_2010.onclick = function() {
    cy.remove(cy.elements())
    cy.add(map_2010)
    var labellist = button_2010.parentElement.parentElement.getElementsByTagName("label")
    for(var i = 0; i < labellist.length; i++) {
      labellist[i].classList.remove("active")
    }
    button_2010.parentElement.classList.add("active")
  }
  var button_2015 = document.getElementById("button_2015")
  button_2015.onclick = function() {
    cy.remove(cy.elements())
    cy.add(map_2015)
    var labellist = button_2015.parentElement.parentElement.getElementsByTagName("label")
    for(var i = 0; i < labellist.length; i++) {
      labellist[i].classList.remove("active")
    }
    button_2015.parentElement.classList.add("active")
  }
  var button_2020 = document.getElementById("button_2020")
  button_2020.onclick = function() {
    cy.remove(cy.elements())
    cy.add(map_2020)
    var labellist = button_2020.parentElement.parentElement.getElementsByTagName("label")
    for(var i = 0; i < labellist.length; i++) {
      labellist[i].classList.remove("active")
    }
    button_2020.parentElement.classList.add("active")
  }

  var filter_hide = document.getElementById("filter_hide")
  var filter_body = document.getElementById("filter_body")
  var filter_show = document.getElementById("filter_show")


  filter_hide.onclick = function() {
    filter_body.style.display = "none"
    filter_hide.style.display = "none"
    filter_show.style.display = "block"

  }
  filter_show.onclick = function() {
    filter_body.style.display = "block"
    filter_show.style.display = "none"
    filter_hide.style.display = "block"

  }
};