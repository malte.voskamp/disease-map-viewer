var sbgn_JSON_doubles = [
  {
    "data": {
      "id": "c0",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Extracellular",
      "class": "compartment",
      "parent": ""
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c1",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "PM",
      "class": "compartment",
      "parent": "c0"
    },
    "position": {
      "x": 50,
      "y": 50
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c2",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Cytosol",
      "class": "compartment",
      "parent": "c0"
    },
    "position": {
      "x": 50,
      "y": 105
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c3",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Golgi",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 275,
      "y": 150
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c4",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "ER",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 275,
      "y": 450
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c5",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Nucleus",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 200,
      "y": 740
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c6",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Mitochondrion",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 80,
      "y": 550
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c7",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "Endosome",
      "class": "compartment",
      "parent": "c2"
    },
    "position": {
      "x": 450,
      "y": 100
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "c8",
      "clonemaker": false,
      "stateVariable": [],
      "unitsOfInformation": [],
      "label": "undefined",
      "class": "compartment",
      "parent": ""
    },
    "position": {
      "x": 1000,
      "y": 0
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "SLC26A9",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "SLC26A9",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 150,
      "y": 120
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "AMP",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "AMP",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 150,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "RAB11B",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "RAB11B",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 250,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "RACK1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "RACK1",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 350,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ATP",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ATP",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 450,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "WNK1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "WNK1",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 550,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CBL",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "CBL",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 60,
      "y": 175
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "USP10",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "USP10",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 250,
      "y": 245
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ARF1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ARF1",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 350,
      "y": 245
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "PP2A",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "PP2A",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 450,
      "y": 245
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "COPI",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "COPI",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 550,
      "y": 245
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "DNAJA1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "DNAJA1",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 60,
      "y": 245
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "SLC26A6",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "SLC26A6",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 250,
      "y": 315
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "FLNA",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "FLNA",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 350,
      "y": 315
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "HSPBP1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "HSPBP1",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 450,
      "y": 315
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "IRF1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "IRF1",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 550,
      "y": 315
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "VCP",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "VCP",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 60,
      "y": 315
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CALU",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "CALU",
      "class": "macromolecule",
      "parent": "c4",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 375,
      "y": 520
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "BAP31",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "BAP31",
      "class": "macromolecule",
      "parent": "c4",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 475,
      "y": 520
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "Glc",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "Glc",
      "class": "macromolecule",
      "parent": "c4",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 285,
      "y": 520
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "SEC12",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "SEC12",
      "class": "macromolecule",
      "parent": "c4",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 475,
      "y": 590
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "SEC61B",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "SEC61B",
      "class": "macromolecule",
      "parent": "c4",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 285,
      "y": 590
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "STX8",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "STX8",
      "class": "macromolecule",
      "parent": "c3",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 375,
      "y": 220
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "GSH",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "GSH",
      "class": "macromolecule",
      "parent": "c3",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 285,
      "y": 220
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "VTI1B",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "VTI1B",
      "class": "macromolecule",
      "parent": "c3",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 285,
      "y": 290
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "RAB7A",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "RAB7A",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 250,
      "y": 385
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "Ca(2+)",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "Ca(2+)",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 350,
      "y": 385
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ADCY1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ADCY1",
      "class": "macromolecule",
      "parent": "c6",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 180,
      "y": 620
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "S100A10",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "S100A10",
      "class": "macromolecule",
      "parent": "c6",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 90,
      "y": 620
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "FKBP8",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "FKBP8",
      "class": "macromolecule",
      "parent": "c6",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 90,
      "y": 690
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "G-protein",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "G-protein",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 300,
      "y": 810
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ADP",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ADP",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 810
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "SHANK2",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "SHANK2",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 810
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CREB",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "CREB",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 880
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "GTP",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "GTP",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 880
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "PDZK1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "PDZK1",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 950
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CCAAT element",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "CCAAT element",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 950
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "EHF",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "EHF",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 1020
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "BAG1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "BAG1",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 1020
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ANXA5",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ANXA5",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 1090
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CDX2",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "CDX2",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 1090
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CTCF",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "CTCF",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 1160
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "DNAJB1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "DNAJB1",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 1160
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "HIF1A",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "HIF1A",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 1230
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "HNF1A",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "HNF1A",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 1230
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "IRF2",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "IRF2",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 1300
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "RNF4",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "RNF4",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 1300
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "SIN3A",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "SIN3A",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 400,
      "y": 1370
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "YY1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "YY1",
      "class": "macromolecule",
      "parent": "c5",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 210,
      "y": 1370
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "PDE4D",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "PDE4D",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 250,
      "y": 120
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ANO1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ANO1",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 350,
      "y": 120
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "HCO3-",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "HCO3-",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 450,
      "y": 120
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "AP2",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "AP2",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 550,
      "y": 120
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "EZR",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "EZR",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 60,
      "y": 120
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "DAB2",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "DAB2",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 250,
      "y": 190
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "EHD1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "EHD1",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 350,
      "y": 190
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "NRF2",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "NRF2",
      "class": "macromolecule",
      "parent": "c1",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 450,
      "y": 190
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "clathrin",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "clathrin",
      "class": "macromolecule",
      "parent": "c2",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 450,
      "y": 385
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ADO",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ADO",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1100,
      "y": 70
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CFTR",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "CFTR",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 70
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "Cl-",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "Cl-",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 140
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "Na+",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "Na+",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 210
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "KCNN4",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "KCNN4",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 280
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "USP19",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "USP19",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 350
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "GDP",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "GDP",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 420
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "UBC9",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "UBC9",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 490
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "ATF6",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "ATF6",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 560
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "BECN1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "BECN1",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 630
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "Estradiol",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "Estradiol",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 700
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "PP2B",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "PP2B",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 770
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "FOXA",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "FOXA",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 840
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "IFNG",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "IFNG",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 910
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "LPA",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "LPA",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 980
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "PPi",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "PPi",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 1050
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "SLC26A3",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "SLC26A3",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 1120
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "SQSTM1",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "SQSTM1",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 1190
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "TNF",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "TNF",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 1260
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "folded CFTR",
      "clonemarker": false,
      "stateVariables": [],
      "unitsOfInformation": [],
      "label": "folded CFTR",
      "class": "macromolecule",
      "parent": "c8",
      "bbox": {
        "w": 90,
        "h": 60
      }
    },
    "position": {
      "x": 1010,
      "y": 1330
    },
    "group": "nodes"
  },
  {
    "data": {
      "id": "CALUADO",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "ADO",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "ADO",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate found was use compartmentalize",
          "categorical_verbs": "activate neutral undefined undefined undefined",
          "sentences": "When cystic fibrosis transmembrane conductance regulator (CFTR) channel activity was used as  a functional readout, we found signaling elements compartmentalized at both extracellular and intracellular surfaces of the apical cell membrane that activate apical Cl(-) conductance in Calu-3 cells.",
          "PMID": 11707576
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRADO",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "ADO",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "ADO",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate found was use compartmentalize",
          "categorical_verbs": "activate neutral undefined undefined undefined",
          "sentences": "When cystic fibrosis transmembrane conductance regulator (CFTR) channel activity was used as  a functional readout, we found signaling elements compartmentalized at both extracellular and intracellular surfaces of the apical cell membrane that activate apical Cl(-) conductance in Calu-3 cells.",
          "PMID": 11707576
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADOADP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADO",
      "target": "ADP",
      "bendPointPositions": [],
      "portSource": "ADO",
      "portTarget": "ADP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADCY1AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADCY1",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "ADCY1",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate translocate containe produce",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "GPCRs translocate Ca2+-sensitive adenylate cyclase type 1 (ADCY1) and exchange protein directly activated by cAMP (EPAC1) to particular plasma membrane domains containing GPCRs, CFTR and TMEM16A, thereby producing compartmentalized Ca2+ and cAMP signals and significant crosstalk.",
          "PMID": 29331508
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADOAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADO",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "ADO",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADPAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADP",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "ADP",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ANO1AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ANO1",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "ANO1",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate expres",
          "categorical_verbs": "activate activate",
          "sentences": "Airway epithelial cells express both Ca2+ activated TMEM16A/ANO1 and cAMP activated CFTR anion channels.",
          "PMID": 29331508
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ATPAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 5,
      "references": {
        "0": {
          "Verbs": "regulates present short-circuit culture triggere",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "We present results from whole-cell and single-channel patch-clamp recordings, short-circuit current recordings, and [gamma-32P]ATP release assays of normal, CF, and wild-type or mutant CFTR-transfected CF airway cultured epithelial cells wherein CFTR regulates ORCCs by triggering the transport of the potent agonist, ATP, out of the cell.",
          "PMID": 7541313
        },
        "1": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "2": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        },
        "3": {
          "Verbs": "is remain",
          "categorical_verbs": "undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated, ATP-gated Cl(-) channel and cellular conductance regulator, but the detailed mechanisms of CFTR regulation and its regulation of other transport  proteins remain obscure.",
          "PMID": 12519745
        },
        "4": {
          "Verbs": "result",
          "categorical_verbs": "undefined",
          "sentences": "Cystic fibrosis results from mutations in the cystic fibrosis conductance regulator protein (CFTR), a cAMP/protein kinase A (PKA) and ATP-regulated Cl(-) channel.",
          "PMID": 17581860
        },
        "5": {
          "Verbs": "is characterise cause",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Cystic fibrosis (CF) is characterised by impaired epithelial ion transport and is caused by mutations in the cystic fibrosis conductance regulator protein (CFTR),  a cAMP/PKA and ATP-regulated chloride channel.",
          "PMID": 18346874
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "BAP31AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "BAP31",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "BAP31",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "increase enabl",
          "categorical_verbs": "activate undefined",
          "sentences": "Antisense inhibition of BAP31 in various cell types increased expression of both wild-type CFTR and [DeltaPhe(508)]CFTR and enabled cAMP-activated Cl(-) currents in [DeltaPhe(508)]CFTR-expressing CHO cells.",
          "PMID": 11274174
        },
        "1": {
          "Verbs": "attenuate",
          "categorical_verbs": "undefined",
          "sentences": "Coexpression of CFTR together with BAP31 attenuated cAMP-activated Cl(-) currents in Xenopus oocytes.",
          "PMID": 11274174
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CALUAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "reduces show do",
          "categorical_verbs": "inhibit undefined undefined",
          "sentences": "Whole-cell patch clamp of Calu-3 cells shows that Ht31 peptide reduces cAMP-stimulated CFTR Cl(-) current, but Ht31P does not.",
          "PMID": 10799517
        },
        "1": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "In whole-cell patch clamp experiments, the activation of  endogenous AMPK either pharmacologically or by the overexpression of an AMPK-activating non-catalytic subunit mutant (AMPK-gamma1-R70Q) dramatically inhibited forskolin-stimulated CFTR conductance in Calu-3 and CFTR-expressing Chinese hamster ovary cells.",
          "PMID": 12427743
        },
        "2": {
          "Verbs": "reduce was transfect close",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "In contrast, the single channel open probability of CFTR was strongly reduced in cell-attached patch clamp measurements of Calu-3 cells transfected with the AMPK-activating mutant, an effect due primarily to a substantial prolongation of the mean closed  time of the channel.",
          "PMID": 12427743
        },
        "3": {
          "Verbs": "inhibit block bind block affect embedd pull do",
          "categorical_verbs": "inhibit inhibit neutral inhibit neutral undefined undefined undefined",
          "sentences": "An internal 11-amino acid motif embedding the GYGF carboxylate binding loop of PDZ1 binds to RACK1, inhibits binding of recombinant NHERF1 and RACK1, pulls down endogenous RACK1 from Calu-3 cell lysate, and blocks coimmunoprecipitation of endogenous RACK1 with endogenous NHERF1 but does not affect cAMP-dependent activation of CFTR.",
          "PMID": 15075202
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "inhibit",
      "inhibit",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CCAAT elementAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CCAAT element",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CCAAT element",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "require recognize",
          "categorical_verbs": "undefined undefined",
          "sentences": "Basal expression of CFTR transcription and cAMP-mediated transcriptional regulation require the presence of an imperfect and inverted CCAAT element recognized as 5'-AATTGGAAGCAAAT-3', located between 132 and 119 nucleotides upstream of the translational start site.",
          "PMID": 7499410
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 56,
      "references": {
        "0": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "Phosphorylation of the R domain by cAMP-dependent protein kinase regulates the CFTR chloride channel.",
          "PMID": 1716180
        },
        "1": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "Phosphorylation of the R domain by cAMP-dependent protein kinase regulates the CFTR chloride channel.",
          "PMID": 1716180
        },
        "2": {
          "Verbs": "associate associate is phosphorylate",
          "categorical_verbs": "neutral neutral undefined undefined",
          "sentences": "CFTR, the protein associated with cystic fibrosis, is phosphorylated on serine residues in response to cAMP agonists.",
          "PMID": 1716180
        },
        "3": {
          "Verbs": "require recognize",
          "categorical_verbs": "undefined undefined",
          "sentences": "Basal expression of CFTR transcription and cAMP-mediated transcriptional regulation require the presence of an imperfect and inverted CCAAT element recognized as 5'-AATTGGAAGCAAAT-3', located between 132 and 119 nucleotides upstream of the translational start site.",
          "PMID": 7499410
        },
        "4": {
          "Verbs": "activate suggest be involv",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Also, the detection of specific activating transcription factor/cyclic-AMP response element binding protein antigens by antibody supershift analysis of nuclear complexes suggest that species of this family of transcription factors could be involved in the formation of complexes with C/EBP delta within the CFTR gene inverted CCAAT-like element.",
          "PMID": 7499410
        },
        "5": {
          "Verbs": "activate raise contribute rendere",
          "categorical_verbs": "activate activate undefined undefined",
          "sentences": "These studies  raise the possibility of interactions between individual members of the C/EBP and activating transcription factor/cyclic-AMP response element binding protein families potentially contribute to the tight transcriptional control rendered by  the CFTR gene promoter.",
          "PMID": 7499410
        },
        "6": {
          "Verbs": "regulates present short-circuit culture triggere",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "We present results from whole-cell and single-channel patch-clamp recordings, short-circuit current recordings, and [gamma-32P]ATP release assays of normal, CF, and wild-type or mutant CFTR-transfected CF airway cultured epithelial cells wherein CFTR regulates ORCCs by triggering the transport of the potent agonist, ATP, out of the cell.",
          "PMID": 7541313
        },
        "7": {
          "Verbs": "inhibit has been shown",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "A dominant negative inhibitor of the cAMP-dependent protein kinase has been shown to inhibit the basal expression of the cystic fibrosis transmembrane conductance  regulator (CFTR) gene in the human colon carcinoma cell line, T84.",
          "PMID": 8943230
        },
        "8": {
          "Verbs": "was have analyze",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "A functional cAMP response element (CRE) was localized at -48 in the CFTR promoter, and we have analyzed the interactions of this regulatory region with transcription factors.",
          "PMID": 8943230
        },
        "9": {
          "Verbs": "drive place",
          "categorical_verbs": "undefined undefined",
          "sentences": "The CFTR CRE will also drive cAMP-mediated expression when placed upstream of a heterologous basal promoter.",
          "PMID": 8943230
        },
        "10": {
          "Verbs": "activate demonstrate is suggest be",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "These results demonstrate that CFTR is a bona fide CRE-dependent  gene, and we suggest that CFTR expression levels in vivo may be responsive to hormones or drugs that activate the cAMP-dependent protein kinase system.",
          "PMID": 8943230
        },
        "11": {
          "Verbs": "inhibit (",
          "categorical_verbs": "inhibit undefined",
          "sentences": "cAMP-dependent activation of CFTR inhibits epithelial Na+ channels (ENaC).",
          "PMID": 10220462
        },
        "12": {
          "Verbs": "coexpress were perform",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "To that end, two-electrode voltage-clamp experiments were performed on Xenopus oocytes coexpressing ENaC together with CFTR, the multidrug  resistance protein MDR1, the sulfonyl urea receptor SUR1, or the cadmium permease YCF1.",
          "PMID": 10220462
        },
        "13": {
          "Verbs": "inhibit is shown truncate",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "In fact, it is shown that C-terminal truncated CFTR is able to inhibit ENaC on activation by intracellular cAMP.",
          "PMID": 10220462
        },
        "14": {
          "Verbs": "affect disrupt include augment derive did",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined",
          "sentences": "Reagents that disrupt the CFTR-syntaxin 1A interaction, including soluble syntaxin 1A cytosolic domain and recombinant Munc-18, augmented cAMP-dependent CFTR Cl(-) currents by more than 2- to 4-fold in mouse tracheal epithelial cells and cells derived from human nasal polyps, but these reagents did not affect CaMK II-activated Cl(-) currents in these cells.",
          "PMID": 10675364
        },
        "15": {
          "Verbs": "controll controll is",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is an epithelial Cl(-) channel whose activity is controlled by cAMP-dependent protein kinase (PKA)-mediated phosphorylation.",
          "PMID": 10799517
        },
        "16": {
          "Verbs": "reduces show do",
          "categorical_verbs": "inhibit undefined undefined",
          "sentences": "Whole-cell patch clamp of Calu-3 cells shows that Ht31 peptide reduces cAMP-stimulated CFTR Cl(-) current, but Ht31P does not.",
          "PMID": 10799517
        },
        "17": {
          "Verbs": "potentiate",
          "categorical_verbs": "activate",
          "sentences": "Co-expression of CFTR with E3KARP and ezrin in Xenopus oocytes potentiated cAMP-stimulated CFTR Cl(-) currents.",
          "PMID": 10893422
        },
        "18": {
          "Verbs": "activate regulate be is known",
          "categorical_verbs": "activate neutral undefined undefined undefined",
          "sentences": "Other than the fact that the cystic fibrosis transmembrane conductance regulator  (CFTR) Cl- channel can be activated by cAMP dependent kinase (PKA), little is known about the signal transduction pathways regulating CFTR.",
          "PMID": 11155209
        },
        "19": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "20": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        },
        "21": {
          "Verbs": "induce was compare",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "The magnitude of mutant CFTR G(Cl) activation by G-proteins was smaller as compared to non-CF ducts but comparable to that induced by cAMP in CF ducts.",
          "PMID": 11155209
        },
        "22": {
          "Verbs": "increase enabl",
          "categorical_verbs": "activate undefined",
          "sentences": "Antisense inhibition of BAP31 in various cell types increased expression of both wild-type CFTR and [DeltaPhe(508)]CFTR and enabled cAMP-activated Cl(-) currents in [DeltaPhe(508)]CFTR-expressing CHO cells.",
          "PMID": 11274174
        },
        "23": {
          "Verbs": "attenuate",
          "categorical_verbs": "undefined",
          "sentences": "Coexpression of CFTR together with BAP31 attenuated cAMP-activated Cl(-) currents in Xenopus oocytes.",
          "PMID": 11274174
        },
        "24": {
          "Verbs": "is learn",
          "categorical_verbs": "undefined undefined",
          "sentences": "cAMP signaling cascades and CFTR: is there more to learn?",
          "PMID": 11845310
        },
        "25": {
          "Verbs": "is learn",
          "categorical_verbs": "undefined undefined",
          "sentences": "cAMP signaling cascades and CFTR: is there more to learn?",
          "PMID": 11845310
        },
        "26": {
          "Verbs": "regulate mediate is",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "CFTR is an apically resident ion channel whose activity is regulated by the activation of the cAMP mediated second messenger cascade.",
          "PMID": 11845310
        },
        "27": {
          "Verbs": "regulate stimulate is",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "Activation of the chloride selective anion channel CFTR is stimulated by cAMP-dependent phosphorylation and is regulated by the target membrane t-SNARE syntaxin 1A.",
          "PMID": 11865034
        },
        "28": {
          "Verbs": "demonstrate was require",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "We demonstrated previously that PKC epsilon was required for cAMP-dependent CFTR function.",
          "PMID": 11956211
        },
        "29": {
          "Verbs": "regulates is",
          "categorical_verbs": "neutral undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-regulated chloride channel whose phosphorylation regulates both channel gating and its trafficking at the plasma membrane.",
          "PMID": 12039948
        },
        "30": {
          "Verbs": "express increases decrease evoke detect",
          "categorical_verbs": "activate activate inhibit undefined undefined",
          "sentences": "In Xenopus oocytes expressing CFTR, Csp overexpression decreased the chloride current and membrane capacitance increases evoked by cAMP stimulation and decreased the levels of CFTR protein detected by immunoblot.",
          "PMID": 12039948
        },
        "31": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "In whole-cell patch clamp experiments, the activation of  endogenous AMPK either pharmacologically or by the overexpression of an AMPK-activating non-catalytic subunit mutant (AMPK-gamma1-R70Q) dramatically inhibited forskolin-stimulated CFTR conductance in Calu-3 and CFTR-expressing Chinese hamster ovary cells.",
          "PMID": 12427743
        },
        "32": {
          "Verbs": "reduce was transfect close",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "In contrast, the single channel open probability of CFTR was strongly reduced in cell-attached patch clamp measurements of Calu-3 cells transfected with the AMPK-activating mutant, an effect due primarily to a substantial prolongation of the mean closed  time of the channel.",
          "PMID": 12427743
        },
        "33": {
          "Verbs": "is remain",
          "categorical_verbs": "undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated, ATP-gated Cl(-) channel and cellular conductance regulator, but the detailed mechanisms of CFTR regulation and its regulation of other transport  proteins remain obscure.",
          "PMID": 12519745
        },
        "34": {
          "Verbs": "addres have study polarize evaluate measure",
          "categorical_verbs": "undefined undefined undefined undefined undefined undefined",
          "sentences": "To address the physiological relevance of the CFTR-AMPK interaction, we have now studied polarized epithelia and have evaluated the localization of endogenous AMPK and CFTR and measured CFTR activity with modulation of AMPK activity.",
          "PMID": 12519745
        },
        "35": {
          "Verbs": "suppress",
          "categorical_verbs": "inhibit",
          "sentences": "Of interest, expression of Shank2 suppressed cAMP-induced phosphorylation and activation of CFTR.",
          "PMID": 14679199
        },
        "36": {
          "Verbs": "regulates express indicate",
          "categorical_verbs": "neutral activate undefined",
          "sentences": "Syntaxin 8 regulates CFTR-mediated currents in chinese hamster ovary (CHO) cells stably expressing CFTR and syntaxin 8. Iodide efflux and whole-cell patch-clamp experiments on these cells indicate a strong inhibition of CFTR chloride current by syntaxin 8 overexpression.",
          "PMID": 15039462
        },
        "37": {
          "Verbs": "inhibit block bind block affect embedd pull do",
          "categorical_verbs": "inhibit inhibit neutral inhibit neutral undefined undefined undefined",
          "sentences": "An internal 11-amino acid motif embedding the GYGF carboxylate binding loop of PDZ1 binds to RACK1, inhibits binding of recombinant NHERF1 and RACK1, pulls down endogenous RACK1 from Calu-3 cell lysate, and blocks coimmunoprecipitation of endogenous RACK1 with endogenous NHERF1 but does not affect cAMP-dependent activation of CFTR.",
          "PMID": 15075202
        },
        "38": {
          "Verbs": "affect is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CFTR activation by cAMP-generating agent is not  affected by loss of RACK1-NHERF1 interaction.",
          "PMID": 15075202
        },
        "39": {
          "Verbs": "raise do hypothesize ( generate includes",
          "categorical_verbs": "activate undefined undefined undefined undefined undefined",
          "sentences": "Because luminal A2B receptor activation does not raise total cellular cAMP levels, we hypothesized that activation of phosphodiesterases (PDEs) confines cAMP generated by apical A2B receptors to a microdomain that includes the CFTR channel.",
          "PMID": 15611099
        },
        "40": {
          "Verbs": "is identify",
          "categorical_verbs": "undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-dependent chloride channel in epithelial cells; recently, we identified it in mast cells.",
          "PMID": 16051699
        },
        "41": {
          "Verbs": "is line establish play",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-regulated chloride channel localized primarily at the apical or luminal surfaces of epithelial cells that line the airway, gut, and exocrine glands; it is well established that CFTR plays a pivotal role in cholera toxin (CTX)-induced secretory diarrhea.",
          "PMID": 16203867
        },
        "42": {
          "Verbs": "express is",
          "categorical_verbs": "activate undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated chloride channel expressed at the apical surface of epithelia.",
          "PMID": 16239222
        },
        "43": {
          "Verbs": "is cause",
          "categorical_verbs": "undefined undefined",
          "sentences": "Cystic fibrosis (CF), an autosomal recessive disorder, is caused by the disruption of biosynthesis or the function of a membrane cAMP-activated chloride  channel, CFTR.",
          "PMID": 16413502
        },
        "44": {
          "Verbs": "express respond",
          "categorical_verbs": "activate undefined",
          "sentences": "The colorectal cell line HT-29 natively expresses CFTR and responds to cAMP stimulation with an increase in CFTR-mediated currents.",
          "PMID": 16413502
        },
        "45": {
          "Verbs": "inhibit",
          "categorical_verbs": "inhibit",
          "sentences": "Rab4 over-expression in HT-29 cells inhibits both basal and cAMP-stimulated CFTR-mediated currents.",
          "PMID": 16413502
        },
        "46": {
          "Verbs": "express is are seen",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl- channel expressed in a wide variety of epithelial cells, mutations of which are responsible for hallmark defective Cl- and HCO3- secretion seen in cystic fibrosis (CF).",
          "PMID": 16414184
        },
        "47": {
          "Verbs": "block block show posses be known",
          "categorical_verbs": "inhibit inhibit undefined undefined undefined undefined",
          "sentences": "Our recent results show that endometrial epithelial cells possess a cAMP-activated HCO3- transport mechanism, which could be impaired with  channel blockers known to block CFTR or antisense against CFTR.",
          "PMID": 16414184
        },
        "48": {
          "Verbs": "perform treate detect",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "We also performed a functional assay for the CFTR chloride channel in CFPAC-1 cells treated or not with curcumin and detected an increase in a cAMP-dependent chloride efflux in treated DeltaF508-CFTR-expressing cells.",
          "PMID": 16424149
        },
        "49": {
          "Verbs": "is establish",
          "categorical_verbs": "undefined undefined",
          "sentences": "The role of the cystic fibrosis transmembrane conductance regulator (CFTR) as a cAMP-dependent chloride channel on the apical membrane of epithelia is well established.",
          "PMID": 17235394
        },
        "50": {
          "Verbs": "demonstrate use",
          "categorical_verbs": "undefined undefined",
          "sentences": "Here we demonstrate a physical and physiological competition between EBP50-CFTR and Shank2-CFTR associations and the dynamic regulation of CFTR activity by these positive and negative interactions using the surface plasmon resonance assays and consecutive patch clamp experiments.",
          "PMID": 17244609
        },
        "51": {
          "Verbs": "associate associate found recruit was be precludes",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined undefined",
          "sentences": "Furthermore whereas EBP50 recruits a cAMP-dependent protein kinase (PKA) complex to CFTR, Shank2 was found to be physically and functionally associated with the cyclic nucleotide phosphodiesterase PDE4D that precludes cAMP/PKA signals in epithelial cells and mouse brains.",
          "PMID": 17244609
        },
        "52": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "The formation of the cAMP/protein kinase A-dependent annexin 2-S100A10 complex with cystic fibrosis conductance regulator protein (CFTR) regulates CFTR channel function.",
          "PMID": 17581860
        },
        "53": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "The formation of the cAMP/protein kinase A-dependent annexin 2-S100A10 complex with cystic fibrosis conductance regulator protein (CFTR) regulates CFTR channel  function.",
          "PMID": 17581860
        },
        "54": {
          "Verbs": "result",
          "categorical_verbs": "undefined",
          "sentences": "Cystic fibrosis results from mutations in the cystic fibrosis conductance regulator protein (CFTR), a cAMP/protein kinase A (PKA) and ATP-regulated Cl(-) channel.",
          "PMID": 17581860
        },
        "55": {
          "Verbs": "report",
          "categorical_verbs": "undefined",
          "sentences": "We report  that annexin 2 (anx 2)-S100A10 forms a functional cAMP/PKA/calcineurin (CaN)-dependent complex with CFTR.",
          "PMID": 17581860
        },
        "56": {
          "Verbs": "inhibit ( DIDS taken rectify showe",
          "categorical_verbs": "inhibit undefined undefined undefined undefined undefined",
          "sentences": "Analysis of 4,4'-diisothiocyanatostilbene-2,2'-disulfonic acid (DIDS) and CFTR(inh172)-sensitive currents, taken as indication of the outwardly rectifying  Cl(-) channels (ORCC) and CFTR-mediated currents, respectively, showed that Ac1-14, but not N1-14, inhibits both the cAMP/PKA-dependent ORCC and CFTR activities.",
          "PMID": 17581860
        },
        "57": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) functions as a cAMP-activated chloride channel, which is regulated by protein-protein interactions.",
          "PMID": 17869070
        },
        "58": {
          "Verbs": "decrease had deplete showe",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Importantly, decreased trafficking of CFTR had a functional consequence as cells depleted of beta-COP showed decreased cAMP-activated chloride currents.",
          "PMID": 17932045
        },
        "59": {
          "Verbs": "is localize exist",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-regulated chloride channel localized at apical cell membranes and exists in macromolecular  complexes with a variety of signaling and transporter molecules.",
          "PMID": 18045536
        },
        "60": {
          "Verbs": "associates associates report",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "Here, we report  that the multidrug resistance protein 4 (MRP4), a cAMP transporter, functionally  and physically associates with CFTR.",
          "PMID": 18045536
        },
        "61": {
          "Verbs": "potentiate are is coupl attenuate",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Adenosine-stimulated CFTR-mediated chloride  currents are potentiated by MRP4 inhibition, and this potentiation is directly coupled to attenuated cAMP efflux through the apical cAMP transporter.",
          "PMID": 18045536
        },
        "62": {
          "Verbs": "suggest occur form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "CFTR single-channel recordings and FRET-based intracellular cAMP dynamics suggest that a compartmentalized coupling of cAMP transporter and CFTR occurs via the PDZ scaffolding protein, PDZK1, forming a macromolecular complex at apical surfaces of gut epithelia.",
          "PMID": 18045536
        },
        "63": {
          "Verbs": "Disrupt abrogates",
          "categorical_verbs": "undefined undefined",
          "sentences": "Disrupting this complex abrogates the functional coupling of cAMP transporter activity to CFTR function.",
          "PMID": 18045536
        },
        "64": {
          "Verbs": "diminish reduce demonstrate lead",
          "categorical_verbs": "inhibit inhibit undefined undefined",
          "sentences": "In the present study, we demonstrate that inhibition of CFTR expression under ER stress leads to reduced cAMP-activated chloride secretion in  epithelial monolayers, an indication of diminished CFTR function.",
          "PMID": 18319256
        },
        "65": {
          "Verbs": "is characterise cause",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Cystic fibrosis (CF) is characterised by impaired epithelial ion transport and is caused by mutations in the cystic fibrosis conductance regulator protein (CFTR),  a cAMP/PKA and ATP-regulated chloride channel.",
          "PMID": 18346874
        },
        "66": {
          "Verbs": "demonstrate",
          "categorical_verbs": "undefined",
          "sentences": "We recently demonstrated a cAMP/PKA/calcineurin (CnA)-driven association between annexin 2 (anx 2), its cognate partner -S100A10 and cell surface CFTR.",
          "PMID": 18346874
        },
        "67": {
          "Verbs": "express is hypothesize be",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Since the cAMP/PKA-induced Cl(-) current is absent in CF epithelia, we hypothesized that the anx 2-S100A10/CFTR complex may be defective in CFBE41o cells expressing the commonest F508del-CFTR (DeltaF-CFTR) mutation.",
          "PMID": 18346874
        },
        "68": {
          "Verbs": "induce demonstrate fail",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Here, we demonstrate that, despite the presence of cell surface DeltaF-CFTR, cAMP/PKA fails to induce anx 2-S100A10/CFTR complex formation in CFBE41o- cells homozygous for F508del-CFTR.",
          "PMID": 18346874
        },
        "69": {
          "Verbs": "demonstrate is suggest contribute",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Thus, we demonstrate that  cAMP/PKA/CnA signaling pathway is defective in CF cells and suggest that loss of  anx 2-S100A10/CFTR complex formation may contribute to defective cAMP/PKA-dependent CFTR channel function.",
          "PMID": 18346874
        },
        "70": {
          "Verbs": "interact showe SNARE belong",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Moreover, co-immunoprecipitation experiments in LLC-PK1-CFTR cells showed that CFTR and SNARE proteins belong to a same complex and pull-down assays showed that VAMP8 and vti1b preferentially interact with CFTR N-terminus tail.",
          "PMID": 18570918
        },
        "71": {
          "Verbs": "block associate block increase associate is",
          "categorical_verbs": "inhibit neutral inhibit activate neutral undefined",
          "sentences": "Elevation of intracellular cAMP (a CFTR activator) is also ineffective whereas increasing intracellular calcium blocks the SLC26A9 associated currents.",
          "PMID": 18769029
        },
        "72": {
          "Verbs": "undergo polarize",
          "categorical_verbs": "undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR), a cAMP/PKA-activated anion channel, undergoes efficient apical recycling in polarized epithelia.",
          "PMID": 19244346
        },
        "73": {
          "Verbs": "inhibite stimulate express increase perform demonstrate",
          "categorical_verbs": "inhibit neutral activate activate undefined undefined",
          "sentences": "CFTR function  assays performed on T84 cells expressing the Rab11a or Rab11b GDP-locked S25N mutants demonstrated that only the Rab11b mutant inhibited 80% of the cAMP-activated halide efflux and that only the constitutively active Rab11b-Q70L  increased the rate constant for stimulated halide efflux.",
          "PMID": 19244346
        },
        "74": {
          "Verbs": "express is",
          "categorical_verbs": "activate undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl(-) channel expressed in the apical membrane of fluid-transporting epithelia.",
          "PMID": 20525683
        },
        "75": {
          "Verbs": "restore lack are hampere rescu",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "Therapeutic efforts to restore biosynthetic processing of the cystic fibrosis transmembrane conductance regulator lacking the F508 residue (DeltaF508CFTR) are  hampered by ubiquitin-dependent lysosomal degradation of nonnative, rescued DeltaF508CFTR from the plasma membrane.",
          "PMID": 20595578
        },
        "76": {
          "Verbs": "elevates upregulates",
          "categorical_verbs": "activate activate",
          "sentences": "Functionally, it elevates cAMP levels in proximity to CFTR and upregulates its channel activity.",
          "PMID": 21299497
        },
        "77": {
          "Verbs": "activate potentiate demonstrate",
          "categorical_verbs": "activate activate undefined",
          "sentences": "RESULTS: Studies in control tissues demonstrate that 1-EBIO activated CFTR-mediated Cl⁻ secretion in the absence of cAMP-mediated stimulation and potentiated cAMP-induced Cl⁻ secretion by 39.2±6.7% (P<0.001) via activation of basolateral Ca²⁺-activated and clotrimazole-sensitive KCNN4 K⁺ channels.",
          "PMID": 21909392
        },
        "78": {
          "Verbs": "potentiate",
          "categorical_verbs": "activate",
          "sentences": "In CF specimens, 1-EBIO potentiated cAMP-induced Cl⁻ secretion in tissues with residual CFTR function by 44.4±11.5% (P<0.001),",
          "PMID": 21909392
        },
        "79": {
          "Verbs": "is",
          "categorical_verbs": "undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP/protein kinase A (PKA)-regulated chloride channel whose phosphorylation controls anion secretion across epithelial cell apical membranes.",
          "PMID": 22278744
        },
        "80": {
          "Verbs": "regulates examine base predict",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "We examined the hypothesis that cAMP/PKA stimulation regulates CFTR biogenesis posttranslationally, based on predicted 14-3-3 binding motifs within CFTR and forskolin-induced CFTR expression.",
          "PMID": 22278744
        },
        "81": {
          "Verbs": "permit make",
          "categorical_verbs": "undefined undefined",
          "sentences": "This mechanism permits cAMP/PKA stimulation to make more CFTR available for anion secretion.",
          "PMID": 22278744
        },
        "82": {
          "Verbs": "controll express controll is",
          "categorical_verbs": "neutral activate neutral undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl(-) channel expressed in the apical plasma membrane of fluid-transporting epithelia, where the plasma membrane abundance of CFTR is in part controlled by clathrin-mediated endocytosis.",
          "PMID": 22399289
        },
        "83": {
          "Verbs": "have reviewe obtaine were stratify",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "We have reviewed transcriptome data obtained from intestinal epithelial samples of homozygotes for c.1521_1523delCTT in CFTR, which were stratified for their EHF genetic background.",
          "PMID": 24105369
        },
        "84": {
          "Verbs": "regulates play",
          "categorical_verbs": "neutral undefined",
          "sentences": "The formation of CFTR-NHERF2-LPA2 macromolecular complex in airway epithelia regulates CFTR channel function and plays an important role in compartmentalized  cAMP signaling.",
          "PMID": 24613836
        },
        "85": {
          "Verbs": "is",
          "categorical_verbs": "undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-regulated chloride (Cl(-)) channel.",
          "PMID": 24685677
        },
        "86": {
          "Verbs": "Compartmentalize contributes",
          "categorical_verbs": "undefined undefined",
          "sentences": "Compartmentalized accumulation of cAMP near complexes of multidrug resistance protein 4 (MRP4) and cystic fibrosis transmembrane conductance regulator (CFTR) contributes to drug-induced diarrhea.",
          "PMID": 25762723
        },
        "87": {
          "Verbs": "Compartmentalize contributes",
          "categorical_verbs": "undefined undefined",
          "sentences": "Compartmentalized accumulation of cAMP near complexes of multidrug resistance protein 4 (MRP4) and cystic fibrosis transmembrane conductance regulator (CFTR) contributes to drug-induced diarrhea.",
          "PMID": 25762723
        },
        "88": {
          "Verbs": "activate inhibite",
          "categorical_verbs": "activate inhibit",
          "sentences": "These drugs activate cystic fibrosis transmembrane conductance regulator (CFTR)-mediated fluid secretion by inhibiting MRP4-mediated cAMP efflux.",
          "PMID": 25762723
        },
        "89": {
          "Verbs": "elevate reduce have are",
          "categorical_verbs": "activate inhibit undefined undefined",
          "sentences": "Some CFTR mutants have reduced responsiveness to cAMP/PKA signaling; hence, pharmacological agents that elevate intracellular cAMP are potentially useful for the treatment of CF.",
          "PMID": 26545902
        },
        "90": {
          "Verbs": "inhibite stimulate",
          "categorical_verbs": "inhibit neutral",
          "sentences": "By inhibiting cAMP breakdown, phosphodiesterase (PDE) inhibitors stimulate CFTR in vitro and in vivo.",
          "PMID": 26545902
        },
        "91": {
          "Verbs": "modulate enhance",
          "categorical_verbs": "neutral undefined",
          "sentences": "Correctors of mutant CFTR enhance subcortical cAMP-PKA signaling through modulating ezrin phosphorylation and cytoskeleton organization.",
          "PMID": 26823603
        },
        "92": {
          "Verbs": "modulate enhance",
          "categorical_verbs": "neutral undefined",
          "sentences": "Correctors of mutant CFTR enhance subcortical cAMP-PKA signaling through modulating ezrin phosphorylation and cytoskeleton organization.",
          "PMID": 26823603
        },
        "93": {
          "Verbs": "activate show stabilize rescu",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Here, we show that the phosphorylation of ezrin together with its binding to phosphatidylinositol-4,5-bisphosphate (PIP2) tethers the F508del CFTR to the actin cytoskeleton, stabilizing it on the apical membrane and rescuing the sub-membrane compartmentalization of cAMP and activated PKA.",
          "PMID": 26823603
        },
        "94": {
          "Verbs": "activate act rescu restore phosphorylate",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Both the small molecules trimethylangelicin (TMA) and VX-809, which act as 'correctors' for F508del CFTR by rescuing F508del-CFTR-dependent chloride secretion, also restore the apical expression of phosphorylated ezrin and actin organization and increase cAMP and activated PKA submembrane compartmentalization in both primary and secondary cystic fibrosis airway cells.",
          "PMID": 26823603
        },
        "95": {
          "Verbs": "activate reverse highlight create generate",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Latrunculin B treatment or expression of the inactive ezrin mutant T567A reverse the TMA and VX-809-induced effects highlighting the role of corrector-dependent ezrin activation and actin re-organization in creating the conditions to generate a sub-cortical cAMP pool of adequate amplitude to activate the F508del-CFTR-dependent chloride secretion.",
          "PMID": 26823603
        },
        "96": {
          "Verbs": "is cause",
          "categorical_verbs": "undefined undefined",
          "sentences": "Of particular importance is the cAMP-dependent cystic fibrosis transmembrane conductance regulator Cl- channel (CFTR) with mutations of the CFTR encoding gene causing cystic fibrosis.",
          "PMID": 27092946
        },
        "97": {
          "Verbs": "allowe leave is modify reach be delivere function",
          "categorical_verbs": "undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "If allowed to leave the ER, CFTR is modified at the Golgi and reaches the post-Golgi compartments to be delivered to the plasma membrane where it functions as a cAMP- and phosphorylation-regulated chloride/bicarbonate channel.",
          "PMID": 27699454
        },
        "98": {
          "Verbs": "interact hamper",
          "categorical_verbs": "neutral undefined",
          "sentences": "The intermediate filament protein keratin 8 (K8) interacts with the nucleotide-binding domain 1 (NBD1) of the cystic fibrosis (CF) transmembrane regulator (CFTR) with phenylalanine 508 deletion (ΔF508), and this interaction hampers the biogenesis of functional ΔF508-CFTR and its insertion into the plasma membrane.",
          "PMID": 27870250
        },
        "99": {
          "Verbs": "is line",
          "categorical_verbs": "undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP- and cGMP-regulated chloride (Cl-) and bicarbonate (HCO₃-) channel localized primarily at the apical plasma membrane of epithelial cells lining the airway, gut and exocrine glands, where it is responsible for transepithelial salt and water transport.",
          "PMID": 28869532
        },
        "100": {
          "Verbs": "activate expres",
          "categorical_verbs": "activate activate",
          "sentences": "Airway epithelial cells express both Ca2+ activated TMEM16A/ANO1 and cAMP activated CFTR anion channels.",
          "PMID": 29331508
        },
        "101": {
          "Verbs": "activate translocate containe produce",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "GPCRs translocate Ca2+-sensitive adenylate cyclase type 1 (ADCY1) and exchange protein directly activated by cAMP (EPAC1) to particular plasma membrane domains containing GPCRs, CFTR and TMEM16A, thereby producing compartmentalized Ca2+ and cAMP signals and significant crosstalk.",
          "PMID": 29331508
        },
        "102": {
          "Verbs": "regulates is include",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-regulated, apical anion channel that regulates ion and fluid transport in many epithelia including the airways.",
          "PMID": 30547226
        },
        "103": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "104": {
          "Verbs": "increase constitute was",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "RESULTS: Among the WNK1, SPAK, and OSR1 kinases that constitute a [Cl-]i-sensitive kinase cascade, the expression of WNK1 alone was sufficient to increase the CFTR bicarbonate permeability (PHCO3/PCl) and conductance (GHCO3) in patch clamp recordings.",
          "PMID": 31561038
        },
        "105": {
          "Verbs": "reduce hampere",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Furthermore, the pancreatitis-causing R74Q and R75Q mutations in the elbow helix 1 of CFTR hampered WNK1-CFTR physical associations and reduced WNK1-mediated CFTR PHCO3/PCl regulation.",
          "PMID": 31561038
        },
        "106": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "Spatiotemporal coupling of cAMP transporter to CFTR chloride channel function in the gut epithelia.",
          "PMID": 18045536
        },
        "107": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "Spatiotemporal coupling of cAMP transporter to CFTR chloride channel function in \nthe gut epithelia.",
          "PMID": 18045536
        },
        "108": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "The cystic-fibrosis transmembrane conductance regulator (CFTR) functions as a\ncAMP-regulated Cl- channel and as a regulator of other membrane conductances.",
          "PMID": 10220462
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "neutral",
      "neutral",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "activate",
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "inhibit",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "inhibit",
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "neutral",
      "undefined",
      "undefined",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "activate",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "inhibit",
      "activate",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "inhibit",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "activate",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CREBAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CREB",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "CREB",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "controll controll includes link",
          "categorical_verbs": "neutral neutral undefined undefined",
          "sentences": "The transduction pathway controlled by these extracellular [HCO(3)(-)] variations includes cAMP production linked to the stimulation of soluble adenylyl cyclase (sAC), and nuclear accumulation of the transcription factor, CREB.",
          "PMID": 18209474
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "Cl-AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Cl-",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "Cl-",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 7,
      "references": {
        "0": {
          "Verbs": "result",
          "categorical_verbs": "undefined",
          "sentences": "Indeed, concomitant mutagenesis of three of the four sites still resulted in cAMP-responsive Cl- channel activity.",
          "PMID": 1716180
        },
        "1": {
          "Verbs": "activate regulate be is known",
          "categorical_verbs": "activate neutral undefined undefined undefined",
          "sentences": "Other than the fact that the cystic fibrosis transmembrane conductance regulator  (CFTR) Cl- channel can be activated by cAMP dependent kinase (PKA), little is known about the signal transduction pathways regulating CFTR.",
          "PMID": 11155209
        },
        "2": {
          "Verbs": "express is are seen",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl- channel expressed in a wide variety of epithelial cells, mutations of which are responsible for hallmark defective Cl- and HCO3- secretion seen in cystic fibrosis (CF).",
          "PMID": 16414184
        },
        "3": {
          "Verbs": "is cause",
          "categorical_verbs": "undefined undefined",
          "sentences": "Of particular importance is the cAMP-dependent cystic fibrosis transmembrane conductance regulator Cl- channel (CFTR) with mutations of the CFTR encoding gene causing cystic fibrosis.",
          "PMID": 27092946
        },
        "4": {
          "Verbs": "is line",
          "categorical_verbs": "undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP- and cGMP-regulated chloride (Cl-) and bicarbonate (HCO₃-) channel localized primarily at the apical plasma membrane of epithelial cells lining the airway, gut and exocrine glands, where it is responsible for transepithelial salt and water transport.",
          "PMID": 28869532
        },
        "5": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "6": {
          "Verbs": "increase constitute was",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "RESULTS: Among the WNK1, SPAK, and OSR1 kinases that constitute a [Cl-]i-sensitive kinase cascade, the expression of WNK1 alone was sufficient to increase the CFTR bicarbonate permeability (PHCO3/PCl) and conductance (GHCO3) in patch clamp recordings.",
          "PMID": 31561038
        },
        "7": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "The cystic-fibrosis transmembrane conductance regulator (CFTR) functions as a\ncAMP-regulated Cl- channel and as a regulator of other membrane conductances.",
          "PMID": 10220462
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "EHFAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "EHF",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "EHF",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "have reviewe obtaine were stratify",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "We have reviewed transcriptome data obtained from intestinal epithelial samples of homozygotes for c.1521_1523delCTT in CFTR, which were stratified for their EHF genetic background.",
          "PMID": 24105369
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "G-proteinAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "G-protein",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "G-protein",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        },
        "2": {
          "Verbs": "induce was compare",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "The magnitude of mutant CFTR G(Cl) activation by G-proteins was smaller as compared to non-CF ducts but comparable to that induced by cAMP in CF ducts.",
          "PMID": 11155209
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "GDPAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "GDP",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "GDP",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibite stimulate express increase perform demonstrate",
          "categorical_verbs": "inhibit neutral activate activate undefined undefined",
          "sentences": "CFTR function  assays performed on T84 cells expressing the Rab11a or Rab11b GDP-locked S25N mutants demonstrated that only the Rab11b mutant inhibited 80% of the cAMP-activated halide efflux and that only the constitutively active Rab11b-Q70L  increased the rate constant for stimulated halide efflux.",
          "PMID": 19244346
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "GTPAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "GTP",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "GTP",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "HCO3-AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "HCO3-",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "HCO3-",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "express is are seen",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl- channel expressed in a wide variety of epithelial cells, mutations of which are responsible for hallmark defective Cl- and HCO3- secretion seen in cystic fibrosis (CF).",
          "PMID": 16414184
        },
        "1": {
          "Verbs": "block block show posses be known",
          "categorical_verbs": "inhibit inhibit undefined undefined undefined undefined",
          "sentences": "Our recent results show that endometrial epithelial cells possess a cAMP-activated HCO3- transport mechanism, which could be impaired with  channel blockers known to block CFTR or antisense against CFTR.",
          "PMID": 16414184
        },
        "2": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "KCNN4AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "KCNN4",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "KCNN4",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate potentiate demonstrate",
          "categorical_verbs": "activate activate undefined",
          "sentences": "RESULTS: Studies in control tissues demonstrate that 1-EBIO activated CFTR-mediated Cl⁻ secretion in the absence of cAMP-mediated stimulation and potentiated cAMP-induced Cl⁻ secretion by 39.2±6.7% (P<0.001) via activation of basolateral Ca²⁺-activated and clotrimazole-sensitive KCNN4 K⁺ channels.",
          "PMID": 21909392
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "Na+AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Na+",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "Na+",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibit (",
          "categorical_verbs": "inhibit undefined",
          "sentences": "cAMP-dependent activation of CFTR inhibits epithelial Na+ channels (ENaC).",
          "PMID": 10220462
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "PDE4DAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "PDE4D",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "PDE4D",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "associate associate found recruit was be precludes",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined undefined",
          "sentences": "Furthermore whereas EBP50 recruits a cAMP-dependent protein kinase (PKA) complex to CFTR, Shank2 was found to be physically and functionally associated with the cyclic nucleotide phosphodiesterase PDE4D that precludes cAMP/PKA signals in epithelial cells and mouse brains.",
          "PMID": 17244609
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "PDZK1AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "PDZK1",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "PDZK1",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "suggest occur form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "CFTR single-channel recordings and FRET-based intracellular cAMP dynamics suggest that a compartmentalized coupling of cAMP transporter and CFTR occurs via the PDZ scaffolding protein, PDZK1, forming a macromolecular complex at apical surfaces of gut epithelia.",
          "PMID": 18045536
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "RAB11BAMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "RAB11B",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "RAB11B",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibite stimulate express increase perform demonstrate",
          "categorical_verbs": "inhibit neutral activate activate undefined undefined",
          "sentences": "CFTR function  assays performed on T84 cells expressing the Rab11a or Rab11b GDP-locked S25N mutants demonstrated that only the Rab11b mutant inhibited 80% of the cAMP-activated halide efflux and that only the constitutively active Rab11b-Q70L  increased the rate constant for stimulated halide efflux.",
          "PMID": 19244346
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "RACK1AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "RACK1",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "RACK1",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibit block bind block affect embedd pull do",
          "categorical_verbs": "inhibit inhibit neutral inhibit neutral undefined undefined undefined",
          "sentences": "An internal 11-amino acid motif embedding the GYGF carboxylate binding loop of PDZ1 binds to RACK1, inhibits binding of recombinant NHERF1 and RACK1, pulls down endogenous RACK1 from Calu-3 cell lysate, and blocks coimmunoprecipitation of endogenous RACK1 with endogenous NHERF1 but does not affect cAMP-dependent activation of CFTR.",
          "PMID": 15075202
        },
        "1": {
          "Verbs": "affect is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CFTR activation by cAMP-generating agent is not  affected by loss of RACK1-NHERF1 interaction.",
          "PMID": 15075202
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "S100A10AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "S100A10",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "S100A10",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "The formation of the cAMP/protein kinase A-dependent annexin 2-S100A10 complex with cystic fibrosis conductance regulator protein (CFTR) regulates CFTR channel function.",
          "PMID": 17581860
        },
        "1": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "The formation of the cAMP/protein kinase A-dependent annexin 2-S100A10 complex with cystic fibrosis conductance regulator protein (CFTR) regulates CFTR channel  function.",
          "PMID": 17581860
        },
        "2": {
          "Verbs": "report",
          "categorical_verbs": "undefined",
          "sentences": "We report  that annexin 2 (anx 2)-S100A10 forms a functional cAMP/PKA/calcineurin (CaN)-dependent complex with CFTR.",
          "PMID": 17581860
        },
        "3": {
          "Verbs": "demonstrate",
          "categorical_verbs": "undefined",
          "sentences": "We recently demonstrated a cAMP/PKA/calcineurin (CnA)-driven association between annexin 2 (anx 2), its cognate partner -S100A10 and cell surface CFTR.",
          "PMID": 18346874
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "SHANK2AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "SHANK2",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "SHANK2",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "suppress",
          "categorical_verbs": "inhibit",
          "sentences": "Of interest, expression of Shank2 suppressed cAMP-induced phosphorylation and activation of CFTR.",
          "PMID": 14679199
        },
        "1": {
          "Verbs": "associate associate found recruit was be precludes",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined undefined",
          "sentences": "Furthermore whereas EBP50 recruits a cAMP-dependent protein kinase (PKA) complex to CFTR, Shank2 was found to be physically and functionally associated with the cyclic nucleotide phosphodiesterase PDE4D that precludes cAMP/PKA signals in epithelial cells and mouse brains.",
          "PMID": 17244609
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "SLC26A9AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "SLC26A9",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "SLC26A9",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "block associate block increase associate is",
          "categorical_verbs": "inhibit neutral inhibit activate neutral undefined",
          "sentences": "Elevation of intracellular cAMP (a CFTR activator) is also ineffective whereas increasing intracellular calcium blocks the SLC26A9 associated currents.",
          "PMID": 18769029
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "USP19AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "USP19",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "USP19",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "is involv",
          "categorical_verbs": "undefined undefined",
          "sentences": "Thus, USP19 is the first example of a membrane-anchored DUB involved in the turnover of ERAD substrates.",
          "PMID": 19465887
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "WNK1AMP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "WNK1",
      "target": "AMP",
      "bendPointPositions": [],
      "portSource": "WNK1",
      "portTarget": "AMP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "1": {
          "Verbs": "increase constitute was",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "RESULTS: Among the WNK1, SPAK, and OSR1 kinases that constitute a [Cl-]i-sensitive kinase cascade, the expression of WNK1 alone was sufficient to increase the CFTR bicarbonate permeability (PHCO3/PCl) and conductance (GHCO3) in patch clamp recordings.",
          "PMID": 31561038
        },
        "2": {
          "Verbs": "reduce hampere",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Furthermore, the pancreatitis-causing R74Q and R75Q mutations in the elbow helix 1 of CFTR hampered WNK1-CFTR physical associations and reduced WNK1-mediated CFTR PHCO3/PCl regulation.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADCY1ANO1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADCY1",
      "target": "ANO1",
      "bendPointPositions": [],
      "portSource": "ADCY1",
      "portTarget": "ANO1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "Compartmentalize",
          "categorical_verbs": "undefined",
          "sentences": "Compartmentalized crosstalk of CFTR and TMEM16A (ANO1) through EPAC1 and ADCY1.",
          "PMID": 29331508
        },
        "1": {
          "Verbs": "Compartmentalize",
          "categorical_verbs": "undefined",
          "sentences": "Compartmentalized crosstalk of CFTR and TMEM16A (ANO1) through EPAC1 and ADCY1.",
          "PMID": 29331508
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRANO1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "ANO1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "ANO1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate expres",
          "categorical_verbs": "activate activate",
          "sentences": "Airway epithelial cells express both Ca2+ activated TMEM16A/ANO1 and cAMP activated CFTR anion channels.",
          "PMID": 29331508
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRAP2",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "AP2",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "AP2",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "modulate indicate modify",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Our findings indicate that direct interacters with CFTR, such as SNAP23, PPP2R4 and PPP2R1A, may modify the residual function of p.Phe508del-CFTR while variants in KRT19 may modulate the amount of p.Phe508del-CFTR at the apical membrane and consequently modify CF disease.",
          "PMID": 22892532
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ADOATP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADO",
      "target": "ATP",
      "bendPointPositions": [],
      "portSource": "ADO",
      "portTarget": "ATP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADPATP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADP",
      "target": "ATP",
      "bendPointPositions": [],
      "portSource": "ADP",
      "portTarget": "ATP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "AMPATP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "ATP",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "ATP",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        },
        "1": {
          "Verbs": "support is enhanc",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Interestingly, although ATP supports GSH flux through CFTR, this activity is enhanced in the presence of the non-hydrolyzable ATP analog AMP-PNP.",
          "PMID": 12727866
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRATP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "ATP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "ATP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "regulates present short-circuit culture triggere",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "We present results from whole-cell and single-channel patch-clamp recordings, short-circuit current recordings, and [gamma-32P]ATP release assays of normal, CF, and wild-type or mutant CFTR-transfected CF airway cultured epithelial cells wherein CFTR regulates ORCCs by triggering the transport of the potent agonist, ATP, out of the cell.",
          "PMID": 7541313
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "UBC9ATP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "UBC9",
      "target": "ATP",
      "bendPointPositions": [],
      "portSource": "UBC9",
      "portTarget": "ATP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "increase decrease reduce target indicate ) reflect was followe solubilize stabilize",
          "categorical_verbs": "activate inhibit inhibit neutral undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "Several findings indicated that Hsp27-Ubc9 targets the SUMOylation of a transitional, non-native conformation of F508del NBD1: (a) its modification decreased as [ATP] increased,  reflecting stabilization of the nucleotide-binding domain by ligand binding; (b)  a temperature-induced increase in intrinsic fluorescence, which reflects formation of a transitional NBD1 conformation, was followed by its SUMO modification; and (c) introduction of solubilizing or revertant mutations to stabilize F508del NBD1 reduced its SUMO modification.",
          "PMID": 26627832
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADPBAG1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADP",
      "target": "BAG1",
      "bendPointPositions": [],
      "portSource": "ADP",
      "portTarget": "BAG1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "accelerate include are known",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Members of the BAG family of co-chaperones, including Bag1 and Bag3, are known to accelerate release of both ADP and client from Hsc70.",
          "PMID": 27474739
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRBAP31",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "BAP31",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "BAP31",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "increase enabl",
          "categorical_verbs": "activate undefined",
          "sentences": "Antisense inhibition of BAP31 in various cell types increased expression of both wild-type CFTR and [DeltaPhe(508)]CFTR and enabled cAMP-activated Cl(-) currents in [DeltaPhe(508)]CFTR-expressing CHO cells.",
          "PMID": 11274174
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AMPCALU",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "CALU",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "CALU",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibite polarize",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Pharmacologic activation  of AMPK inhibited forskolin-stimulated CFTR short circuit currents in polarized Calu-3 cell monolayers.",
          "PMID": 12427743
        },
        "1": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "In whole-cell patch clamp experiments, the activation of  endogenous AMPK either pharmacologically or by the overexpression of an AMPK-activating non-catalytic subunit mutant (AMPK-gamma1-R70Q) dramatically inhibited forskolin-stimulated CFTR conductance in Calu-3 and CFTR-expressing Chinese hamster ovary cells.",
          "PMID": 12427743
        },
        "2": {
          "Verbs": "reduce was transfect close",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "In contrast, the single channel open probability of CFTR was strongly reduced in cell-attached patch clamp measurements of Calu-3 cells transfected with the AMPK-activating mutant, an effect due primarily to a substantial prolongation of the mean closed  time of the channel.",
          "PMID": 12427743
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "inhibit",
      "inhibit",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRCALU",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "CALU",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "CALU",
      "review_status": "to_review",
      "width": 17,
      "references": {
        "0": {
          "Verbs": "activate bind demonstrate",
          "categorical_verbs": "activate neutral undefined",
          "sentences": "Electrophoretic mobility shift assays demonstrate that CRE-binding protein (CREB) binds to the CFTR CRE with high affinity and independently of the adjacent Y box and that the  CFTR CRE binds CREB and activating transcription factor-1 in nuclear extracts of  T84 and CaLu-3 cells.",
          "PMID": 8943230
        },
        "1": {
          "Verbs": "found contain is phosphorylate",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "We found that CFTR immunoprecipitates from Calu-3 airway cells contain endogenous PKA, which is capable of phosphorylating CFTR.",
          "PMID": 10799517
        },
        "2": {
          "Verbs": "reduces show do",
          "categorical_verbs": "inhibit undefined undefined",
          "sentences": "Whole-cell patch clamp of Calu-3 cells shows that Ht31 peptide reduces cAMP-stimulated CFTR Cl(-) current, but Ht31P does not.",
          "PMID": 10799517
        },
        "3": {
          "Verbs": "is",
          "categorical_verbs": "undefined",
          "sentences": "In contrast to EBP50/NHERF, E3KARP is predominantly localized (>95%) in the membrane fractions of Calu-3 and T84 cells, where CFTR is located.",
          "PMID": 10893422
        },
        "4": {
          "Verbs": "polarize show are co-localize",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Moreover, confocal immunofluorescence microscopy of polarized Calu-3 monolayers shows that E3KARP and CFTR are co-localized at the apical membrane domain.",
          "PMID": 10893422
        },
        "5": {
          "Verbs": "activate found was use compartmentalize",
          "categorical_verbs": "activate neutral undefined undefined undefined",
          "sentences": "When cystic fibrosis transmembrane conductance regulator (CFTR) channel activity was used as  a functional readout, we found signaling elements compartmentalized at both extracellular and intracellular surfaces of the apical cell membrane that activate apical Cl(-) conductance in Calu-3 cells.",
          "PMID": 11707576
        },
        "6": {
          "Verbs": "expres has been demonstrate include",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Protein kinase C (PKC) regulation of cystic fibrosis transmembrane regulator (CFTR) chloride function has been demonstrated in several cell lines, including Calu-3 cells that express native, wild-type CFTR.",
          "PMID": 11956211
        },
        "7": {
          "Verbs": "reveale",
          "categorical_verbs": "undefined",
          "sentences": "Immunofluorescence and confocal microscopy of RACK1 and CFTR revealed colocalization of the proteins to the apical and lateral regions of Calu-3 cells.",
          "PMID": 11956211
        },
        "8": {
          "Verbs": "colocalize",
          "categorical_verbs": "undefined",
          "sentences": "In Calu-3 airway cells, immunofluorescence colocalized Csp with calnexin in the endoplasmic reticulum and with CFTR at the apical membrane domain.",
          "PMID": 12039948
        },
        "9": {
          "Verbs": "coprecipitate",
          "categorical_verbs": "undefined",
          "sentences": "CFTR coprecipitated with Csp from Calu-3 cell lysates.",
          "PMID": 12039948
        },
        "10": {
          "Verbs": "associate express associate glycosylate ( predominate",
          "categorical_verbs": "neutral activate neutral undefined undefined undefined",
          "sentences": "Csp associated with both core-glycosylated immature and fully glycosylated mature CFTRs (bands B and C); however, in relation to the endogenous levels of the B and C bands expressed in Calu-3 cells, the Csp interaction with band B predominated.",
          "PMID": 12039948
        },
        "11": {
          "Verbs": "inhibite polarize",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Pharmacologic activation  of AMPK inhibited forskolin-stimulated CFTR short circuit currents in polarized Calu-3 cell monolayers.",
          "PMID": 12427743
        },
        "12": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "In whole-cell patch clamp experiments, the activation of  endogenous AMPK either pharmacologically or by the overexpression of an AMPK-activating non-catalytic subunit mutant (AMPK-gamma1-R70Q) dramatically inhibited forskolin-stimulated CFTR conductance in Calu-3 and CFTR-expressing Chinese hamster ovary cells.",
          "PMID": 12427743
        },
        "13": {
          "Verbs": "reduce was transfect close",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "In contrast, the single channel open probability of CFTR was strongly reduced in cell-attached patch clamp measurements of Calu-3 cells transfected with the AMPK-activating mutant, an effect due primarily to a substantial prolongation of the mean closed  time of the channel.",
          "PMID": 12427743
        },
        "14": {
          "Verbs": "inhibit block bind block affect embedd pull do",
          "categorical_verbs": "inhibit inhibit neutral inhibit neutral undefined undefined undefined",
          "sentences": "An internal 11-amino acid motif embedding the GYGF carboxylate binding loop of PDZ1 binds to RACK1, inhibits binding of recombinant NHERF1 and RACK1, pulls down endogenous RACK1 from Calu-3 cell lysate, and blocks coimmunoprecipitation of endogenous RACK1 with endogenous NHERF1 but does not affect cAMP-dependent activation of CFTR.",
          "PMID": 15075202
        },
        "15": {
          "Verbs": "polarize ( form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Endogenous, apical membrane CFTR in polarized human airway epithelial cells (Calu-3) formed a complex with myosin VI, the myosin VI adaptor protein Disabled 2 (Dab2), and clathrin.",
          "PMID": 15247260
        },
        "16": {
          "Verbs": "localize",
          "categorical_verbs": "undefined",
          "sentences": "PP2A subunits localize to the apical surface of airway epithelia and PP2A phosphatase activity co-purifies with CFTR in Calu-3 cells.",
          "PMID": 16239222
        },
        "17": {
          "Verbs": "Looke investigate",
          "categorical_verbs": "undefined undefined",
          "sentences": "Looking for a control of CFTR expression by ionic conditions, we investigated the effect of altered extracellular bicarbonate ion concentration on CFTR expression  in human pulmonary Calu-3 cells.",
          "PMID": 18209474
        },
        "18": {
          "Verbs": "reduce show transfect culture disrupt",
          "categorical_verbs": "inhibit undefined undefined undefined undefined",
          "sentences": "We show that FlnA-Ig repeats transfected into cultured Calu-3 cells disrupt CFTR-filamin interaction and reduce surface levels of CFTR.",
          "PMID": 20351101
        },
        "19": {
          "Verbs": "inhibit increases decrease reduce Us show leade",
          "categorical_verbs": "inhibit activate inhibit inhibit undefined undefined undefined",
          "sentences": "Using a combination of steady state and pulse-chase analyses, we show that FKBP38 knockdown increases protein synthesis but inhibits the post-translational folding of CFTR, leading to reduced steady state levels of CFTR in the ER, decreased processing, and impaired cell surface functional expression in Calu-3 human airway epithelial cells.",
          "PMID": 22030396
        },
        "20": {
          "Verbs": "associate associate",
          "categorical_verbs": "neutral neutral",
          "sentences": "Proteomic identification of calumenin as a G551D-CFTR associated protein.",
          "PMID": 22768251
        },
        "21": {
          "Verbs": "associate associate",
          "categorical_verbs": "neutral neutral",
          "sentences": "Proteomic identification of calumenin as a G551D-CFTR associated protein.",
          "PMID": 22768251
        },
        "22": {
          "Verbs": "reveale was link",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Mass Spectrometry revealed that calumenin was present in  the protein complex linked to G551D-CFTR.",
          "PMID": 22768251
        },
        "23": {
          "Verbs": "found was suggest is involv mutate",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined",
          "sentences": "Because in our cellular model calumenin was found in the endoplasmic reticulum (ER) by immunofluorescence experiments, we suggest that calumenin is likely involved in the mutated CFTR's maturation.",
          "PMID": 22768251
        },
        "24": {
          "Verbs": "bind increase showe is",
          "categorical_verbs": "neutral activate undefined undefined",
          "sentences": "In conclusion, we showed for the first time that calumenin binds to CFTR and that it is increased in the G551D-CFTR complex.",
          "PMID": 22768251
        },
        "25": {
          "Verbs": "propose is",
          "categorical_verbs": "undefined undefined",
          "sentences": "Finally, we propose here that Calumenin is  a new CFTR chaperone.",
          "PMID": 22768251
        },
        "26": {
          "Verbs": "increase reduce was prevent",
          "categorical_verbs": "activate inhibit undefined undefined",
          "sentences": "In bronchial and primary airway cells CFTR mRNA expression was also reduced, whereas channel activity was  increased which was prevented by LY-294002 in Calu-3 cells.",
          "PMID": 25910246
        },
        "27": {
          "Verbs": "express contributes mutate",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Calumenin contributes to ER-Ca<sup>2+</sup> homeostasis in bronchial epithelial cells expressing WT and F508del mutated CFTR and to F508del-CFTR retention.",
          "PMID": 28189267
        },
        "28": {
          "Verbs": "express contributes mutate",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Epub 2017 Feb 4.  Calumenin contributes to ER-Ca2+ homeostasis in bronchial epithelial cells expressing WT and F508del mutated CFTR and to F508del-CFTR retention.",
          "PMID": 28189267
        },
        "29": {
          "Verbs": "reduce result highlight",
          "categorical_verbs": "inhibit undefined undefined",
          "sentences": "In addition, reducing Calumenin expression in CF cells results in a partial restoration of CFTR activity, highlighting a potential function of Calumenin in CFTR maturation.",
          "PMID": 28189267
        },
        "30": {
          "Verbs": "associate associate Us deplete ( identify",
          "categorical_verbs": "neutral neutral undefined undefined undefined undefined",
          "sentences": "Using an siRNA screen to deplete ∼1500 transcription factors (TFs) and associated regulatory proteins in Calu-3 lung epithelial cells, we identified nearly 40 factors that upon depletion elevated CFTR mRNA levels more than 2-fold.",
          "PMID": 29572268
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "inhibit",
      "inhibit",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "inhibit",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CREBCALU",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CREB",
      "target": "CALU",
      "bendPointPositions": [],
      "portSource": "CREB",
      "portTarget": "CALU",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate bind demonstrate",
          "categorical_verbs": "activate neutral undefined",
          "sentences": "Electrophoretic mobility shift assays demonstrate that CRE-binding protein (CREB) binds to the CFTR CRE with high affinity and independently of the adjacent Y box and that the  CFTR CRE binds CREB and activating transcription factor-1 in nuclear extracts of  T84 and CaLu-3 cells.",
          "PMID": 8943230
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRCBL",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "CBL",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "CBL",
      "review_status": "to_review",
      "width": 4,
      "references": {
        "0": {
          "Verbs": "co-immunoprecipitate differentiate culture",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "c-Cbl co-immunoprecipitated with  CFTR in primary differentiated human bronchial epithelial cells and in cultured human airway cells.",
          "PMID": 20525683
        },
        "1": {
          "Verbs": "inhibite increase interfere",
          "categorical_verbs": "inhibit activate neutral",
          "sentences": "Small interfering RNA-mediated silencing of c-Cbl increased CFTR expression in the plasma membrane by inhibiting CFTR endocytosis and increased CFTR-mediated Cl(-) currents.",
          "PMID": 20525683
        },
        "2": {
          "Verbs": "did change",
          "categorical_verbs": "undefined undefined",
          "sentences": "Silencing c-Cbl did not change the expression of the ubiquitinated fraction of plasma membrane CFTR.",
          "PMID": 20525683
        },
        "3": {
          "Verbs": "affect did",
          "categorical_verbs": "neutral undefined",
          "sentences": "Moreover, the c-Cbl mutant with impaired ubiquitin ligase activity (FLAG-70Z-Cbl) did not affect the plasma membrane expression or the endocytosis of CFTR.",
          "PMID": 20525683
        },
        "4": {
          "Verbs": "interfere truncate had",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "In contrast, the c-Cbl mutant with the truncated C-terminal region (FLAG-Cbl-480), responsible for protein adaptor function, had a dominant interfering effect on the endocytosis and plasma membrane expression of CFTR.",
          "PMID": 20525683
        },
        "5": {
          "Verbs": "reduce co-immunoprecipitate",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Moreover, CFTR and c-Cbl co-localized and co-immunoprecipitated in early endosomes, and silencing c-Cbl reduced the amount of ubiquitinated CFTR in early endosomes.",
          "PMID": 20525683
        },
        "6": {
          "Verbs": "regulates demonstrate act facilitate ubiquitinate",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "In summary, our data demonstrate that in human airway epithelial cells, c-Cbl regulates CFTR by two mechanisms: first by acting as an adaptor protein and facilitating CFTR endocytosis by a ubiquitin-independent mechanism, and second by ubiquitinating CFTR in early endosomes and thereby facilitating the lysosomal degradation of CFTR.",
          "PMID": 20525683
        },
        "7": {
          "Verbs": "test were require siRNA-deplete",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "To test whether two E3 ligases were required for the endocytosis and/or down-regulation of surface CFTR, we siRNA-depleted CHIP [C-terminus of the Hsc (heat-shock cognate) 70-interacting protein] and c-Cbl (casitas B-lineage lymphoma).",
          "PMID": 21995445
        },
        "8": {
          "Verbs": "demonstrate have enhanc",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "We demonstrate that CHIP and c-Cbl depletion have no effect on CFTR endocytosis, but c-Cbl depletion modestly enhanced the half-life of CFTR.",
          "PMID": 21995445
        },
        "9": {
          "Verbs": "had abolish",
          "categorical_verbs": "undefined undefined",
          "sentences": "Although arsenic had no effect on the abundance or activity of USP10, a deubiquitinylating enzyme, siRNA-mediated knockdown of c-Cbl, an E3 ubiquitin ligase, abolished the arsenic-stimulated degradation of CFTR.",
          "PMID": 22467879
        },
        "10": {
          "Verbs": "increase enhanc phosphorylate",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Arsenic enhanced the degradation of CFTR by increasing phosphorylated c-Cbl, which increased its interaction with CFTR, and subsequent ubiquitinylation of CFTR.",
          "PMID": 22467879
        },
        "11": {
          "Verbs": "increase report lead followe result enhanc",
          "categorical_verbs": "activate undefined undefined undefined undefined undefined",
          "sentences": "We report that serine-to-tyrosine mutation leads to increased tyrosine phosphorylation of S1045Y-CFTR, followed by recruitment and binding of E3-ubiquitin ligase c-cbl, resulting in enhanced ubiquitination and passage of S1045Y-CFTR in the endosome/lysosome degradative compartments.",
          "PMID": 27261451
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "USP10CBL",
      "class": "stimulation",
      "cardinality": 0,
      "source": "USP10",
      "target": "CBL",
      "bendPointPositions": [],
      "portSource": "USP10",
      "portTarget": "CBL",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "had abolish",
          "categorical_verbs": "undefined undefined",
          "sentences": "Although arsenic had no effect on the abundance or activity of USP10, a deubiquitinylating enzyme, siRNA-mediated knockdown of c-Cbl, an E3 ubiquitin ligase, abolished the arsenic-stimulated degradation of CFTR.",
          "PMID": 22467879
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADCY1CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADCY1",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ADCY1",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "Compartmentalize",
          "categorical_verbs": "undefined",
          "sentences": "Compartmentalized crosstalk of CFTR and TMEM16A (ANO1) through EPAC1 and ADCY1.",
          "PMID": 29331508
        },
        "1": {
          "Verbs": "Compartmentalize",
          "categorical_verbs": "undefined",
          "sentences": "Compartmentalized crosstalk of CFTR and TMEM16A (ANO1) through EPAC1 and ADCY1.",
          "PMID": 29331508
        },
        "2": {
          "Verbs": "activate translocate containe produce",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "GPCRs translocate Ca2+-sensitive adenylate cyclase type 1 (ADCY1) and exchange protein directly activated by cAMP (EPAC1) to particular plasma membrane domains containing GPCRs, CFTR and TMEM16A, thereby producing compartmentalized Ca2+ and cAMP signals and significant crosstalk.",
          "PMID": 29331508
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADOCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADO",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ADO",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        },
        "1": {
          "Verbs": "activate indicate are is",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Together, these findings indicate that ADO and its nucleotides are capable of activating wtCFTR-dependent  halide permeability through A2B AR and that this AR subtype is present in human bronchial epithelium.",
          "PMID": 9950763
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ADPCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADP",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ADP",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        },
        "1": {
          "Verbs": "inhibit Block Block express",
          "categorical_verbs": "inhibit inhibit inhibit activate",
          "sentences": "Blocking COPI recruitment to membranes by expressing an inactive form of the GBF1 guanine nucleotide exchange  factor for ADP-ribosylation factor inhibits CFTR trafficking to the PM.",
          "PMID": 17932045
        },
        "2": {
          "Verbs": "inhibite express disrupt",
          "categorical_verbs": "inhibit activate undefined",
          "sentences": "Similarly, inhibiting COPI dissociation from membranes by expressing a constitutively active ADP-ribosylation factor 1 mutant arrests CFTR within disrupted Golgi elements.",
          "PMID": 17932045
        },
        "3": {
          "Verbs": "stimulates accelerate was destabilize",
          "categorical_verbs": "neutral activate undefined undefined",
          "sentences": "Hsc70 binding to CFTR was destabilized by the C-terminal domain of Bag-1 (CBag), which stimulates client release by accelerating ADP-ATP exchange.",
          "PMID": 21697503
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "inhibit",
      "activate",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AMPCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 10,
      "references": {
        "0": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        },
        "1": {
          "Verbs": "inhibit bind ( phosphorylates co-localizes",
          "categorical_verbs": "inhibit neutral undefined undefined undefined",
          "sentences": "The metabolic sensor AMP-activated kinase (AMPK) binds to and phosphorylates CFTR, co-localizes with it in various tissues, and inhibits CFTR currents in Xenopus oocytes (Hallows, K. R., Raghuram, V., Kemp, B. E., Witters, L. A. & Foskett, J. K. (2000) J. Clin.",
          "PMID": 12427743
        },
        "2": {
          "Verbs": "inhibite polarize",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Pharmacologic activation  of AMPK inhibited forskolin-stimulated CFTR short circuit currents in polarized Calu-3 cell monolayers.",
          "PMID": 12427743
        },
        "3": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "In whole-cell patch clamp experiments, the activation of  endogenous AMPK either pharmacologically or by the overexpression of an AMPK-activating non-catalytic subunit mutant (AMPK-gamma1-R70Q) dramatically inhibited forskolin-stimulated CFTR conductance in Calu-3 and CFTR-expressing Chinese hamster ovary cells.",
          "PMID": 12427743
        },
        "4": {
          "Verbs": "affect assess was",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Plasma membrane expression of CFTR, assessed by surface biotinylation, was not affected by AMPK activation.",
          "PMID": 12427743
        },
        "5": {
          "Verbs": "reduce was transfect close",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "In contrast, the single channel open probability of CFTR was strongly reduced in cell-attached patch clamp measurements of Calu-3 cells transfected with the AMPK-activating mutant, an effect due primarily to a substantial prolongation of the mean closed  time of the channel.",
          "PMID": 12427743
        },
        "6": {
          "Verbs": "be link",
          "categorical_verbs": "undefined undefined",
          "sentences": "As a metabolic sensor in cells, AMPK may be important in tuning CFTR activity to cellular energy charge, thereby linking transepithelial transport and the maintenance of cellular ion gradients to cellular metabolism.",
          "PMID": 12427743
        },
        "7": {
          "Verbs": "polarize",
          "categorical_verbs": "undefined",
          "sentences": "Physiological modulation of CFTR activity by AMP-activated protein kinase in polarized T84 cells.",
          "PMID": 12519745
        },
        "8": {
          "Verbs": "polarize",
          "categorical_verbs": "undefined",
          "sentences": "2003 May;284(5):C1297-308. Epub 2003 Jan 2.  Physiological modulation of CFTR activity by AMP-activated protein kinase in polarized T84 cells.",
          "PMID": 12519745
        },
        "9": {
          "Verbs": "inhibite found coexpress interact identify phosphorylate",
          "categorical_verbs": "inhibit neutral activate neutral undefined undefined",
          "sentences": "We previously identified the metabolic sensor AMP-activated protein kinase (AMPK) as a novel protein interacting with CFTR and  found that AMPK phosphorylated CFTR and inhibited CFTR-dependent whole cell conductances when coexpressed with CFTR in Xenopus oocytes.",
          "PMID": 12519745
        },
        "10": {
          "Verbs": "addres have study polarize evaluate measure",
          "categorical_verbs": "undefined undefined undefined undefined undefined undefined",
          "sentences": "To address the physiological relevance of the CFTR-AMPK interaction, we have now studied polarized epithelia and have evaluated the localization of endogenous AMPK and CFTR and measured CFTR activity with modulation of AMPK activity.",
          "PMID": 12519745
        },
        "11": {
          "Verbs": "share overlapp include",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "By immunofluorescent imaging, AMPK and CFTR share an overlapping apical distribution in several rat epithelial tissues, including nasopharynx, submandibular gland, pancreas, and ileum.",
          "PMID": 12519745
        },
        "12": {
          "Verbs": "modulate ( were measure polarize grown use",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined",
          "sentences": "CFTR-dependent short-circuit currents (I(sc)) were measured  in polarized T84 cells grown on permeable supports, and several independent methods were used to modulate endogenous AMPK activity.",
          "PMID": 12519745
        },
        "13": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "Activation of endogenous  AMPK with the cell-permeant adenosine analog 5-amino-4-imidazolecarboxamide-1-beta-d-ribofuranoside (AICAR) inhibited forskolin-stimulated CFTR-dependent I(sc) in nonpermeabilized monolayers and monolayers with nystatin permeabilization of the basolateral membrane.",
          "PMID": 12519745
        },
        "14": {
          "Verbs": "inhibite was",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Raising intracellular AMP concentration in monolayers with basolateral membranes permeabilized with alpha-toxin also inhibited CFTR, an effect that was unrelated  to adenosine receptors.",
          "PMID": 12519745
        },
        "15": {
          "Verbs": "enhanc polarize",
          "categorical_verbs": "undefined undefined",
          "sentences": "Finally, overexpression of a kinase-dead mutant AMPK-alpha1 subunit (alpha1-K45R) enhanced forskolin-stimulated I(sc) in polarized T84 monolayers, consistent with a dominant-negative reduction in the inhibition of CFTR by endogenous AMPK.",
          "PMID": 12519745
        },
        "16": {
          "Verbs": "modulate indicate play polarize suggest",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "These results indicate that AMPK plays a physiological role in modulating CFTR activity in polarized epithelia and suggest a novel paradigm for the coupling of ion transport to cellular metabolism.",
          "PMID": 12519745
        },
        "17": {
          "Verbs": "support is enhanc",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Interestingly, although ATP supports GSH flux through CFTR, this activity is enhanced in the presence of the non-hydrolyzable ATP analog AMP-PNP.",
          "PMID": 12727866
        },
        "18": {
          "Verbs": "mediates is",
          "categorical_verbs": "neutral undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cyclic AMP-dependent chloride channel that mediates electrolyte transport across the luminal surface of epithelial cells.",
          "PMID": 15039462
        },
        "19": {
          "Verbs": "express is",
          "categorical_verbs": "activate undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cyclic AMP-regulated Cl(-) channel expressed in the apical plasma membrane in fluid-transporting epithelia.",
          "PMID": 15247260
        },
        "20": {
          "Verbs": "is",
          "categorical_verbs": "undefined",
          "sentences": "jbomberger@dartmouth.edu  The cystic fibrosis transmembrane conductance regulator (CFTR), a member of the ABC transporter superfamily, is a cyclic AMP-regulated chloride channel and a regulator of other ion channels and transporters.",
          "PMID": 19398555
        },
        "21": {
          "Verbs": "regulate is play",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "jbomberger@dartmouth.edu  The Cystic Fibrosis Transmembrane Conductance Regulator (CFTR) is a cyclic AMP-regulated chloride channel that plays an important role in regulating the volume of the lung airway surface liquid, and thereby mucociliary clearance and elimination of pathogens from the lung.",
          "PMID": 20215869
        },
        "22": {
          "Verbs": "interact hamper",
          "categorical_verbs": "neutral undefined",
          "sentences": "The intermediate filament protein keratin 8 (K8) interacts with the nucleotide-binding domain 1 (NBD1) of the cystic fibrosis (CF) transmembrane regulator (CFTR) with phenylalanine 508 deletion (ΔF508), and this interaction hampers the biogenesis of functional ΔF508-CFTR and its insertion into the plasma membrane.",
          "PMID": 27870250
        },
        "23": {
          "Verbs": "increase decrease",
          "categorical_verbs": "activate inhibit",
          "sentences": "AMP-activated protein kinase (AMPK) activity increased, and expression of cystic fibrosis transmembrane conductance regulator (CFTR) decreased, in nutrient-starved cells.",
          "PMID": 29662080
        },
        "24": {
          "Verbs": "increase use",
          "categorical_verbs": "activate undefined",
          "sentences": "Intracellular chloride concentrations and hyperpolarization increased after over-activation of AMPK using the specific activator AICAR or suppression of CFTR activity using specific inhibitor GlyH-101.",
          "PMID": 29662080
        },
        "25": {
          "Verbs": "regulate control control influenc influences reveal",
          "categorical_verbs": "neutral neutral neutral neutral neutral undefined",
          "sentences": "These results reveal that AMPK controls the dynamic change in Vmem by  regulating CFTR and influencing the intracellular chloride concentration, which in turn influences cell-cycle progression.",
          "PMID": 29662080
        },
        "26": {
          "Verbs": "regulate offer underly",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "These findings offer new insights into the mechanisms underlying cell-cycle arrest regulated by AMPK and CFTR.",
          "PMID": 29662080
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "inhibit",
      "undefined",
      "inhibit",
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "inhibit",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "activate",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ANO1CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ANO1",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ANO1",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "Compartmentalize",
          "categorical_verbs": "undefined",
          "sentences": "Compartmentalized crosstalk of CFTR and TMEM16A (ANO1) through EPAC1 and ADCY1.",
          "PMID": 29331508
        },
        "1": {
          "Verbs": "Compartmentalize",
          "categorical_verbs": "undefined",
          "sentences": "Compartmentalized crosstalk of CFTR and TMEM16A (ANO1) through EPAC1 and ADCY1.",
          "PMID": 29331508
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ANXA5CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ANXA5",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ANXA5",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "regulate have implicate",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Previous studies have implicated annexins in regulating ion channels and in particular annexin A5 (AnxA5) in the traffic of the cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 21067452
        },
        "1": {
          "Verbs": "regulate investigate",
          "categorical_verbs": "neutral undefined",
          "sentences": "In the present study, we further investigated the role of AnxA5 in regulating CFTR function and intracellular trafficking in both Xenopus oocytes and mammalian cells.",
          "PMID": 21067452
        },
        "2": {
          "Verbs": "inhibit found confirm report result",
          "categorical_verbs": "inhibit neutral undefined undefined undefined",
          "sentences": "Although we could confirm the previously reported CFTR/AnnxA5 interaction, we found that in oocytes AnxA5 inhibits CFTR-mediated whole-cell membrane conductance presumably by a mechanism independent of PDZ-binding domain at the C-terminus of CFTR but protein kinase C  (PKC)-dependent and results from either endocytosis activation and/or exocytosis  block.",
          "PMID": 21067452
        },
        "3": {
          "Verbs": "augment was",
          "categorical_verbs": "undefined undefined",
          "sentences": "In contrast, in human cells, co-expression of AnxA5 augmented CFTR whole-cell currents, an effect that was independent of CFTR PDZ-binding domain.",
          "PMID": 21067452
        },
        "4": {
          "Verbs": "confirm rescues conclude is",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Finally, we could not confirm that AnxA5 overexpression rescues traffic/function of the most  frequent disease-causing mutant F508del-CFTR, thus concluding that AnxA5 is not a promising tool for correction of the F508del-CFTR defect.",
          "PMID": 21067452
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AP2CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AP2",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "AP2",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "modulate indicate modify",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Our findings indicate that direct interacters with CFTR, such as SNAP23, PPP2R4 and PPP2R1A, may modify the residual function of p.Phe508del-CFTR while variants in KRT19 may modulate the amount of p.Phe508del-CFTR at the apical membrane and consequently modify CF disease.",
          "PMID": 22892532
        },
        "1": {
          "Verbs": "was measure followe knock",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: CFTR short circuit current was measured in intestinal T84 cells following shRNA knock down of AP2α (AP2αKD).",
          "PMID": 28438500
        },
        "2": {
          "Verbs": "tagg was clone determine pull",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "GST tagged human AP2α appendage domain was cloned and its interaction with CFTR determined by GST pull down assay.",
          "PMID": 28438500
        },
        "3": {
          "Verbs": "control increase control result compare delay",
          "categorical_verbs": "neutral activate neutral undefined undefined undefined",
          "sentences": "RESULT: AP2αKD in T84 cells resulted in higher CFTR current (57%) compared to control, consistent with increased functional CFTR and delayed endocytosis.",
          "PMID": 28438500
        },
        "4": {
          "Verbs": "Pull reveale",
          "categorical_verbs": "undefined undefined",
          "sentences": "Pull down assays revealed an interaction between human AP2α appendage domain and CFTR.",
          "PMID": 28438500
        },
        "5": {
          "Verbs": "interact modulates participate",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "CONCLUSION: AP2 α interacts with and modulates CFTR function in the intestine by  participating in clathrin assembly and recruitment of CFTR to CCS.",
          "PMID": 28438500
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ARF1CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ARF1",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ARF1",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "block block followe was SNARE",
          "categorical_verbs": "inhibit inhibit undefined undefined undefined",
          "sentences": "Remarkably, in a cell type-specific manner, processing of  CFTR from the core-glycosylated (band B) ER form to the complex-glycosylated (band C) isoform followed a non-conventional pathway that was insensitive to dominant negative Arf1, Rab1a/Rab2 GTPases, or the SNAp REceptor (SNARE) component syntaxin 5, all of which block the conventional trafficking pathway from the ER to the Golgi.",
          "PMID": 11799116
        },
        "1": {
          "Verbs": "induce reveale was redistribute",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Immunolocalization analyses of mammalian cells  revealed that the Golgi protein GRASP55 was redistributed to the ER by stimuli that induce unconventional secretion of ΔF508-CFTR, such as induction of ER-to-Golgi blockade by the Arf1 mutant.",
          "PMID": 27062250
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ATF6CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATF6",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ATF6",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express increase encode identify is",
          "categorical_verbs": "activate activate activate undefined undefined",
          "sentences": "We identified that SUMOylation of ATF6 is significantly increased in the cells expressing misfolded cystic fibrosis transmembrane conductance regulator (CFTR) encoded by the mutant human CFTR gene  (dF508CFTR).",
          "PMID": 29061306
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ATPCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 16,
      "references": {
        "0": {
          "Verbs": "regulates rectify involv",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "CFTR regulates outwardly rectifying chloride channels through an autocrine mechanism involving ATP.",
          "PMID": 7541313
        },
        "1": {
          "Verbs": "regulates rectify involv",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "CFTR regulates outwardly rectifying chloride channels through an autocrine mechanism involving ATP.",
          "PMID": 7541313
        },
        "2": {
          "Verbs": "regulates present short-circuit culture triggere",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "We present results from whole-cell and single-channel patch-clamp recordings, short-circuit current recordings, and [gamma-32P]ATP release assays of normal, CF, and wild-type or mutant CFTR-transfected CF airway cultured epithelial cells wherein CFTR regulates ORCCs by triggering the transport of the potent agonist, ATP, out of the cell.",
          "PMID": 7541313
        },
        "3": {
          "Verbs": "simplify use demonstrate followe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "In a simplified model using COS-7 cells, we demonstrate acquisition of an ATP-, ADP-, AMP-, and adenosine (ADO)-regulated halide permeability specifically following expression of wild-type (wt) cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 9950763
        },
        "4": {
          "Verbs": "was examine",
          "categorical_verbs": "undefined undefined",
          "sentences": "The specificity of interaction between CFTR and ENaC was examined by coexpression of  ENaC and ATP-binding cassette (ABC) proteins other than CFTR.",
          "PMID": 10220462
        },
        "5": {
          "Verbs": "encodes belong",
          "categorical_verbs": "activate undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) gene encodes a chloride channel protein that belongs to the superfamily of ATP binding cassette  (ABC) transporters.",
          "PMID": 11051556
        },
        "6": {
          "Verbs": "activates",
          "categorical_verbs": "activate",
          "sentences": "Phosphorylation by protein kinase A in the presence of ATP activates the CFTR-mediated chloride conductance of the apical membranes.",
          "PMID": 11051556
        },
        "7": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "8": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        },
        "9": {
          "Verbs": "is require suggest coupl",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "CFTR is unique among ion channels in requiring ATP hydrolysis for its gating, suggesting that its activity is coupled to cellular metabolic status.",
          "PMID": 12427743
        },
        "10": {
          "Verbs": "is remain",
          "categorical_verbs": "undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated, ATP-gated Cl(-) channel and cellular conductance regulator, but the detailed mechanisms of CFTR regulation and its regulation of other transport  proteins remain obscure.",
          "PMID": 12519745
        },
        "11": {
          "Verbs": "support is enhanc",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Interestingly, although ATP supports GSH flux through CFTR, this activity is enhanced in the presence of the non-hydrolyzable ATP analog AMP-PNP.",
          "PMID": 12727866
        },
        "12": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "Protein kinase A regulates ATP hydrolysis and dimerization by a CFTR (cystic fibrosis transmembrane conductance regulator) domain.",
          "PMID": 14602047
        },
        "13": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "Protein kinase A regulates ATP hydrolysis and dimerization by a CFTR (cystic fibrosis transmembrane conductance regulator) domain.",
          "PMID": 14602047
        },
        "14": {
          "Verbs": "associate associate Gate is ( requires",
          "categorical_verbs": "neutral neutral undefined undefined undefined undefined",
          "sentences": "Gating of the CFTR Cl- channel is associated with ATP hydrolysis at the nucleotide-binding domains (NBD1, NBD2) and requires PKA (protein kinase A) phosphorylation of the R domain.",
          "PMID": 14602047
        },
        "15": {
          "Verbs": "control control interact suggest hydrolyse",
          "categorical_verbs": "neutral neutral neutral undefined undefined",
          "sentences": "These findings suggest that during the activation of native  CFTR, phosphorylation of the R domain by PKA can control the ability of the NBD1  domain to hydrolyse ATP and to interact with other NBD domains.",
          "PMID": 14602047
        },
        "16": {
          "Verbs": "is",
          "categorical_verbs": "undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is an ATP-gated chloride channel.",
          "PMID": 17194447
        },
        "17": {
          "Verbs": "result",
          "categorical_verbs": "undefined",
          "sentences": "Cystic fibrosis results from mutations in the cystic fibrosis conductance regulator protein (CFTR), a cAMP/protein kinase A (PKA) and ATP-regulated Cl(-) channel.",
          "PMID": 17581860
        },
        "18": {
          "Verbs": "is characterise cause",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Cystic fibrosis (CF) is characterised by impaired epithelial ion transport and is caused by mutations in the cystic fibrosis conductance regulator protein (CFTR),  a cAMP/PKA and ATP-regulated chloride channel.",
          "PMID": 18346874
        },
        "19": {
          "Verbs": "bind stimulate impair modulate inherite",
          "categorical_verbs": "neutral neutral inhibit neutral undefined",
          "sentences": "Mutations in both the N- and C-terminal domains of Aha1 impair its ability  to bind Hsp90 and stimulate its ATPase activity in vitro and impair in vivo the ability of the Hsp90 system to modulate the folding and trafficking of wild-type  and variant (DeltaF508) cystic fibrosis transmembrane conductance regulator (CFTR) responsible for the inherited disease cystic fibrosis (CF).",
          "PMID": 20089831
        },
        "20": {
          "Verbs": "increases decreases",
          "categorical_verbs": "activate inhibit",
          "sentences": "Stimulation of β2-adrenergic receptor increases CFTR function and decreases ATP levels in murine hematopoietic stem/progenitor cells.",
          "PMID": 25178873
        },
        "21": {
          "Verbs": "increases decreases",
          "categorical_verbs": "activate inhibit",
          "sentences": "Stimulation of β2-adrenergic receptor increases CFTR function and decreases ATP levels in murine hematopoietic stem/progenitor cells.",
          "PMID": 25178873
        },
        "22": {
          "Verbs": "were evaluate",
          "categorical_verbs": "undefined undefined",
          "sentences": "HSPCs were evaluated for ATP content and CFTR activity by means of luminometric and spectrofluorometric methods, respectively, upon stimulation with salbutamol.",
          "PMID": 25178873
        },
        "23": {
          "Verbs": "reduce were was revers",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Intracellular ATP levels were reduced by salbutamol stimulation and this effect was reversed when ICI 118,551 or CFTR inhibitors were present.",
          "PMID": 25178873
        },
        "24": {
          "Verbs": "regulate is determine",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "CONCLUSIONS: In HSPCs, CFTR is regulated by β2-adrenergic receptor stimulation determining intracellular ATP depletion.",
          "PMID": 25178873
        },
        "25": {
          "Verbs": "increases reduce showe ( were compare correct",
          "categorical_verbs": "activate inhibit undefined undefined undefined undefined undefined",
          "sentences": "We also showed that ER F508del-CFTR retention increases SERCA (Sarcoplasmic/Reticulum Ca2+ ATPase) pump  activity whereas PMCA (Plasma Membrane Ca2+ ATPase) activities were reduced in these CF cells compared to corrected CF cells (VX-809) and non-CF cells.",
          "PMID": 25661196
        },
        "26": {
          "Verbs": "regulate suggest",
          "categorical_verbs": "neutral undefined",
          "sentences": "CK19  also positively regulated multidrug resistance-associated protein 4 expression at the PM, suggesting that this keratin may regulate the apical expression of other  ATP-binding cassette proteins as well as CFTR.-Hou,",
          "PMID": 31450978
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "neutral",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "inhibit",
      "activate",
      "inhibit",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "BAP31CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "BAP31",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "BAP31",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "deliver",
          "categorical_verbs": "undefined",
          "sentences": "The present study delivers evidence for cytosolic co-localization of both BAP31 and CFTR and for the control of expression of recombinant CFTR in Chinese hamster ovary (CHO) cells and Xenopus oocytes by BAP31.",
          "PMID": 11274174
        },
        "1": {
          "Verbs": "increase enabl",
          "categorical_verbs": "activate undefined",
          "sentences": "Antisense inhibition of BAP31 in various cell types increased expression of both wild-type CFTR and [DeltaPhe(508)]CFTR and enabled cAMP-activated Cl(-) currents in [DeltaPhe(508)]CFTR-expressing CHO cells.",
          "PMID": 11274174
        },
        "2": {
          "Verbs": "attenuate",
          "categorical_verbs": "undefined",
          "sentences": "Coexpression of CFTR together with BAP31 attenuated cAMP-activated Cl(-) currents in Xenopus oocytes.",
          "PMID": 11274174
        },
        "3": {
          "Verbs": "control control suggest",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "These data therefore suggest association of BAP31 with CFTR that may control maturation or trafficking of CFTR and thus expression in the plasma membrane.",
          "PMID": 11274174
        },
        "4": {
          "Verbs": "interact promotes",
          "categorical_verbs": "neutral activate",
          "sentences": "BAP31 interacts with Sec61 translocons and promotes retrotranslocation of CFTRDeltaF508 via the derlin-1 complex.",
          "PMID": 18555783
        },
        "5": {
          "Verbs": "interact promotes",
          "categorical_verbs": "neutral activate",
          "sentences": "BAP31 interacts with Sec61 translocons and promotes retrotranslocation of CFTRDeltaF508 via the derlin-1 complex.",
          "PMID": 18555783
        },
        "6": {
          "Verbs": "promotes synthesize",
          "categorical_verbs": "activate undefined",
          "sentences": "BAP31 associates with the N terminus of one of its newly synthesized client proteins, the DeltaF508 mutant of CFTR, and promotes its retrotranslocation from the ER and degradation by the cytoplasmic 26S proteasome system.",
          "PMID": 18555783
        },
        "7": {
          "Verbs": "operates deliver synthesize",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Thus, BAP31 operates at early steps to deliver newly synthesized CFTRDeltaF508 to its degradation pathway.",
          "PMID": 18555783
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "activate",
      "neutral",
      "activate",
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "BECN1CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "BECN1",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "BECN1",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "reduce prevent recycl enhanc",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Addition of cystamine prevented the recycling defect of CFTR by enhancing BECN1 expression and reducing SQSTM1 accumulation.",
          "PMID": 23686137
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CALUCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "found analyze ( transfect was delocalize",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined",
          "sentences": "First, we analyzed the effects of curcumin on the localization of DeltaF508-CFTR  in different cell lines (HeLa cells stably transfected with wild-type CFTR or DeltaF508-CFTR, CALU-3 cells, or cystic fibrosis pancreatic epithelial cells CFPAC-1) and found that it was significantly delocalized toward the plasma membrane in DeltaF508-CFTR-expressing cells.",
          "PMID": 16424149
        },
        "1": {
          "Verbs": "express contributes mutate",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Calumenin contributes to ER-Ca<sup>2+</sup> homeostasis in bronchial epithelial cells expressing WT and F508del mutated CFTR and to F508del-CFTR retention.",
          "PMID": 28189267
        },
        "2": {
          "Verbs": "express contributes mutate",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Epub 2017 Feb 4.  Calumenin contributes to ER-Ca2+ homeostasis in bronchial epithelial cells expressing WT and F508del mutated CFTR and to F508del-CFTR retention.",
          "PMID": 28189267
        },
        "3": {
          "Verbs": "discovere is",
          "categorical_verbs": "undefined undefined",
          "sentences": "In this study, we discovered that the Ca2+ binding protein Calumenin (CALU) is a key regulator in the maintenance of ER-Ca2+ calcium homeostasis in both wild type and F508del-CFTR expressing cells.",
          "PMID": 28189267
        },
        "4": {
          "Verbs": "demonstrate provide be use correct",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "These findings demonstrate a pivotal role for Calumenin in CF cells, providing insights into how modulation of Calumenin expression or activity may be used as a potential therapeutic tool to correct defects in F508del-CFTR.",
          "PMID": 28189267
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CCAAT elementCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CCAAT element",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "CCAAT element",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "require recognize",
          "categorical_verbs": "undefined undefined",
          "sentences": "Basal expression of CFTR transcription and cAMP-mediated transcriptional regulation require the presence of an imperfect and inverted CCAAT element recognized as 5'-AATTGGAAGCAAAT-3', located between 132 and 119 nucleotides upstream of the translational start site.",
          "PMID": 7499410
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CDX2CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CDX2",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "CDX2",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "interfere cause suggest is",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Moreover, siRNA (small interfering RNA)-mediated knockdown of CDX2 caused a significant reduction in endogenous CFTR transcription in intestinal cells, suggesting that this factor is critical for the maintenance  of high levels of CFTR expression in these cells.",
          "PMID": 22671145
        },
        "1": {
          "Verbs": "bound indicate coordinates",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Finally, we indicate that CHD6 structurally coordinates a three-dimensional stricture between intragenic elements of CFTR bound by several cell-type specific transcription factors, such as CDX2, SOX18, HNF4α and HNF1α.",
          "PMID": 25631877
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "COPICFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "COPI",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "COPI",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "induce abolish did",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "In an initial gene silencing screen, Sec16A knockdown abolished the unconventional secretion of wild-type and ΔF508-CFTR induced by ER-to-Golgi blockade, whereas the knockdown of other COPII-related components did not.",
          "PMID": 28067262
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "Cl-CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Cl-",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "Cl-",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "diminish show is",
          "categorical_verbs": "inhibit undefined undefined",
          "sentences": "Assays of adenosine 3',5'-cyclic monophosphate-stimulated 36Cl- efflux and whole cell currents show that CFTR function is diminished in IFN-gamma-treated cells.",
          "PMID": 7526699
        },
        "1": {
          "Verbs": "promote stabilize is",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "CK19 overexpression stabilized both wild-type (WT)-CFTR and Lumacaftor (VX-809)-rescued F508del-CFTR (where F508del is the deletion of the phenylalanine residue at position 508) at the plasma membrane (PM), promoting Cl- secretion across human bronchial epithelial (HBE) cells.",
          "PMID": 31450978
        },
        "2": {
          "Verbs": "regulate play",
          "categorical_verbs": "neutral undefined",
          "sentences": "Dynamically regulated ion channel activity and anion selectivity of CFTR by kinases sensitive to intracellular chloride concentration ([Cl-]i) play an important role in epithelial HCO3- secretion.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "EHFCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "EHF",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "EHF",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "promotes altere",
          "categorical_verbs": "activate undefined",
          "sentences": "The CF-modifying gene EHF promotes p.Phe508del-CFTR residual function by altering protein glycosylation and trafficking in epithelial cells.",
          "PMID": 24105369
        },
        "1": {
          "Verbs": "promotes altere",
          "categorical_verbs": "activate undefined",
          "sentences": "The CF-modifying gene EHF promotes p.Phe508del-CFTR residual function by altering protein glycosylation and trafficking in epithelial cells.",
          "PMID": 24105369
        },
        "2": {
          "Verbs": "enrich upregulate were carry alter be",
          "categorical_verbs": "activate activate undefined undefined undefined undefined",
          "sentences": "Transcripts that were upregulated among homozygotes for c.1521_1523delCTT in CFTR, who carry two rare EHF alleles, were enriched for genes that alter protein glycosylation and trafficking, both mechanisms being pivotal for the effective targeting of fully functional p.Phe508del-CFTR to the apical membrane of epithelial cells.",
          "PMID": 24105369
        },
        "3": {
          "Verbs": "conclude modify altere proces",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "We conclude that EHF modifies the CF phenotype by altering capabilities of the epithelial cell to correctly process the folding and trafficking of mutant p.Phe508del-CFTR.",
          "PMID": 24105369
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "EZRCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "EZR",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "EZR",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activates find interact form",
          "categorical_verbs": "activate neutral neutral undefined",
          "sentences": "We find that ezrin binding activates the second PDZ domain of NHERF to interact with the cytoplasmic tails of CFTR (C-CFTR), so as to form a specific 2:1:1 (C-CFTR)(2).NHERF.ezrin",
          "PMID": 16129695
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "EstradiolCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Estradiol",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "Estradiol",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "increase",
          "categorical_verbs": "activate",
          "sentences": "17β-Estradiol increased CFTR and SLC26A6 expression levels of human duodenocytes in experiments in vitro.",
          "PMID": 27615377
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "verified"
    ]
  },
  {
    "data": {
      "id": "GlcCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Glc",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "Glc",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "Us determine purify containe had",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "Using glycosidases and FACE analysis (fluorophore-assisted carbohydrate electrophoresis) we determined that purified CHO-CFTR contained polylactosaminoglycan (PL) sequences, while Sf9-CFTR  had only oligomannosidic saccharides with fucosylation on the innermost GlcNAc.",
          "PMID": 11087715
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "PP2ACFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "PP2A",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "PP2A",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express did",
          "categorical_verbs": "activate undefined",
          "sentences": "A monoclonal anti-CFTR antibody co-precipitated type 2C protein phosphatase (PP2C) from baby hamster kidney cells stably expressing CFTR but did not co-precipitate PP1, PP2A, or PP2B.",
          "PMID": 10506164
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "PP2BCFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "PP2B",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "PP2B",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express did",
          "categorical_verbs": "activate undefined",
          "sentences": "A monoclonal anti-CFTR antibody co-precipitated type 2C protein phosphatase (PP2C) from baby hamster kidney cells stably expressing CFTR but did not co-precipitate PP1, PP2A, or PP2B.",
          "PMID": 10506164
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "RACK1CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "RACK1",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "RACK1",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate bind Us show do",
          "categorical_verbs": "activate neutral undefined undefined undefined",
          "sentences": "Using overlay  assay, immunoprecipitation, pulldown and binding assays, we show that PKC epsilon does not bind to CFTR, but does bind to a receptor for activated C kinase (RACK1), a 37-kDa scaffold protein, and that RACK1 binds to Na(+)/H(+) exchange regulatory factor (NHERF1), a binding partner of CFTR.",
          "PMID": 11956211
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "S100A10CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "S100A10",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "S100A10",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "The formation of the cAMP/protein kinase A-dependent annexin 2-S100A10 complex with cystic fibrosis conductance regulator protein (CFTR) regulates CFTR channel function.",
          "PMID": 17581860
        },
        "1": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "The formation of the cAMP/protein kinase A-dependent annexin 2-S100A10 complex with cystic fibrosis conductance regulator protein (CFTR) regulates CFTR channel  function.",
          "PMID": 17581860
        },
        "2": {
          "Verbs": "report",
          "categorical_verbs": "undefined",
          "sentences": "We report  that annexin 2 (anx 2)-S100A10 forms a functional cAMP/PKA/calcineurin (CaN)-dependent complex with CFTR.",
          "PMID": 17581860
        },
        "3": {
          "Verbs": "increases coimmunoprecipitates attenuates",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Cell stimulation with forskolin/3-isobutyl-1-methylxanthine significantly increases the amount of anx 2-S100A10 that reciprocally coimmunoprecipitates with cell surface CFTR and calyculin A. Preinhibition with PKA or CaN inhibitors attenuates the interaction.",
          "PMID": 17581860
        },
        "4": {
          "Verbs": "demonstrate",
          "categorical_verbs": "undefined",
          "sentences": "We recently demonstrated a cAMP/PKA/calcineurin (CnA)-driven association between annexin 2 (anx 2), its cognate partner -S100A10 and cell surface CFTR.",
          "PMID": 18346874
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "SLC26A9CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "SLC26A9",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "SLC26A9",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "stimulate propose favor leade",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "We propose as an alternative hypothesis (not exclusive) to the known SLC26A9-STAS domain/CFTR interaction, that SLC26A9 favors the biogenesis and/or stabilization  of CFTR, leading to stimulated currents.",
          "PMID": 20658517
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "STX8CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "STX8",
      "target": "CFTR",
      "bendPointPositions": [],
      "portSource": "STX8",
      "portTarget": "CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "increase is thought be show rescues allowe reach",
          "categorical_verbs": "activate undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "RESULTS: Although the SNARE protein STX8 is thought to be functionally related and primarily localized to early endosomes, we show that silencing of STX8, particularly in the presence of the Vertex corrector molecule C18, rescues ΔF508-CFTR, allowing it to reach the cell surface and increasing CFTR-dependent chloride currents by approximately 2.5-fold over control values.",
          "PMID": 30485852
        },
        "1": {
          "Verbs": "reduce target",
          "categorical_verbs": "inhibit neutral",
          "sentences": "STX8 silencing reduced the binding of quality control protein, Hsp 27, a protein that targets ΔF508-CFTR for sumoylation and subsequent degradation, to ΔF508-CFTR.",
          "PMID": 30485852
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "neutral",
      "inhibit",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ADPCOPI",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ADP",
      "target": "COPI",
      "bendPointPositions": [],
      "portSource": "ADP",
      "portTarget": "COPI",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibit Block Block express",
          "categorical_verbs": "inhibit inhibit inhibit activate",
          "sentences": "Blocking COPI recruitment to membranes by expressing an inactive form of the GBF1 guanine nucleotide exchange  factor for ADP-ribosylation factor inhibits CFTR trafficking to the PM.",
          "PMID": 17932045
        },
        "1": {
          "Verbs": "inhibite express disrupt",
          "categorical_verbs": "inhibit activate undefined",
          "sentences": "Similarly, inhibiting COPI dissociation from membranes by expressing a constitutively active ADP-ribosylation factor 1 mutant arrests CFTR within disrupted Golgi elements.",
          "PMID": 17932045
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "activate",
      "inhibit",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRCOPI",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "COPI",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "COPI",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "regulate report is test",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "We now report that export of CFTR from the ER is regulated by the conventional coat protein complex II (COPII) in all cell types tested.",
          "PMID": 11799116
        },
        "1": {
          "Verbs": "requires is disrupt",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Export of wild-type CFTR from the ER requires the coat complex II (COPII) machinery, as it is sensitive to Sar1 mutants that disrupt normal coat assembly and disassembly.",
          "PMID": 15479737
        },
        "2": {
          "Verbs": "is use deliver",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "In contrast, COPII is  not used to deliver CFTR to ER-associated degradation.",
          "PMID": 15479737
        },
        "3": {
          "Verbs": "propose play link is",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "We propose that the di-acidic exit code plays a key role in linking CFTR to the COPII coat machinery and is the primary defect responsible for CF in DeltaF508-expressing patients.",
          "PMID": 15479737
        },
        "4": {
          "Verbs": "investigate",
          "categorical_verbs": "undefined",
          "sentences": "Here, we investigated the role of COPI in CFTR trafficking.",
          "PMID": 17932045
        },
        "5": {
          "Verbs": "inhibit Block Block express",
          "categorical_verbs": "inhibit inhibit inhibit activate",
          "sentences": "Blocking COPI recruitment to membranes by expressing an inactive form of the GBF1 guanine nucleotide exchange  factor for ADP-ribosylation factor inhibits CFTR trafficking to the PM.",
          "PMID": 17932045
        },
        "6": {
          "Verbs": "inhibite express disrupt",
          "categorical_verbs": "inhibit activate undefined",
          "sentences": "Similarly, inhibiting COPI dissociation from membranes by expressing a constitutively active ADP-ribosylation factor 1 mutant arrests CFTR within disrupted Golgi elements.",
          "PMID": 17932045
        },
        "7": {
          "Verbs": "interfere explore deplete use",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "To definitively explore the relationship between COPI and CFTR in epithelial cells, we depleted beta-COP from the human colonic epithelial cell HT-29Cl.19A using small interfering RNA.",
          "PMID": 17932045
        },
        "8": {
          "Verbs": "explore test was",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "To explore the mechanism of COPI action in CFTR traffic we tested whether CFTR was COPI cargo.",
          "PMID": 17932045
        },
        "9": {
          "Verbs": "discovere co-immunoprecipitate",
          "categorical_verbs": "undefined undefined",
          "sentences": "We discovered that the alpha-, beta-, and gamma-subunits of COPI co-immunoprecipitated with CFTR.",
          "PMID": 17932045
        },
        "10": {
          "Verbs": "indicate play",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our results indicate that the COPI complex plays a critical role in CFTR trafficking to the PM.",
          "PMID": 17932045
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "inhibit",
      "activate",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRCREB",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "CREB",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "CREB",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate bind demonstrate",
          "categorical_verbs": "activate neutral undefined",
          "sentences": "Electrophoretic mobility shift assays demonstrate that CRE-binding protein (CREB) binds to the CFTR CRE with high affinity and independently of the adjacent Y box and that the  CFTR CRE binds CREB and activating transcription factor-1 in nuclear extracts of  T84 and CaLu-3 cells.",
          "PMID": 8943230
        },
        "1": {
          "Verbs": "block block is",
          "categorical_verbs": "inhibit inhibit undefined",
          "sentences": "In transient transfections of JEG-3 cells, activation of the CFTR promoter is blocked by a dominant negative CREB mutant.",
          "PMID": 8943230
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRCTCF",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "CTCF",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "CTCF",
      "review_status": "to_review",
      "width": 5,
      "references": {
        "0": {
          "Verbs": "mediates",
          "categorical_verbs": "neutral",
          "sentences": "CTCF mediates insulator function at the CFTR locus.",
          "PMID": 17696881
        },
        "1": {
          "Verbs": "mediates",
          "categorical_verbs": "neutral",
          "sentences": "CTCF mediates insulator function at the CFTR locus.",
          "PMID": 17696881
        },
        "2": {
          "Verbs": "bind reveal",
          "categorical_verbs": "neutral undefined",
          "sentences": "An insulator element 3' to the CFTR gene binds CTCF and reveals an active chromatin hub in primary cells.",
          "PMID": 19129223
        },
        "3": {
          "Verbs": "bind reveal",
          "categorical_verbs": "neutral undefined",
          "sentences": "An insulator element 3' to the CFTR gene binds CTCF and reveals an active chromatin hub in primary cells.",
          "PMID": 19129223
        },
        "4": {
          "Verbs": "expres encompass was",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "This DHS, which encompasses a consensus CTCF-binding site, was evident in primary human epididymis cells that express abundant CFTR mRNA.",
          "PMID": 19129223
        },
        "5": {
          "Verbs": "are describe",
          "categorical_verbs": "undefined undefined",
          "sentences": "Moreover, our findings of CTCF  interactions with CHD6 are consistent with the role described previously for CTCF in CFTR regulation.",
          "PMID": 25631877
        },
        "6": {
          "Verbs": "activate establish utilizes remodel recruit",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Probably within its own topological domain established by the architectural proteins CTCF  and cohesin, the CFTR locus utilizes chromatin dynamics to remodel nucleosomes, recruit cell-selective transcription factors, and activate intronic enhancers.",
          "PMID": 26184320
        },
        "7": {
          "Verbs": "associate associate is organize establish",
          "categorical_verbs": "neutral neutral undefined undefined undefined",
          "sentences": "Across all cells, the CFTR locus is organized into an invariant topologically associated domain (TAD) established by the architectural proteins CCCTC-binding factor (CTCF) and cohesin complex.",
          "PMID": 30893953
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "neutral",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRCa(2+)",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "Ca(2+)",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "Ca(2+)",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate point (",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Previous reports point out to a functional relationship of the cystic fibrosis transmembrane conductance regulator (CFTR) and Ca(2+) activated Cl(-) channels (CaCC).",
          "PMID": 22178883
        },
        "1": {
          "Verbs": "elicite was chang attenuate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Intracellular Ca(2+) signals elicited by receptor-stimulation was not changed during activation of CFTR, while ionophore-induced rise in [Ca(2+)](i) was attenuated after stimulation of CFTR.",
          "PMID": 22178883
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ATPCl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "Cl-",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "result",
          "categorical_verbs": "undefined",
          "sentences": "Fusion of uncoated CCV with planar lipid bilayers resulted in the incorporation of kinase- and ATP-activated Cl- channel activity (7.8 pS at 20 degrees C; 11.9 pS at 37 degrees C), with a linear current-voltage relation under symmetrical conditions.",
          "PMID": 7510684
        },
        "1": {
          "Verbs": "stimulate",
          "categorical_verbs": "neutral",
          "sentences": "ATP and its metabolites stimulate Cl- secretion in human epithelium in vitro and  in vivo.",
          "PMID": 9950763
        },
        "2": {
          "Verbs": "associate associate Gate is ( requires",
          "categorical_verbs": "neutral neutral undefined undefined undefined undefined",
          "sentences": "Gating of the CFTR Cl- channel is associated with ATP hydrolysis at the nucleotide-binding domains (NBD1, NBD2) and requires PKA (protein kinase A) phosphorylation of the R domain.",
          "PMID": 14602047
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRCl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "Cl-",
      "review_status": "to_review",
      "width": 21,
      "references": {
        "0": {
          "Verbs": "block block is repel allowe",
          "categorical_verbs": "inhibit inhibit undefined undefined undefined",
          "sentences": "One interpretation of these results is that the CFTR Cl- channel is blocked by the R domain and that phosphorylation on serines by protein kinase A electrostatically repels the domain, allowing passage of Cl-.",
          "PMID": 1716180
        },
        "1": {
          "Verbs": "encode result",
          "categorical_verbs": "activate undefined",
          "sentences": "Cystic fibrosis results from mutations in the gene encoding the CFTR Cl- channel.",
          "PMID": 7510684
        },
        "2": {
          "Verbs": "is cause result",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Cystic fibrosis (CF) is caused by mutations in the CF transmembrane conductance regulator (CFTR) gene, resulting in defective transepithelial Cl- transport.",
          "PMID": 7526699
        },
        "3": {
          "Verbs": "regulate act are",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) functions to regulate both Cl- and Na+ conductive pathways; however, the cellular mechanisms whereby CFTR acts as a conductance regulator are unknown.",
          "PMID": 7541313
        },
        "4": {
          "Verbs": "rectify ( are link",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "CFTR and outwardly rectifying Cl- channels (ORCCs) are distinct channels but are linked functionally via an unknown regulatory mechanism.",
          "PMID": 7541313
        },
        "5": {
          "Verbs": "regulate suggest function conduct",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Our results suggest that CFTR functions to regulate other Cl- secretory pathways in addition to itself conducting Cl-.",
          "PMID": 7541313
        },
        "6": {
          "Verbs": "declines excise transfect",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) Cl- channel activity declines rapidly when excised from transfected Chinese hamster ovary (CHO) or human airway cells because of membrane-associated phosphatase activity.",
          "PMID": 9612228
        },
        "7": {
          "Verbs": "inhibite are cannot substitute",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Moreover, CFTR Cl- currents are effectively inhibited by a minimal syntaxin 1A construct (i.e., the membrane-anchored H3 domain) that cannot fully substitute for wild-type syntaxin 1A in membrane fusion reactions.",
          "PMID": 9724814
        },
        "8": {
          "Verbs": "is localize function facilitate",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is an integral, multidomain membrane protein localized to the apical surface of epithelial cells that functions to facilitate Cl- transport.",
          "PMID": 9792704
        },
        "9": {
          "Verbs": "is form",
          "categorical_verbs": "undefined undefined",
          "sentences": "- The cystic fibrosis transmembrane conductance regulator (CFTR) is a unique member of the ABC transporter family that forms a novel Cl- channel.",
          "PMID": 9922375
        },
        "10": {
          "Verbs": "activate regulate be is known",
          "categorical_verbs": "activate neutral undefined undefined undefined",
          "sentences": "Other than the fact that the cystic fibrosis transmembrane conductance regulator  (CFTR) Cl- channel can be activated by cAMP dependent kinase (PKA), little is known about the signal transduction pathways regulating CFTR.",
          "PMID": 11155209
        },
        "11": {
          "Verbs": "regulate control control play sought test",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined",
          "sentences": "Since G-proteins play a principal role in signal transduction regulating several ion channels [4,  5, 9], we sought to test whether G-proteins control CFTR Cl- conductance (CFTR G(Cl)) in the native sweat duct (SD).",
          "PMID": 11155209
        },
        "12": {
          "Verbs": "associate associate Gate is ( requires",
          "categorical_verbs": "neutral neutral undefined undefined undefined undefined",
          "sentences": "Gating of the CFTR Cl- channel is associated with ATP hydrolysis at the nucleotide-binding domains (NBD1, NBD2) and requires PKA (protein kinase A) phosphorylation of the R domain.",
          "PMID": 14602047
        },
        "13": {
          "Verbs": "express is are seen",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl- channel expressed in a wide variety of epithelial cells, mutations of which are responsible for hallmark defective Cl- and HCO3- secretion seen in cystic fibrosis (CF).",
          "PMID": 16414184
        },
        "14": {
          "Verbs": "arises",
          "categorical_verbs": "activate",
          "sentences": "Cystic fibrosis arises from the misfolding and premature degradation of CFTR Delta F508, a Cl- ion channel with a single amino acid deletion.",
          "PMID": 16901789
        },
        "15": {
          "Verbs": "express is",
          "categorical_verbs": "activate undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a Cl-selective  anion channel expressed in epithelial tissues.",
          "PMID": 17098482
        },
        "16": {
          "Verbs": "modulate demonstrate be tune",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "The results of the present study demonstrate that CFTR Cl- channel function can be finely tuned by modulating PDZ  domain-based protein-protein interactions within the CFTR-containing macromolecular complexes.",
          "PMID": 21299497
        },
        "17": {
          "Verbs": "associate associate help identify treat",
          "categorical_verbs": "neutral neutral undefined undefined undefined",
          "sentences": "The present study might help to identify novel therapeutic targets to treat diseases associated with dysfunctional CFTR Cl- channels.",
          "PMID": 21299497
        },
        "18": {
          "Verbs": "are",
          "categorical_verbs": "undefined",
          "sentences": "CFTR and TMEM16A are separate but functionally related Cl- channels.",
          "PMID": 22178883
        },
        "19": {
          "Verbs": "are",
          "categorical_verbs": "undefined",
          "sentences": "Epub 2011 Dec 14.  CFTR and TMEM16A are separate but functionally related Cl- channels.",
          "PMID": 22178883
        },
        "20": {
          "Verbs": "are suggest",
          "categorical_verbs": "undefined undefined",
          "sentences": "The apical Cl- channels responsible for secretion are unknown but studies suggest an involvement of the cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 25910246
        },
        "21": {
          "Verbs": "modulate demonstrate are cause",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "The results demonstrate that glucocorticoids differentially modulate pulmonary Cl- channels and are likely causing the decline of CFTR during late gestation in preparation for perinatal lung transition.",
          "PMID": 25910246
        },
        "22": {
          "Verbs": "is cause",
          "categorical_verbs": "undefined undefined",
          "sentences": "Of particular importance is the cAMP-dependent cystic fibrosis transmembrane conductance regulator Cl- channel (CFTR) with mutations of the CFTR encoding gene causing cystic fibrosis.",
          "PMID": 27092946
        },
        "23": {
          "Verbs": "is line",
          "categorical_verbs": "undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP- and cGMP-regulated chloride (Cl-) and bicarbonate (HCO₃-) channel localized primarily at the apical plasma membrane of epithelial cells lining the airway, gut and exocrine glands, where it is responsible for transepithelial salt and water transport.",
          "PMID": 28869532
        },
        "24": {
          "Verbs": "activate are requires",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Because Ca2+ activated TMEM16A currents are only transient, continuous Cl- secretion by airway epithelial cells requires CFTR.",
          "PMID": 29331508
        },
        "25": {
          "Verbs": "regulate are",
          "categorical_verbs": "neutral undefined",
          "sentences": "However, the molecular mechanisms of how [Cl-]i-dependent mechanisms regulate CFTR are unknown.",
          "PMID": 31561038
        },
        "26": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "27": {
          "Verbs": "increase constitute was",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "RESULTS: Among the WNK1, SPAK, and OSR1 kinases that constitute a [Cl-]i-sensitive kinase cascade, the expression of WNK1 alone was sufficient to increase the CFTR bicarbonate permeability (PHCO3/PCl) and conductance (GHCO3) in patch clamp recordings.",
          "PMID": 31561038
        },
        "28": {
          "Verbs": "mediate reveale is surround",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Molecular dissection of the WNK1 domains revealed that the WNK1 kinase domain is responsible for CFTR PHCO3/PCl regulation by direct association with CFTR, while the surrounding N-terminal regions mediate the [Cl-]i-sensitivity of WNK1.",
          "PMID": 31561038
        },
        "29": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CONCLUSION: The CFTR HCO3- channel activity is regulated by [Cl-]i and a WNK1-dependent mechanism.",
          "PMID": 31561038
        },
        "30": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "The cystic-fibrosis transmembrane conductance regulator (CFTR) functions as a\ncAMP-regulated Cl- channel and as a regulator of other membrane conductances.",
          "PMID": 10220462
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "HCO3-Cl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "HCO3-",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "HCO3-",
      "portTarget": "Cl-",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "1": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CONCLUSION: The CFTR HCO3- channel activity is regulated by [Cl-]i and a WNK1-dependent mechanism.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "WNK1Cl-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "WNK1",
      "target": "Cl-",
      "bendPointPositions": [],
      "portSource": "WNK1",
      "portTarget": "Cl-",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "1": {
          "Verbs": "increase constitute was",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "RESULTS: Among the WNK1, SPAK, and OSR1 kinases that constitute a [Cl-]i-sensitive kinase cascade, the expression of WNK1 alone was sufficient to increase the CFTR bicarbonate permeability (PHCO3/PCl) and conductance (GHCO3) in patch clamp recordings.",
          "PMID": 31561038
        },
        "2": {
          "Verbs": "mediate reveale is surround",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Molecular dissection of the WNK1 domains revealed that the WNK1 kinase domain is responsible for CFTR PHCO3/PCl regulation by direct association with CFTR, while the surrounding N-terminal regions mediate the [Cl-]i-sensitivity of WNK1.",
          "PMID": 31561038
        },
        "3": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CONCLUSION: The CFTR HCO3- channel activity is regulated by [Cl-]i and a WNK1-dependent mechanism.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CALUDAB2",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "DAB2",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "DAB2",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "polarize ( form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Endogenous, apical membrane CFTR in polarized human airway epithelial cells (Calu-3) formed a complex with myosin VI, the myosin VI adaptor protein Disabled 2 (Dab2), and clathrin.",
          "PMID": 15247260
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRDAB2",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "DAB2",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "DAB2",
      "review_status": "to_review",
      "width": 4,
      "references": {
        "0": {
          "Verbs": "polarize ( form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Endogenous, apical membrane CFTR in polarized human airway epithelial cells (Calu-3) formed a complex with myosin VI, the myosin VI adaptor protein Disabled 2 (Dab2), and clathrin.",
          "PMID": 15247260
        },
        "1": {
          "Verbs": "increase interact reduce",
          "categorical_verbs": "activate neutral inhibit",
          "sentences": "The tail domain of myosin VI, a dominant-negative recombinant fragment, displaced endogenous myosin  VI from interacting with Dab2 and CFTR and increased the expression of CFTR in the plasma membrane by reducing CFTR endocytosis.",
          "PMID": 15247260
        },
        "2": {
          "Verbs": "Taken indicate facilitate requires",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Taken together, these data indicate that myosin VI and Dab2 facilitate CFTR endocytosis by a mechanism that requires actin filaments.",
          "PMID": 15247260
        },
        "3": {
          "Verbs": "bind examine were localize",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "We examined whether alpha-AP-2 and/or Dab2 were binding partners for CFTR and the role of myosin VI in localizing endocytic adaptors in the intestine.",
          "PMID": 20351096
        },
        "4": {
          "Verbs": "co-localize was identify",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "CFTR co-localized with alpha-AP-2, Dab2, and myosin VI and was identified in a complex with all three endocytic proteins in the intestine.",
          "PMID": 20351096
        },
        "5": {
          "Verbs": "interact reveale",
          "categorical_verbs": "neutral undefined",
          "sentences": "Glutathione S-transferase pulldown assays revealed binding of CFTR to alpha-AP-2  (but not Dab2) in the intestine, whereas Dab-2 interacted with alpha-AP-2.",
          "PMID": 20351096
        },
        "6": {
          "Verbs": "modulate include have been propose",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "A number of adaptor proteins including AP-2 (μ2 subunit) and Dab2 (Disabled-2) have been proposed to modulate CFTR internalization.",
          "PMID": 21995445
        },
        "7": {
          "Verbs": "inhibite increase decrease",
          "categorical_verbs": "inhibit activate inhibit",
          "sentences": "While μ2 depletion dramatically decreased CFTR endocytosis  with little effect on the half-life of the CFTR protein, Dab2 depletion increased the CFTR half-life ~3-fold, in addition to inhibiting CFTR endocytosis.",
          "PMID": 21995445
        },
        "8": {
          "Verbs": "inhibite increase sort recycl provide",
          "categorical_verbs": "inhibit activate undefined undefined undefined",
          "sentences": "Furthermore, Dab2 depletion inhibited CFTR trafficking from the sorting endosome  to the recycling compartment, as well as delivery of CFTR to the late endosome, thus providing a mechanistic explanation for increased CFTR expression and half-life.",
          "PMID": 21995445
        },
        "9": {
          "Verbs": "define",
          "categorical_verbs": "undefined",
          "sentences": "The results of the present study define a significant role for Dab2 both in the endocytosis and post-endocytic fate of CFTR.",
          "PMID": 21995445
        },
        "10": {
          "Verbs": "found was play",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "In intestinal epithelial cells Dab2 was not found to play a direct role in CFTR endocytosis.",
          "PMID": 22399289
        },
        "11": {
          "Verbs": "were shown facilitate remain",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "By contrast,  AP-2 and Dab2 were shown to facilitate CFTR endocytosis in human airway epithelial cells, although the specific mechanism remains unknown.",
          "PMID": 22399289
        },
        "12": {
          "Verbs": "mediates demonstrate polarize",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Our data demonstrate that Dab2 mediates AP-2 independent recruitment of CFTR to CCVs in polarized human airway epithelial cells.",
          "PMID": 22399289
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "activate",
      "inhibit",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRDNAJA1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "DNAJA1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "DNAJA1",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "depend is involv",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "CFTR folding depends on the Hsc70 and Hsp70 chaperones and their co-chaperone DNAJA1, but Hsc70/Hsp70 is also involved in CFTR degradation.",
          "PMID": 31408507
        },
        "1": {
          "Verbs": "overexpress were enhanc",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "DNAJA2 and DNAJA1 were both important for CFTR folding, however overexpressing DNAJA2 but not DNAJA1 enhanced CFTR degradation at the endoplasmic reticulum by Hsc70/Hsp70 and the E3 ubiquitin ligase CHIP.",
          "PMID": 31408507
        },
        "2": {
          "Verbs": "include play",
          "categorical_verbs": "undefined undefined",
          "sentences": "Chaperones, including HSC70, DNAJA1 and DNAJA2, play key roles in both the folding and degradation of wild-type and mutant CFTR at multiple cellular locations.",
          "PMID": 32144307
        },
        "3": {
          "Verbs": "promote synthesize are require",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "DNAJA1 and HSC70 promote the folding of newly synthesized CFTR at the endoplasmic reticulum (ER), but are required for the rapid turnover of misfolded channel at the plasma membrane (PM).",
          "PMID": 32144307
        },
        "4": {
          "Verbs": "bind was use identify validate",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "A CFTR peptide library was used to identify binding sites for HSC70, DNAJA1 and DNAJA2, validated by competition and functional assays.",
          "PMID": 32144307
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRDNAJB1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "DNAJB1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "DNAJB1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "cooperate facilitate",
          "categorical_verbs": "undefined undefined",
          "sentences": "The endoplasmic reticulum-associated Hsp40 DNAJB12 and Hsc70 cooperate to facilitate RMA1 E3-dependent degradation of nascent CFTRDeltaF508.",
          "PMID": 21148293
        },
        "1": {
          "Verbs": "cooperate facilitate",
          "categorical_verbs": "undefined undefined",
          "sentences": "The endoplasmic reticulum-associated Hsp40 DNAJB12 and Hsc70 cooperate to facilitate RMA1 E3-dependent degradation of nascent CFTRDeltaF508.",
          "PMID": 21148293
        },
        "2": {
          "Verbs": "is demonstrate play",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Herein, a novel endoplasmic reticulum (ER)-associated Hsp40, DNAJB12 (JB12) is demonstrated to play a role in control of CFTR folding efficiency.",
          "PMID": 21148293
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTREHD1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "EHD1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "EHD1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "visualize colocalize reach",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "We directly visualized internalization and accumulation of CFTR WT from the PM to a perinuclear compartment that colocalized with the endosomal recycling compartment (ERC) markers Rab11 and EHD1, reaching steady-state distribution by 25 minutes.",
          "PMID": 23572510
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTREHF",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "EHF",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "EHF",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "observe exhibit",
          "categorical_verbs": "undefined undefined",
          "sentences": "We could observe an association of rare EHF haplotypes among homozygotes for c.1521_1523delCTT in CFTR, which exhibit a CF-untypical manifestation of the CF basic defect such as CFTR-mediated residual chloride secretion and low response to amiloride.",
          "PMID": 24105369
        },
        "1": {
          "Verbs": "have reviewe obtaine were stratify",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "We have reviewed transcriptome data obtained from intestinal epithelial samples of homozygotes for c.1521_1523delCTT in CFTR, which were stratified for their EHF genetic background.",
          "PMID": 24105369
        },
        "2": {
          "Verbs": "enrich upregulate were carry alter be",
          "categorical_verbs": "activate activate undefined undefined undefined undefined",
          "sentences": "Transcripts that were upregulated among homozygotes for c.1521_1523delCTT in CFTR, who carry two rare EHF alleles, were enriched for genes that alter protein glycosylation and trafficking, both mechanisms being pivotal for the effective targeting of fully functional p.Phe508del-CFTR to the apical membrane of epithelial cells.",
          "PMID": 24105369
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "AMPEZR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "EZR",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "EZR",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "potentiate",
          "categorical_verbs": "activate",
          "sentences": "Co-expression of CFTR with E3KARP and ezrin in Xenopus oocytes potentiated cAMP-stimulated CFTR Cl(-) currents.",
          "PMID": 10893422
        },
        "1": {
          "Verbs": "modulate enhance",
          "categorical_verbs": "neutral undefined",
          "sentences": "Correctors of mutant CFTR enhance subcortical cAMP-PKA signaling through modulating ezrin phosphorylation and cytoskeleton organization.",
          "PMID": 26823603
        },
        "2": {
          "Verbs": "modulate enhance",
          "categorical_verbs": "neutral undefined",
          "sentences": "Correctors of mutant CFTR enhance subcortical cAMP-PKA signaling through modulating ezrin phosphorylation and cytoskeleton organization.",
          "PMID": 26823603
        },
        "3": {
          "Verbs": "activate show stabilize rescu",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Here, we show that the phosphorylation of ezrin together with its binding to phosphatidylinositol-4,5-bisphosphate (PIP2) tethers the F508del CFTR to the actin cytoskeleton, stabilizing it on the apical membrane and rescuing the sub-membrane compartmentalization of cAMP and activated PKA.",
          "PMID": 26823603
        },
        "4": {
          "Verbs": "activate act rescu restore phosphorylate",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Both the small molecules trimethylangelicin (TMA) and VX-809, which act as 'correctors' for F508del CFTR by rescuing F508del-CFTR-dependent chloride secretion, also restore the apical expression of phosphorylated ezrin and actin organization and increase cAMP and activated PKA submembrane compartmentalization in both primary and secondary cystic fibrosis airway cells.",
          "PMID": 26823603
        },
        "5": {
          "Verbs": "activate reverse highlight create generate",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Latrunculin B treatment or expression of the inactive ezrin mutant T567A reverse the TMA and VX-809-induced effects highlighting the role of corrector-dependent ezrin activation and actin re-organization in creating the conditions to generate a sub-cortical cAMP pool of adequate amplitude to activate the F508del-CFTR-dependent chloride secretion.",
          "PMID": 26823603
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CALUEZR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "EZR",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "EZR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "bind express function is immunoprecipitate",
          "categorical_verbs": "neutral activate undefined undefined undefined",
          "sentences": "Ezrin may function as this  AKAP, since it is expressed in Calu-3 and T84 epithelia, ezrin binds RII in overlay assays, and RII is immunoprecipitated with ezrin from Calu-3 cells.",
          "PMID": 10799517
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTREZR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "EZR",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "EZR",
      "review_status": "to_review",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "Taken demonstrate is link suggest serves",
          "categorical_verbs": "undefined undefined undefined undefined undefined undefined",
          "sentences": "Taken together, these data demonstrate that PKA II is linked physically and functionally to CFTR by an  AKAP interaction, and they suggest that ezrin serves as an AKAP for PKA-mediated  phosphorylation of CFTR.",
          "PMID": 10799517
        },
        "1": {
          "Verbs": "potentiate",
          "categorical_verbs": "activate",
          "sentences": "Co-expression of CFTR with E3KARP and ezrin in Xenopus oocytes potentiated cAMP-stimulated CFTR Cl(-) currents.",
          "PMID": 10893422
        },
        "2": {
          "Verbs": "support link ezrin",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "These results support the concept that E3KARP functions as a scaffold protein that links CFTR to ezrin.",
          "PMID": 10893422
        },
        "3": {
          "Verbs": "has been shown function suggest serv is localize",
          "categorical_verbs": "undefined undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "Since ezrin has been shown previously to function as a protein kinase A anchoring protein, we suggest that one function served by the interaction of E3KARP with both ezrin and CFTR is to localize protein kinase A in the vicinity of the R-domain of CFTR.",
          "PMID": 10893422
        },
        "4": {
          "Verbs": "is",
          "categorical_verbs": "undefined",
          "sentences": "Since ezrin is also an actin-binding protein, the formation of a CFTR.E3KARP.ezrin",
          "PMID": 10893422
        },
        "5": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "A macromolecular complex of beta 2 adrenergic receptor, CFTR, and ezrin/radixin/moesin-binding phosphoprotein 50 is regulated by PKA.",
          "PMID": 12502786
        },
        "6": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "A macromolecular complex of beta 2 adrenergic receptor, CFTR, and ezrin/radixin/moesin-binding phosphoprotein 50 is regulated by PKA.",
          "PMID": 12502786
        },
        "7": {
          "Verbs": "bind has been demonstrate referr",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "It has been demonstrated previously that both the cystic fibrosis transmembrane conductance regulator (CFTR) and beta(2) adrenergic receptor (beta(2)AR) can bind ezrinradixinmoesin-binding phosphoprotein 50 (EBP50, also referred to as NHERF) through their PDZ motifs.",
          "PMID": 12502786
        },
        "8": {
          "Verbs": "interact ( share was shown be enhanc",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined",
          "sentences": "The carboxyl terminus of NHERF interacts with the FERM domain (a domain shared by protein 4.1, ezrin, radixin, and moesin) of a family of actin-binding proteins, ezrin-radixin-moesin. NHERF was shown previously to be capable of enhancing the channel activities of cystic fibrosis transmembrane conductance regulator (CFTR).",
          "PMID": 16129695
        },
        "9": {
          "Verbs": "regulates show br",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Here we show that binding of the FERM domain of ezrin to NHERF regulates the cooperative binding of NHERF to bring two cytoplasmic tails of CFTR into spatial proximity to each other.",
          "PMID": 16129695
        },
        "10": {
          "Verbs": "activates find interact form",
          "categorical_verbs": "activate neutral neutral undefined",
          "sentences": "We find that ezrin binding activates the second PDZ domain of NHERF to interact with the cytoplasmic tails of CFTR (C-CFTR), so as to form a specific 2:1:1 (C-CFTR)(2).NHERF.ezrin",
          "PMID": 16129695
        },
        "11": {
          "Verbs": "interact form",
          "categorical_verbs": "neutral undefined",
          "sentences": "Without ezrin binding, the cytoplasmic tail of CFTR only interacts strongly with the first amino-terminal PDZ domain to  form a 1:1 C-CFTR.NHERF complex.",
          "PMID": 16129695
        },
        "12": {
          "Verbs": "confirm",
          "categorical_verbs": "undefined",
          "sentences": "Immunoprecipitation and immunoblotting confirm the specific interactions of NHERF with the full-length CFTR and with ezrin in vivo.",
          "PMID": 16129695
        },
        "13": {
          "Verbs": "interact promote triggere rescu",
          "categorical_verbs": "neutral activate undefined undefined",
          "sentences": "Activation of the cytoskeletal regulator Rac1 promoted an interaction between the actin-binding adaptor protein ezrin and  NHERF1, triggering exposure of the second PDZ domain of NHERF1, which interacted  with rescued F508del-CFTR.",
          "PMID": 25990958
        },
        "14": {
          "Verbs": "bind direct triggere was stabilize",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "Thus, rather than mainly directing anchoring of F508del-CFTR to the actin cytoskeleton, induction of ezrin activation by Rac1 signaling triggered a conformational change in NHERF1, which was then able to bind and stabilize misfolded CFTR at the plasma membrane.",
          "PMID": 25990958
        },
        "15": {
          "Verbs": "modulate enhance",
          "categorical_verbs": "neutral undefined",
          "sentences": "Correctors of mutant CFTR enhance subcortical cAMP-PKA signaling through modulating ezrin phosphorylation and cytoskeleton organization.",
          "PMID": 26823603
        },
        "16": {
          "Verbs": "modulate enhance",
          "categorical_verbs": "neutral undefined",
          "sentences": "Correctors of mutant CFTR enhance subcortical cAMP-PKA signaling through modulating ezrin phosphorylation and cytoskeleton organization.",
          "PMID": 26823603
        },
        "17": {
          "Verbs": "activate show stabilize rescu",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Here, we show that the phosphorylation of ezrin together with its binding to phosphatidylinositol-4,5-bisphosphate (PIP2) tethers the F508del CFTR to the actin cytoskeleton, stabilizing it on the apical membrane and rescuing the sub-membrane compartmentalization of cAMP and activated PKA.",
          "PMID": 26823603
        },
        "18": {
          "Verbs": "activate act rescu restore phosphorylate",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Both the small molecules trimethylangelicin (TMA) and VX-809, which act as 'correctors' for F508del CFTR by rescuing F508del-CFTR-dependent chloride secretion, also restore the apical expression of phosphorylated ezrin and actin organization and increase cAMP and activated PKA submembrane compartmentalization in both primary and secondary cystic fibrosis airway cells.",
          "PMID": 26823603
        },
        "19": {
          "Verbs": "activate reverse highlight create generate",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Latrunculin B treatment or expression of the inactive ezrin mutant T567A reverse the TMA and VX-809-induced effects highlighting the role of corrector-dependent ezrin activation and actin re-organization in creating the conditions to generate a sub-cortical cAMP pool of adequate amplitude to activate the F508del-CFTR-dependent chloride secretion.",
          "PMID": 26823603
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTREstradiol",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "Estradiol",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "Estradiol",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "were was",
          "categorical_verbs": "undefined undefined",
          "sentences": "The expression levels of CFTR and SLC26A6 in young women were markedly higher in preovulatory phases than in premenstrual phases, which was consistent with the changes of serum estradiol concentrations.",
          "PMID": 27615377
        },
        "1": {
          "Verbs": "decrease showe were revers",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Further results showed that duodenal CFTR and SLC26A6 expression levels in female mice were markedly decreased after ovariectomy, and supplementation with estradiol reversed the changes in CFTR and  SLC26A6.",
          "PMID": 27615377
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "SLC26A6Estradiol",
      "class": "stimulation",
      "cardinality": 0,
      "source": "SLC26A6",
      "target": "Estradiol",
      "bendPointPositions": [],
      "portSource": "SLC26A6",
      "portTarget": "Estradiol",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "were was",
          "categorical_verbs": "undefined undefined",
          "sentences": "The expression levels of CFTR and SLC26A6 in young women were markedly higher in preovulatory phases than in premenstrual phases, which was consistent with the changes of serum estradiol concentrations.",
          "PMID": 27615377
        },
        "1": {
          "Verbs": "decrease showe were revers",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Further results showed that duodenal CFTR and SLC26A6 expression levels in female mice were markedly decreased after ovariectomy, and supplementation with estradiol reversed the changes in CFTR and  SLC26A6.",
          "PMID": 27615377
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "inhibit",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRFKBP8",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "FKBP8",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "FKBP8",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "have examine",
          "categorical_verbs": "undefined undefined",
          "sentences": "We have  now examined the role of FK506-binding protein 8 (FKBP8), a component of the CFTR interactome, during the biogenesis of wild-type and ΔF508 CFTR.",
          "PMID": 22474283
        },
        "1": {
          "Verbs": "suggest is require",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Our results suggest that FKBP8 is a key PN factor required at a post-Hsp90 step in CFTR biogenesis.",
          "PMID": 22474283
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CALUFLNA",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "FLNA",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "FLNA",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "reduce show transfect culture disrupt",
          "categorical_verbs": "inhibit undefined undefined undefined undefined",
          "sentences": "We show that FlnA-Ig repeats transfected into cultured Calu-3 cells disrupt CFTR-filamin interaction and reduce surface levels of CFTR.",
          "PMID": 20351101
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRFLNA",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "FLNA",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "FLNA",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "bind Us show",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Using x-ray crystallography, we show how the CFTR N terminus binds to immunoglobulin-like repeat 21 of filamin A (FlnA-Ig21).",
          "PMID": 20351101
        },
        "1": {
          "Verbs": "bind use",
          "categorical_verbs": "neutral undefined",
          "sentences": "CFTR binds to beta-strands C and D of FlnA-Ig21 using backbone-backbone hydrogen bonds, a linchpin serine residue, and hydrophobic side-chain packing.",
          "PMID": 20351101
        },
        "2": {
          "Verbs": "reduce show transfect culture disrupt",
          "categorical_verbs": "inhibit undefined undefined undefined undefined",
          "sentences": "We show that FlnA-Ig repeats transfected into cultured Calu-3 cells disrupt CFTR-filamin interaction and reduce surface levels of CFTR.",
          "PMID": 20351101
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CDX2FOXA",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CDX2",
      "target": "FOXA",
      "bendPointPositions": [],
      "portSource": "CDX2",
      "portTarget": "FOXA",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "identify",
          "categorical_verbs": "undefined",
          "sentences": "ChIP (chromatin immunoprecipitation) identified  FOXA1/A2 (forkhead box A1/A2), HNF1 (hepatocyte nuclear factor 1) and CDX2 (caudal-type homeobox 2) as in vivo trans-interacting factors.",
          "PMID": 22671145
        },
        "1": {
          "Verbs": "suppress include",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Moreover, loss of FOXA1/A2 suppresses the recruitment of other members of the transcriptional network including HNF1 and CDX2, to multiple cis-elements.",
          "PMID": 24440874
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRFOXA",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "FOXA",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "FOXA",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "activates mediate",
          "categorical_verbs": "activate neutral",
          "sentences": "Chromatin remodeling mediated by the FOXA1/A2 transcription factors activates CFTR expression in intestinal epithelial cells.",
          "PMID": 24440874
        },
        "1": {
          "Verbs": "activates mediate",
          "categorical_verbs": "activate neutral",
          "sentences": "Epub 2014 Jan 17.  Chromatin remodeling mediated by the FOXA1/A2 transcription factors activates CFTR expression in intestinal epithelial cells.",
          "PMID": 24440874
        },
        "2": {
          "Verbs": "diminish show repress alter",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Here we show that concurrent depletion of FOXA1 and FOXA2 represses CFTR expression and alters the three-dimensional architecture of the active locus by diminishing interactions between the promoter and intronic cis-acting elements.",
          "PMID": 24440874
        },
        "3": {
          "Verbs": "modify alter",
          "categorical_verbs": "undefined undefined",
          "sentences": "Reduction of FOXA1/A2 also modifies the enrichment profile of the active enhancer marks H3K27ac and H3K4me2 across the CFTR locus and alters chromatin accessibility at individual cis-elements.",
          "PMID": 24440874
        },
        "4": {
          "Verbs": "reveal underly achieve",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "These  data reveal a complex molecular mechanism underlying the role of FOXA1/A2 in achieving high levels of CFTR expression in intestinal epithelial cells.",
          "PMID": 24440874
        },
        "5": {
          "Verbs": "regulate show (",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "We show that lung development-specific  transcription factors (FOXA, C/EBP) and microRNAs (miR-101, miR-145, miR-384) regulate the switch from strong fetal to very low CFTR expression after birth.",
          "PMID": 25186262
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "neutral",
      "activate",
      "inhibit",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ATPG-protein",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "G-protein",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "G-protein",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRG-protein",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "G-protein",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "G-protein",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "activate",
          "categorical_verbs": "activate",
          "sentences": "Apical heterotrimeric g-proteins activate CFTR in the native sweat duct.",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "activate",
          "categorical_verbs": "activate",
          "sentences": "Apical heterotrimeric g-proteins activate CFTR in the native sweat duct.",
          "PMID": 11155209
        },
        "2": {
          "Verbs": "regulate control control play sought test",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined",
          "sentences": "Since G-proteins play a principal role in signal transduction regulating several ion channels [4,  5, 9], we sought to test whether G-proteins control CFTR Cl- conductance (CFTR G(Cl)) in the native sweat duct (SD).",
          "PMID": 11155209
        },
        "3": {
          "Verbs": "activate monitore describe",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "We activated G-proteins and monitored CFTR G(Cl) activity as described earlier [20, 23, 25].",
          "PMID": 11155209
        },
        "4": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "5": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "GDP  (10 mm) inhibited G-protein activation of CFTR G(Cl) even in the presence of GTP-gamma-S.",
          "PMID": 11155209
        },
        "6": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        },
        "7": {
          "Verbs": "activate showe be",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Further, we showed that the mutant CFTR G(Cl) in ducts from cystic fibrosis (CF)  subjects could be partially activated by G-proteins.",
          "PMID": 11155209
        },
        "8": {
          "Verbs": "induce was compare",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "The magnitude of mutant CFTR G(Cl) activation by G-proteins was smaller as compared to non-CF ducts but comparable to that induced by cAMP in CF ducts.",
          "PMID": 11155209
        },
        "9": {
          "Verbs": "regulate controll controll conclude are help",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined",
          "sentences": "We conclude that heterotrimeric G-proteins are present in the apical membrane of the native human sweat duct which may help regulate salt absorption by controlling CFTR G(Cl) activity.",
          "PMID": 11155209
        },
        "10": {
          "Verbs": "activates demonstrate coupl (",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "We demonstrate that in airway epithelial cells, stimulation of purinergic or muscarinic G-protein coupled receptors (GPCRs) activates TMEM16A and CFTR.",
          "PMID": 29331508
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "activate",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "Cl-G-protein",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Cl-",
      "target": "G-protein",
      "bendPointPositions": [],
      "portSource": "Cl-",
      "portTarget": "G-protein",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "regulate control control play sought test",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined",
          "sentences": "Since G-proteins play a principal role in signal transduction regulating several ion channels [4,  5, 9], we sought to test whether G-proteins control CFTR Cl- conductance (CFTR G(Cl)) in the native sweat duct (SD).",
          "PMID": 11155209
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRGDP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "GDP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "GDP",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "GDP  (10 mm) inhibited G-protein activation of CFTR G(Cl) even in the presence of GTP-gamma-S.",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "inhibite stimulate express increase perform demonstrate",
          "categorical_verbs": "inhibit neutral activate activate undefined undefined",
          "sentences": "CFTR function  assays performed on T84 cells expressing the Rab11a or Rab11b GDP-locked S25N mutants demonstrated that only the Rab11b mutant inhibited 80% of the cAMP-activated halide efflux and that only the constitutively active Rab11b-Q70L  increased the rate constant for stimulated halide efflux.",
          "PMID": 19244346
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "COPIGDP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "COPI",
      "target": "GDP",
      "bendPointPositions": [],
      "portSource": "COPI",
      "portTarget": "GDP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "is shown be prevent",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "The dominant negative GDP-restricted mutant Sar1[T39N] is shown to be a potent inhibitor of mSec12 activity, consistent with its role in preventing COPII vesicle formation in vitro and during transient expression in vivo.",
          "PMID": 11422940
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "G-proteinGDP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "G-protein",
      "target": "GDP",
      "bendPointPositions": [],
      "portSource": "G-protein",
      "portTarget": "GDP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "GDP  (10 mm) inhibited G-protein activation of CFTR G(Cl) even in the presence of GTP-gamma-S.",
          "PMID": 11155209
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "verified"
    ]
  },
  {
    "data": {
      "id": "AMPGSH",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "GSH",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "GSH",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "support is enhanc",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Interestingly, although ATP supports GSH flux through CFTR, this activity is enhanced in the presence of the non-hydrolyzable ATP analog AMP-PNP.",
          "PMID": 12727866
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ATPGSH",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "GSH",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "GSH",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "support is enhanc",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Interestingly, although ATP supports GSH flux through CFTR, this activity is enhanced in the presence of the non-hydrolyzable ATP analog AMP-PNP.",
          "PMID": 12727866
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRGSH",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "GSH",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "GSH",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "was (",
          "categorical_verbs": "undefined undefined",
          "sentences": "GSH content in the apical fluid was 55% lower in CFTR-deficient cultures than in CFTR-repleted cells (P < 0.001).",
          "PMID": 10409237
        },
        "1": {
          "Verbs": "was",
          "categorical_verbs": "undefined",
          "sentences": "In  contrast, intracellular GSH content was similar in CFT1 cells and CFTR-repleted cells.",
          "PMID": 10409237
        },
        "2": {
          "Verbs": "was",
          "categorical_verbs": "undefined",
          "sentences": "Rather, GSH efflux of CFTR-deficient cells was lower than that of CFTR-repleted cells.",
          "PMID": 10409237
        },
        "3": {
          "Verbs": "associate decrease associate suggest result",
          "categorical_verbs": "neutral inhibit neutral undefined undefined",
          "sentences": "These studies suggested that decreased GSH content in the apical fluid in CF resulted from abnormal GSH transport associated with a defective CFTR.",
          "PMID": 10409237
        },
        "4": {
          "Verbs": "associate associate have shown is enhanc implicate",
          "categorical_verbs": "neutral neutral undefined undefined undefined undefined undefined",
          "sentences": "Studies have shown that expression of cystic fibrosis transmembrane conductance regulator (CFTR) is associated with enhanced glutathione (GSH) efflux from airway epithelial cells, implicating a role for CFTR in the control of oxidative stress  in the airways.",
          "PMID": 12727866
        },
        "5": {
          "Verbs": "express define underly study purify reconstitute",
          "categorical_verbs": "activate undefined undefined undefined undefined undefined",
          "sentences": "To define the mechanism underlying CFTR-associated GSH flux, we studied wild-type and mutant CFTR proteins expressed in Sf9 membranes, as well as purified and reconstituted CFTR.",
          "PMID": 12727866
        },
        "6": {
          "Verbs": "mediate show is disrupt",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "We show that CFTR-expressing membrane vesicles mediate nucleotide-activated GSH flux, which is disrupted in the R347D pore mutant, and in the Walker A K464A and K1250A mutants.",
          "PMID": 12727866
        },
        "7": {
          "Verbs": "mediates reveal purify",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Further, we reveal that purified CFTR protein alone directly mediates nucleotide-dependent GSH flux.",
          "PMID": 12727866
        },
        "8": {
          "Verbs": "support is enhanc",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Interestingly, although ATP supports GSH flux through CFTR, this activity is enhanced in the presence of the non-hydrolyzable ATP analog AMP-PNP.",
          "PMID": 12727866
        },
        "9": {
          "Verbs": "demonstrate is",
          "categorical_verbs": "undefined undefined",
          "sentences": "In conclusion, our data demonstrate that GSH flux is an intrinsic function of CFTR and prompt future examination of the role of this function in airway biology in health and disease.",
          "PMID": 12727866
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ARF1GTP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ARF1",
      "target": "GTP",
      "bendPointPositions": [],
      "portSource": "ARF1",
      "portTarget": "GTP",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "promotes",
          "categorical_verbs": "activate",
          "sentences": "mSec12 promotes efficient guanine nucleotide exchange on Sar1, but not Arf1 or Rab GTPases.",
          "PMID": 11422940
        },
        "1": {
          "Verbs": "block block followe was SNARE",
          "categorical_verbs": "inhibit inhibit undefined undefined undefined",
          "sentences": "Remarkably, in a cell type-specific manner, processing of  CFTR from the core-glycosylated (band B) ER form to the complex-glycosylated (band C) isoform followed a non-conventional pathway that was insensitive to dominant negative Arf1, Rab1a/Rab2 GTPases, or the SNAp REceptor (SNARE) component syntaxin 5, all of which block the conventional trafficking pathway from the ER to the Golgi.",
          "PMID": 11799116
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ATPGTP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "GTP",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "GTP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRGTP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "GTP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "GTP",
      "review_status": "to_review",
      "width": 6,
      "references": {
        "0": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "increase mean",
          "categorical_verbs": "activate undefined",
          "sentences": "GTP-gamma-S increased CFTR G(Cl) by 44 +/- 20 mS/cm(2) (mean +/- se; n = 7).",
          "PMID": 11155209
        },
        "2": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "GDP  (10 mm) inhibited G-protein activation of CFTR G(Cl) even in the presence of GTP-gamma-S.",
          "PMID": 11155209
        },
        "3": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        },
        "4": {
          "Verbs": "block block followe was SNARE",
          "categorical_verbs": "inhibit inhibit undefined undefined undefined",
          "sentences": "Remarkably, in a cell type-specific manner, processing of  CFTR from the core-glycosylated (band B) ER form to the complex-glycosylated (band C) isoform followed a non-conventional pathway that was insensitive to dominant negative Arf1, Rab1a/Rab2 GTPases, or the SNAp REceptor (SNARE) component syntaxin 5, all of which block the conventional trafficking pathway from the ER to the Golgi.",
          "PMID": 11799116
        },
        "5": {
          "Verbs": "impaire modulates",
          "categorical_verbs": "inhibit neutral",
          "sentences": "Rab4GTPase modulates CFTR function by impairing channel expression at plasma membrane.",
          "PMID": 16413502
        },
        "6": {
          "Verbs": "impaire modulates",
          "categorical_verbs": "inhibit neutral",
          "sentences": "Epub 2006 Jan 6.  Rab4GTPase modulates CFTR function by impairing channel expression at plasma membrane.",
          "PMID": 16413502
        },
        "7": {
          "Verbs": "reduce cause",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Depletion or inhibition of wild-type CFTR present in bronchial epithelial cells reduced the availability of the small GTPase Rab5 by causing Rab5 sequestration within the detergent-insoluble protein fraction together with  its accumulation in aggresomes.",
          "PMID": 23686137
        },
        "8": {
          "Verbs": "controll controll is include",
          "categorical_verbs": "neutral neutral undefined undefined",
          "sentences": "Once CFTR is at the cell  surface, its stability is also controlled by multiple protein interactors, including Rab proteins, Rho small GTPases, and PDZ proteins.",
          "PMID": 23773658
        },
        "9": {
          "Verbs": "regulate review compare is known",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "Here, we review the role of GTPases in regulating trafficking of ion channels and transporters, comparing what is known for CFTR and ENaC with other types of channels.",
          "PMID": 28463591
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "activate",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "neutral",
      "inhibit",
      "neutral",
      "inhibit",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "COPIGTP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "COPI",
      "target": "GTP",
      "bendPointPositions": [],
      "portSource": "COPI",
      "portTarget": "GTP",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "is involv",
          "categorical_verbs": "undefined undefined",
          "sentences": "The Sar1 GTPase is an essential component of COPII vesicle coats involved in export of cargo from the endoplasmic reticulum of mammalian cells.",
          "PMID": 11422940
        },
        "1": {
          "Verbs": "induce form initiate select",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "COPII-coated vesicles form on the endoplasmic reticulum by the stepwise recruitment of three cytosolic components: Sar1-GTP to initiate coat formation, Sec23/24 heterodimer to select SNARE and cargo molecules, and Sec13/31 to induce  coat polymerization and membrane deformation.",
          "PMID": 12239560
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "G-proteinGTP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "G-protein",
      "target": "GTP",
      "bendPointPositions": [],
      "portSource": "G-protein",
      "portTarget": "GTP",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate activates show",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We now show that activating G-proteins with GTP-gamma-S (100 microm) also activates CFTR G(Cl) in the presence of 5 mm ATP alone (without exogenous cAMP).",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "GDP  (10 mm) inhibited G-protein activation of CFTR G(Cl) even in the presence of GTP-gamma-S.",
          "PMID": 11155209
        },
        "2": {
          "Verbs": "activate increase induce was",
          "categorical_verbs": "activate activate activate undefined",
          "sentences": "The heterotrimeric G-protein activator (AlF(4-) in the cytoplasmic bath activated CFTR G(Cl) (increased by 51.5 +/- 9.4 mS/cm(2) in the presence of  5 mm ATP without cAMP, n = 6), the magnitude of which was similar to that induced by GTP-gamma-S.",
          "PMID": 11155209
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "GDPGTP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "GDP",
      "target": "GTP",
      "bendPointPositions": [],
      "portSource": "GDP",
      "portTarget": "GTP",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "GDP  (10 mm) inhibited G-protein activation of CFTR G(Cl) even in the presence of GTP-gamma-S.",
          "PMID": 11155209
        },
        "1": {
          "Verbs": "form stabilize has rearrang expose emb",
          "categorical_verbs": "undefined undefined undefined undefined undefined undefined",
          "sentences": "Sec23 and Sar1 form a continuous surface stabilized by a non-hydrolysable GTP analogue, and Sar1 has rearranged from the GDP conformation  to expose amino-terminal residues that will probably embed in the bilayer.",
          "PMID": 12239560
        },
        "2": {
          "Verbs": "inhibit lock lock appear",
          "categorical_verbs": "inhibit inhibit inhibit undefined",
          "sentences": "GTPase-deficient Rab4Q67L and GDP locked  Rab4S22N both inhibit channel activity, which appears characteristically different.",
          "PMID": 16413502
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRGlc",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "Glc",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "Glc",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "Us determine purify containe had",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "Using glycosidases and FACE analysis (fluorophore-assisted carbohydrate electrophoresis) we determined that purified CHO-CFTR contained polylactosaminoglycan (PL) sequences, while Sf9-CFTR  had only oligomannosidic saccharides with fucosylation on the innermost GlcNAc.",
          "PMID": 11087715
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRHCO3-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "HCO3-",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "HCO3-",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "express is are seen",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl- channel expressed in a wide variety of epithelial cells, mutations of which are responsible for hallmark defective Cl- and HCO3- secretion seen in cystic fibrosis (CF).",
          "PMID": 16414184
        },
        "1": {
          "Verbs": "block block show posses be known",
          "categorical_verbs": "inhibit inhibit undefined undefined undefined undefined",
          "sentences": "Our recent results show that endometrial epithelial cells possess a cAMP-activated HCO3- transport mechanism, which could be impaired with  channel blockers known to block CFTR or antisense against CFTR.",
          "PMID": 16414184
        },
        "2": {
          "Verbs": "reduce result",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Co-culture of sperm with CFTR antisense-treated endometrial cells or HCO3- secretion-defective  CF epithelial cells resulted in reduced sperm capacitation and egg-fertilizing ability.",
          "PMID": 16414184
        },
        "3": {
          "Verbs": "rescu fertilize",
          "categorical_verbs": "undefined undefined",
          "sentences": "Addition of HCO3- to the culture media and transfection of wild-type CFTR into CF cells rescued the fertilizing capacity of sperm.",
          "PMID": 16414184
        },
        "4": {
          "Verbs": "inhibite express reveale is indicate be",
          "categorical_verbs": "inhibit activate undefined undefined undefined undefined",
          "sentences": "Immunostaining and  Western blot revealed that CFTR is expressed in rodent sperm and intracellular measurement of pH during sperm capacitation indicated that the entry of HCO3- into sperm could be inhibited by CFTR inhibitor.",
          "PMID": 16414184
        },
        "5": {
          "Verbs": "controll controll are fertilize suggest be",
          "categorical_verbs": "neutral neutral undefined undefined undefined undefined",
          "sentences": "These results are consistent with a critical role of CFTR in controlling uterine HCO3- secretion and sperm fertilizing capacity, suggesting that CFTR may be a potential target for post-meiotic regulation of fertility.",
          "PMID": 16414184
        },
        "6": {
          "Verbs": "associate associate cause is include",
          "categorical_verbs": "neutral neutral undefined undefined undefined",
          "sentences": "BACKGRAOUD & AIMS: Aberrant epithelial bicarbonate (HCO3-) secretion caused by mutations in the cystic fibrosis transmembrane conductance regulator (CFTR) gene  is associated with several diseases including cystic fibrosis and pancreatitis.",
          "PMID": 31561038
        },
        "7": {
          "Verbs": "regulate play",
          "categorical_verbs": "neutral undefined",
          "sentences": "Dynamically regulated ion channel activity and anion selectivity of CFTR by kinases sensitive to intracellular chloride concentration ([Cl-]i) play an important role in epithelial HCO3- secretion.",
          "PMID": 31561038
        },
        "8": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "9": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CONCLUSION: The CFTR HCO3- channel activity is regulated by [Cl-]i and a WNK1-dependent mechanism.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "Cl-HCO3-",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Cl-",
      "target": "HCO3-",
      "bendPointPositions": [],
      "portSource": "Cl-",
      "portTarget": "HCO3-",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "express is are seen",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl- channel expressed in a wide variety of epithelial cells, mutations of which are responsible for hallmark defective Cl- and HCO3- secretion seen in cystic fibrosis (CF).",
          "PMID": 16414184
        },
        "1": {
          "Verbs": "regulate play",
          "categorical_verbs": "neutral undefined",
          "sentences": "Dynamically regulated ion channel activity and anion selectivity of CFTR by kinases sensitive to intracellular chloride concentration ([Cl-]i) play an important role in epithelial HCO3- secretion.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRHIF1A",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "HIF1A",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "HIF1A",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "perform underline verify",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Experiments performed in Cftr(-/-) and Nkcc1(-/-) mice underlined the role of altered CFTR expression for these functional changes, and  work in conditional Hif1a mutant mice verified HIF-1-dependent CFTR regulation in vivo.",
          "PMID": 18779379
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRHNF1A",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "HNF1A",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "HNF1A",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "is involv",
          "categorical_verbs": "undefined undefined",
          "sentences": "HNF1alpha is involved in tissue-specific regulation of CFTR gene expression.",
          "PMID": 14656222
        },
        "1": {
          "Verbs": "is involv",
          "categorical_verbs": "undefined undefined",
          "sentences": "HNF1alpha is involved in tissue-specific regulation of CFTR gene expression.",
          "PMID": 14656222
        },
        "2": {
          "Verbs": "express bound is show",
          "categorical_verbs": "activate neutral undefined undefined",
          "sentences": "HNF1alpha, which is expressed in many of the same epithelial cell types as CFTR and shows similar differentiation-dependent changes in gene expression, bound to these sites in vitro.",
          "PMID": 14656222
        },
        "3": {
          "Verbs": "augment",
          "categorical_verbs": "undefined",
          "sentences": "Overexpression of heterologous HNF1alpha augmented CFTR transcription in vivo.",
          "PMID": 14656222
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "ATPHSPBP1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ATP",
      "target": "HSPBP1",
      "bendPointPositions": [],
      "portSource": "ATP",
      "portTarget": "HSPBP1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "bind demonstrate",
          "categorical_verbs": "neutral undefined",
          "sentences": "The ATPase domain of Hsp70 demonstrated binding to HspBP1.",
          "PMID": 9830037
        },
        "1": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "HspBP1 (8 microM)  inhibited approximately 90% of the Hsp40-activated Hsp70 ATPase activity.",
          "PMID": 9830037
        },
        "2": {
          "Verbs": "prevent is",
          "categorical_verbs": "undefined undefined",
          "sentences": "HspBP1  prevented ATP binding to Hsp70, and therefore this is the likely mechanism of inhibition.",
          "PMID": 9830037
        },
        "3": {
          "Verbs": "is define were examine",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Hsp40-activated ATPase activity is essential for the renaturation activity of Hsp70; therefore, the effects of HspBP1 on renaturation of luciferase in a reticulocyte lysate and a defined system were examined.",
          "PMID": 9830037
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRHSPBP1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "HSPBP1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "HSPBP1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "stimulates interferes",
          "categorical_verbs": "neutral neutral",
          "sentences": "As a consequence, HspBP1 interferes with the CHIP-induced degradation of immature forms of the cystic fibrosis transmembrane conductance regulator (CFTR) and stimulates CFTR maturation.",
          "PMID": 15215316
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRIFNG",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "IFNG",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "IFNG",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "down-regulate found up-regulate confirm showe (",
          "categorical_verbs": "inhibit neutral activate undefined undefined undefined",
          "sentences": "Previous work that we confirmed showed that interferon gamma (IFNgamma) down-regulated CFTR expression in epithelial cells (T84), but by contrast, we found that IFNgamma up-regulated CFTR mRNA and protein expression in rat and human mast cells.",
          "PMID": 16051699
        },
        "1": {
          "Verbs": "inhibite was",
          "categorical_verbs": "inhibit undefined",
          "sentences": "IFNgamma up-regulation of CFTR in mast cells was inhibited by p38 and extracellular signal-regulated kinase (ERK) kinase inhibitors but not a Janus tyrosine kinase (JAK)2 inhibitor, whereas in T84 cells IFNgamma-mediated down-regulation of CFTR was JAK2-dependent and ERK- and p38-independent.",
          "PMID": 16051699
        },
        "2": {
          "Verbs": "was",
          "categorical_verbs": "undefined",
          "sentences": "Furthermore, IFNgamma down-regulation of CFTR in T84 epithelial cells was STAT1-dependent, but up-regulation of CFTR in mast cells was STAT1-independent.",
          "PMID": 16051699
        },
        "3": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "Surprisingly, IFNgamma treatment of mast cells inhibited Cl(-) efflux, in contrast to up-regulation of CFTR/mRNA and protein expression.",
          "PMID": 16051699
        },
        "4": {
          "Verbs": "suggest is",
          "categorical_verbs": "undefined undefined",
          "sentences": "This and other work suggests that the effect of IFNgamma on CFTR expression in mast cells is important for their function.",
          "PMID": 16051699
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRIRF1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "IRF1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "IRF1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "interfere reduces",
          "categorical_verbs": "neutral inhibit",
          "sentences": "Small interfering RNA (siRNA)-mediated depletion of IRF1 or overexpression of IRF2, an antagonist of IRF1, reduces CFTR expression in 16HBE14o- cells.",
          "PMID": 23689137
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRIRF2",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "IRF2",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "IRF2",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "interfere reduces",
          "categorical_verbs": "neutral inhibit",
          "sentences": "Small interfering RNA (siRNA)-mediated depletion of IRF1 or overexpression of IRF2, an antagonist of IRF1, reduces CFTR expression in 16HBE14o- cells.",
          "PMID": 23689137
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "verified"
    ]
  },
  {
    "data": {
      "id": "IRF1IRF2",
      "class": "stimulation",
      "cardinality": 0,
      "source": "IRF1",
      "target": "IRF2",
      "bendPointPositions": [],
      "portSource": "IRF1",
      "portTarget": "IRF2",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "interfere reduces",
          "categorical_verbs": "neutral inhibit",
          "sentences": "Small interfering RNA (siRNA)-mediated depletion of IRF1 or overexpression of IRF2, an antagonist of IRF1, reduces CFTR expression in 16HBE14o- cells.",
          "PMID": 23689137
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRKCNN4",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "KCNN4",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "KCNN4",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate potentiate demonstrate",
          "categorical_verbs": "activate activate undefined",
          "sentences": "RESULTS: Studies in control tissues demonstrate that 1-EBIO activated CFTR-mediated Cl⁻ secretion in the absence of cAMP-mediated stimulation and potentiated cAMP-induced Cl⁻ secretion by 39.2±6.7% (P<0.001) via activation of basolateral Ca²⁺-activated and clotrimazole-sensitive KCNN4 K⁺ channels.",
          "PMID": 21909392
        },
        "1": {
          "Verbs": "express increase potentiates conclude drive",
          "categorical_verbs": "activate activate activate undefined undefined",
          "sentences": "CONCLUSIONS: We conclude that 1-EBIO potentiates Cl⁻secretion in native CF tissues expressing CFTR mutants with residual Cl⁻ channel function by activation  of basolateral KCNN4 K⁺ channels that increase the driving force for luminal Cl⁻  exit.",
          "PMID": 21909392
        },
        "2": {
          "Verbs": "increase augment suggest",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "This mechanism may augment effects of CFTR correctors and potentiators that increase the number and/or activity of mutant CFTR channels at the cell surface and suggests KCNN4 as a therapeutic target for CF.",
          "PMID": 21909392
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRLPA",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "LPA",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "LPA",
      "review_status": "to_review",
      "width": 4,
      "references": {
        "0": {
          "Verbs": "express show ( are form",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Here we show, for the first time, that  type 2 LPA receptors (LPA2) are expressed at the apical surface of intestinal epithelial cells, where they form a macromolecular complex with Na+/H+ exchanger  regulatory factor-2 and CFTR through a PSD95/Dlg/ZO-1-based interaction.",
          "PMID": 16203867
        },
        "1": {
          "Verbs": "inhibite",
          "categorical_verbs": "inhibit",
          "sentences": "LPA inhibited CFTR-dependent iodide efflux through LPA2-mediated Gi pathway, and LPA  inhibited CFTR-mediated short-circuit currents in a compartmentalized fashion.",
          "PMID": 16203867
        },
        "2": {
          "Verbs": "induce reduce was use revers",
          "categorical_verbs": "activate inhibit undefined undefined undefined",
          "sentences": "CFTR-dependent intestinal fluid secretion induced by CTX in mice was reduced substantially by LPA administration; disruption of this complex using a cell-permeant LPA2-specific peptide reversed LPA2-mediated inhibition.",
          "PMID": 16203867
        },
        "3": {
          "Verbs": "targete test",
          "categorical_verbs": "neutral undefined",
          "sentences": "We specifically targeted the PDZ domain-based LPA2 (type 2 lysophosphatidic acid receptor)-NHERF2 (Na+/H+ exchanger regulatory factor-2) interaction within the CFTR-NHERF2-LPA2-containing macromolecular complexes in airway epithelia and tested its regulatory role on CFTR channel function.",
          "PMID": 21299497
        },
        "4": {
          "Verbs": "implicate",
          "categorical_verbs": "undefined",
          "sentences": "Structural insights into PDZ-mediated interaction of NHERF2 and LPA(2), a cellular event implicated in CFTR channel regulation.",
          "PMID": 24613836
        },
        "5": {
          "Verbs": "implicate",
          "categorical_verbs": "undefined",
          "sentences": "Epub 2014 Mar 12.  Structural insights into PDZ-mediated interaction of NHERF2 and LPA(2), a cellular event implicated in CFTR channel regulation.",
          "PMID": 24613836
        },
        "6": {
          "Verbs": "have shown abolish augment",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "We previously have shown that disruption of the PDZ-mediated NHERF2-LPA2 interaction abolishes the LPA inhibitory effect and augments CFTR Cl(-) channel activity in vitro and in vivo.",
          "PMID": 24613836
        },
        "7": {
          "Verbs": "provides prove",
          "categorical_verbs": "undefined undefined",
          "sentences": "This study provides an understanding of the structural basis for the PDZ-mediated NHERF2-LPA2 interaction that could prove valuable in selective drug design against CFTR-related human diseases.",
          "PMID": 24613836
        },
        "8": {
          "Verbs": "review discus",
          "categorical_verbs": "undefined undefined",
          "sentences": "In this paper, we review the current knowledge of a macromolecular complex of CFTR, Na⁺/H⁺ exchanger regulatory factor 2 (NHERF2), and lysophosphatidic acids (LPA) receptor 2 (LPA₂) at the apical plasma membrane of airway and gut epithelial cells, and discuss its relevance in human physiology and diseases.",
          "PMID": 28869532
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "inhibit",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRNRF2",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "NRF2",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "NRF2",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "bind conserv ARE has been identify",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined",
          "sentences": "A conserved antioxidant response element (ARE) in the CFTR minimal promoter, which binds Nrf2, has been identified.",
          "PMID": 20309604
        },
        "1": {
          "Verbs": "exert",
          "categorical_verbs": "undefined",
          "sentences": "Surprisingly, Nrf2 exerts an unexpected repressive role on the CFTR gene promoter activity.",
          "PMID": 20309604
        },
        "2": {
          "Verbs": "increases promotes demonstrate",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We demonstrated that Nrf2 promotes YY1 nuclear localization and increases its binding to the CFTR promoter.",
          "PMID": 20309604
        },
        "3": {
          "Verbs": "is report contributes clarify leade",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "To our knowledge, this study is the first to report a repressor role of Nrf2 through the cooperation with YY1 and contributes to clarify the cascade events leading to the oxidative stress-suppressed CFTR expression.",
          "PMID": 20309604
        },
        "4": {
          "Verbs": "activates displaces",
          "categorical_verbs": "activate undefined",
          "sentences": "After 2 hours of SFN treatment, Nrf2 displaces these repressive factors and activates CFTR expression.",
          "PMID": 25259561
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRNa+",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "Na+",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "Na+",
      "review_status": "to_review",
      "width": 4,
      "references": {
        "0": {
          "Verbs": "regulate act are",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) functions to regulate both Cl- and Na+ conductive pathways; however, the cellular mechanisms whereby CFTR acts as a conductance regulator are unknown.",
          "PMID": 7541313
        },
        "1": {
          "Verbs": "inhibit (",
          "categorical_verbs": "inhibit undefined",
          "sentences": "cAMP-dependent activation of CFTR inhibits epithelial Na+ channels (ENaC).",
          "PMID": 10220462
        },
        "2": {
          "Verbs": "express show ( are form",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Here we show, for the first time, that  type 2 LPA receptors (LPA2) are expressed at the apical surface of intestinal epithelial cells, where they form a macromolecular complex with Na+/H+ exchanger  regulatory factor-2 and CFTR through a PSD95/Dlg/ZO-1-based interaction.",
          "PMID": 16203867
        },
        "3": {
          "Verbs": "targete test",
          "categorical_verbs": "neutral undefined",
          "sentences": "We specifically targeted the PDZ domain-based LPA2 (type 2 lysophosphatidic acid receptor)-NHERF2 (Na+/H+ exchanger regulatory factor-2) interaction within the CFTR-NHERF2-LPA2-containing macromolecular complexes in airway epithelia and tested its regulatory role on CFTR channel function.",
          "PMID": 21299497
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "Cl-Na+",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Cl-",
      "target": "Na+",
      "bendPointPositions": [],
      "portSource": "Cl-",
      "portTarget": "Na+",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "regulate act are",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) functions to regulate both Cl- and Na+ conductive pathways; however, the cellular mechanisms whereby CFTR acts as a conductance regulator are unknown.",
          "PMID": 7541313
        },
        "1": {
          "Verbs": "participates",
          "categorical_verbs": "undefined",
          "sentences": "The basolateral Na+,K+,2Cl- cotransporter (NKCC1) participates in Cl- secretion.",
          "PMID": 25910246
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "EZRNa+",
      "class": "stimulation",
      "cardinality": 0,
      "source": "EZR",
      "target": "Na+",
      "bendPointPositions": [],
      "portSource": "EZR",
      "portTarget": "Na+",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "control control form",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "Ezrin controls the macromolecular complexes formed between an adapter protein Na+/H+ exchanger regulatory factor and the cystic fibrosis transmembrane conductance regulator.",
          "PMID": 16129695
        },
        "1": {
          "Verbs": "control control form",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "Ezrin controls the macromolecular complexes formed between an adapter protein Na+/H+ exchanger regulatory factor and the cystic fibrosis transmembrane conductance regulator.",
          "PMID": 16129695
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "LPANa+",
      "class": "stimulation",
      "cardinality": 0,
      "source": "LPA",
      "target": "Na+",
      "bendPointPositions": [],
      "portSource": "LPA",
      "portTarget": "Na+",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "express show ( are form",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Here we show, for the first time, that  type 2 LPA receptors (LPA2) are expressed at the apical surface of intestinal epithelial cells, where they form a macromolecular complex with Na+/H+ exchanger  regulatory factor-2 and CFTR through a PSD95/Dlg/ZO-1-based interaction.",
          "PMID": 16203867
        },
        "1": {
          "Verbs": "targete test",
          "categorical_verbs": "neutral undefined",
          "sentences": "We specifically targeted the PDZ domain-based LPA2 (type 2 lysophosphatidic acid receptor)-NHERF2 (Na+/H+ exchanger regulatory factor-2) interaction within the CFTR-NHERF2-LPA2-containing macromolecular complexes in airway epithelia and tested its regulatory role on CFTR channel function.",
          "PMID": 21299497
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "RACK1Na+",
      "class": "stimulation",
      "cardinality": 0,
      "source": "RACK1",
      "target": "Na+",
      "bendPointPositions": [],
      "portSource": "RACK1",
      "portTarget": "Na+",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate bind involves",
          "categorical_verbs": "activate neutral undefined",
          "sentences": "Protein kinase C epsilon-dependent regulation of cystic fibrosis transmembrane regulator involves binding to a receptor for activated C kinase (RACK1) and RACK1 binding to Na+/H+ exchange regulatory factor.",
          "PMID": 11956211
        },
        "1": {
          "Verbs": "activate bind involves",
          "categorical_verbs": "activate neutral undefined",
          "sentences": "Protein kinase C epsilon-dependent regulation of cystic fibrosis transmembrane regulator involves binding to a receptor for activated C kinase (RACK1) and RACK1 binding to Na+/H+ exchange regulatory factor.",
          "PMID": 11956211
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRPDE4D",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "PDE4D",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "PDE4D",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "associate associate found recruit was be precludes",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined undefined",
          "sentences": "Furthermore whereas EBP50 recruits a cAMP-dependent protein kinase (PKA) complex to CFTR, Shank2 was found to be physically and functionally associated with the cyclic nucleotide phosphodiesterase PDE4D that precludes cAMP/PKA signals in epithelial cells and mouse brains.",
          "PMID": 17244609
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRPDZK1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "PDZK1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "PDZK1",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "bind are concentrate",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "ClC-3B as well as CFTR also binds EBP50 (ERM-binding phosphoprotein 50) and PDZK1, which are concentrated at the plasma membrane.",
          "PMID": 12471024
        },
        "1": {
          "Verbs": "suggest occur form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "CFTR single-channel recordings and FRET-based intracellular cAMP dynamics suggest that a compartmentalized coupling of cAMP transporter and CFTR occurs via the PDZ scaffolding protein, PDZK1, forming a macromolecular complex at apical surfaces of gut epithelia.",
          "PMID": 18045536
        },
        "2": {
          "Verbs": "mediate augment is",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Binding of drugs to MRP4 augments the formation of MRP4-CFTR-containing macromolecular complexes that is mediated via scaffolding protein PDZK1.",
          "PMID": 25762723
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CALUPP2A",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "PP2A",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "PP2A",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "localize",
          "categorical_verbs": "undefined",
          "sentences": "PP2A subunits localize to the apical surface of airway epithelia and PP2A phosphatase activity co-purifies with CFTR in Calu-3 cells.",
          "PMID": 16239222
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRPP2A",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "PP2A",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "PP2A",
      "review_status": "to_review",
      "width": 4,
      "references": {
        "0": {
          "Verbs": "reduce Add excise did abolish",
          "categorical_verbs": "inhibit undefined undefined undefined undefined",
          "sentences": "Adding PP2A, PP2C, or alkaline phosphatase to excised patches reduced CFTR channel activity by > 90% but did not abolish it  completely.",
          "PMID": 9612228
        },
        "1": {
          "Verbs": "associate associate was shortene report",
          "categorical_verbs": "neutral neutral undefined undefined undefined",
          "sentences": "By contrast, deactivation by exogenous PP2A was associated with a dramatic shortening of burst duration similar to that  reported previously in patches from cardiac cells during deactivation of CFTR by  endogenous phosphatases.",
          "PMID": 9612228
        },
        "2": {
          "Verbs": "was demonstrate is are be require",
          "categorical_verbs": "undefined undefined undefined undefined undefined undefined",
          "sentences": "Rundown of CFTR-mediated current across intact T84 epithelial cell monolayers was insensitive to toxic levels of the PP2A inhibitor  calyculin A. These results demonstrate that exogenous PP2C is a potent regulator  of CFTR activity, that its effects on single-channel gating are distinct from those of PP2A but similar to those of endogenous phosphatases in CHO, BHK, and T84 epithelial cells, and that multiple protein phosphatases may be required for  complete deactivation of CFTR channels.",
          "PMID": 9612228
        },
        "3": {
          "Verbs": "express did",
          "categorical_verbs": "activate undefined",
          "sentences": "A monoclonal anti-CFTR antibody co-precipitated type 2C protein phosphatase (PP2C) from baby hamster kidney cells stably expressing CFTR but did not co-precipitate PP1, PP2A, or PP2B.",
          "PMID": 10506164
        },
        "4": {
          "Verbs": "was did",
          "categorical_verbs": "undefined undefined",
          "sentences": "Chemical cross-linking was specific for PP2C, because PP1, PP2A, and PP2B did not co-purify with CFTR(His10) after dithiobis (sulfosuccinimidyl propionate) exposure.",
          "PMID": 10506164
        },
        "5": {
          "Verbs": "was shown",
          "categorical_verbs": "undefined undefined",
          "sentences": "A direct interaction of the regulatory domain (R domain) of the cystic fibrosis transmembrane conductance regulator protein (CFTR) with PR65, a regulatory subunit of the protein phosphatase 2A (PP2A), was shown in yeast two hybrid, pull-down and co-immunoprecipitation experiments.",
          "PMID": 15936019
        },
        "6": {
          "Verbs": "Taken show",
          "categorical_verbs": "undefined undefined",
          "sentences": "Taken together our results show a direct and functional interaction between CFTR and PP2A.",
          "PMID": 15936019
        },
        "7": {
          "Verbs": "find associate associate",
          "categorical_verbs": "neutral neutral neutral",
          "sentences": "We find that the serine/threonine phosphatase PP2A can physically associate with the CFTR COOH terminus.",
          "PMID": 16239222
        },
        "8": {
          "Verbs": "bind find associate associate identify",
          "categorical_verbs": "neutral neutral neutral neutral undefined",
          "sentences": "By mass spectrometry, we identified the exact PP2A regulatory subunits associated with CFTR as Aalpha and B'epsilon and find that the B'epsilon subunit binds CFTR  directly.",
          "PMID": 16239222
        },
        "9": {
          "Verbs": "localize",
          "categorical_verbs": "undefined",
          "sentences": "PP2A subunits localize to the apical surface of airway epithelia and PP2A phosphatase activity co-purifies with CFTR in Calu-3 cells.",
          "PMID": 16239222
        },
        "10": {
          "Verbs": "excise",
          "categorical_verbs": "undefined",
          "sentences": "In functional assays, inhibitors of PP2A block rundown of basal CFTR currents and increase channel activity in excised patches of airway epithelia and in intact mouse jejunum.",
          "PMID": 16239222
        },
        "11": {
          "Verbs": "differentiate result",
          "categorical_verbs": "undefined undefined",
          "sentences": "Moreover, PP2A inhibition in well differentiated human bronchial epithelial cells results in a CFTR-dependent increase in the airway surface liquid.",
          "PMID": 16239222
        },
        "12": {
          "Verbs": "demonstrate is",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our data demonstrate that PP2A is a relevant CFTR phosphatase in epithelial tissues.",
          "PMID": 16239222
        },
        "13": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "Differential regulation of single CFTR channels by PP2C, PP2A, and other phosphatases.",
          "PMID": 9612228
        },
        "14": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "Differential regulation of single CFTR channels by PP2C, PP2A, and other\nphosphatases.",
          "PMID": 9612228
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRPP2B",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "PP2B",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "PP2B",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express did",
          "categorical_verbs": "activate undefined",
          "sentences": "A monoclonal anti-CFTR antibody co-precipitated type 2C protein phosphatase (PP2C) from baby hamster kidney cells stably expressing CFTR but did not co-precipitate PP1, PP2A, or PP2B.",
          "PMID": 10506164
        },
        "1": {
          "Verbs": "was did",
          "categorical_verbs": "undefined undefined",
          "sentences": "Chemical cross-linking was specific for PP2C, because PP1, PP2A, and PP2B did not co-purify with CFTR(His10) after dithiobis (sulfosuccinimidyl propionate) exposure.",
          "PMID": 10506164
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "PP2APP2B",
      "class": "stimulation",
      "cardinality": 0,
      "source": "PP2A",
      "target": "PP2B",
      "bendPointPositions": [],
      "portSource": "PP2A",
      "portTarget": "PP2B",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "express did",
          "categorical_verbs": "activate undefined",
          "sentences": "A monoclonal anti-CFTR antibody co-precipitated type 2C protein phosphatase (PP2C) from baby hamster kidney cells stably expressing CFTR but did not co-precipitate PP1, PP2A, or PP2B.",
          "PMID": 10506164
        },
        "1": {
          "Verbs": "was did",
          "categorical_verbs": "undefined undefined",
          "sentences": "Chemical cross-linking was specific for PP2C, because PP1, PP2A, and PP2B did not co-purify with CFTR(His10) after dithiobis (sulfosuccinimidyl propionate) exposure.",
          "PMID": 10506164
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AMPPPi",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "PPi",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "PPi",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "share overlapp include",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "By immunofluorescent imaging, AMPK and CFTR share an overlapping apical distribution in several rat epithelial tissues, including nasopharynx, submandibular gland, pancreas, and ileum.",
          "PMID": 12519745
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRPPi",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "PPi",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "PPi",
      "review_status": "to_review",
      "width": 5,
      "references": {
        "0": {
          "Verbs": "was detect identify base",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "CFTR immunoreactivity was detected in CCV by immunoblot and was identified as CFTR based on labeling of immunoprecipitates with protein kinase A and by tryptic phosphopeptide mapping.",
          "PMID": 7510684
        },
        "1": {
          "Verbs": "share overlapp include",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "By immunofluorescent imaging, AMPK and CFTR share an overlapping apical distribution in several rat epithelial tissues, including nasopharynx, submandibular gland, pancreas, and ileum.",
          "PMID": 12519745
        },
        "2": {
          "Verbs": "bind swapp reveale",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "Domain swapping experiments and deletion analysis revealed that gp78 binds to CFTRDeltaF508 through its ubiquitin binding region, the so-called coupling of ubiquitin to ER degradation (CUE) domain.",
          "PMID": 18216283
        },
        "3": {
          "Verbs": "identify",
          "categorical_verbs": "undefined",
          "sentences": "Nucleosome mapping across the CFTR locus identifies novel regulatory factors.",
          "PMID": 23325854
        },
        "4": {
          "Verbs": "identify",
          "categorical_verbs": "undefined",
          "sentences": "Nucleosome mapping across the CFTR locus identifies novel regulatory factors.",
          "PMID": 23325854
        },
        "5": {
          "Verbs": "found upregulate use are act overlapp",
          "categorical_verbs": "neutral activate undefined undefined undefined undefined",
          "sentences": "By using miRNome profiling and gene reporter assays, we found that miR-101 and miR-145 are specifically upregulated in adult lung and that miR-101 directly acts on its cognate site in the CFTR-3'UTR in combination with an overlapping AU-rich  element.",
          "PMID": 25186262
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "DAB2PPi",
      "class": "stimulation",
      "cardinality": 0,
      "source": "DAB2",
      "target": "PPi",
      "bendPointPositions": [],
      "portSource": "DAB2",
      "portTarget": "PPi",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "indicate perform overlapp",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "The results indicate that μ2 and Dab2 performed partially overlapping, but divergent, functions.",
          "PMID": 21995445
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRRAB11B",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "RAB11B",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "RAB11B",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "reveale",
          "categorical_verbs": "undefined",
          "sentences": "Western blotting of immunoisolated Rab11a or Rab11b vesicles revealed localization of endogenous CFTR within both compartments.",
          "PMID": 19244346
        },
        "1": {
          "Verbs": "inhibite stimulate express increase perform demonstrate",
          "categorical_verbs": "inhibit neutral activate activate undefined undefined",
          "sentences": "CFTR function  assays performed on T84 cells expressing the Rab11a or Rab11b GDP-locked S25N mutants demonstrated that only the Rab11b mutant inhibited 80% of the cAMP-activated halide efflux and that only the constitutively active Rab11b-Q70L  increased the rate constant for stimulated halide efflux.",
          "PMID": 19244346
        },
        "2": {
          "Verbs": "reduce",
          "categorical_verbs": "inhibit",
          "sentences": "Similarly, RNAi knockdown of Rab11b, but not Rab11a, reduced by 50% the CFTR-mediated anion conductance response.",
          "PMID": 19244346
        },
        "3": {
          "Verbs": "polarize result assess",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "In polarized T84 monolayers, adenoviral expression of Rab11b-S25N resulted in a 70% inhibition of forskolin-stimulated transepithelial  anion secretion and a 50% decrease in apical membrane CFTR as assessed by cell surface biotinylation.",
          "PMID": 19244346
        },
        "4": {
          "Verbs": "express reveale recycl polarize demonstrate",
          "categorical_verbs": "activate undefined undefined undefined undefined",
          "sentences": "Biotin protection assays revealed a robust inhibition of CFTR recycling in polarized T84 cells expressing Rab11b-S25N, demonstrating the selective requirement for the Rab11b isoform.",
          "PMID": 19244346
        },
        "5": {
          "Verbs": "regulates is detaile demonstrate polarize",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "This is the first report detailing  apical CFTR recycling in a native expression system and to demonstrate that Rab11b regulates apical recycling in polarized epithelial cells.",
          "PMID": 19244346
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "inhibit",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "GDPRAB11B",
      "class": "stimulation",
      "cardinality": 0,
      "source": "GDP",
      "target": "RAB11B",
      "bendPointPositions": [],
      "portSource": "GDP",
      "portTarget": "RAB11B",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibite stimulate express increase perform demonstrate",
          "categorical_verbs": "inhibit neutral activate activate undefined undefined",
          "sentences": "CFTR function  assays performed on T84 cells expressing the Rab11a or Rab11b GDP-locked S25N mutants demonstrated that only the Rab11b mutant inhibited 80% of the cAMP-activated halide efflux and that only the constitutively active Rab11b-Q70L  increased the rate constant for stimulated halide efflux.",
          "PMID": 19244346
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRRAB7A",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "RAB7A",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "RAB7A",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "was",
          "categorical_verbs": "undefined",
          "sentences": "CK19 prevention of Rab7A-mediated lysosomal degradation was a key mechanism in apical CFTR stabilization.",
          "PMID": 31450978
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CALURACK1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "RACK1",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "RACK1",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "reveale",
          "categorical_verbs": "undefined",
          "sentences": "Immunofluorescence and confocal microscopy of RACK1 and CFTR revealed colocalization of the proteins to the apical and lateral regions of Calu-3 cells.",
          "PMID": 11956211
        },
        "1": {
          "Verbs": "pull",
          "categorical_verbs": "undefined",
          "sentences": "A GST-tagged PDZ1 domain pulled down endogenous RACK1 from Calu-3 cell lysate.",
          "PMID": 15075202
        },
        "2": {
          "Verbs": "inhibit block bind block affect embedd pull do",
          "categorical_verbs": "inhibit inhibit neutral inhibit neutral undefined undefined undefined",
          "sentences": "An internal 11-amino acid motif embedding the GYGF carboxylate binding loop of PDZ1 binds to RACK1, inhibits binding of recombinant NHERF1 and RACK1, pulls down endogenous RACK1 from Calu-3 cell lysate, and blocks coimmunoprecipitation of endogenous RACK1 with endogenous NHERF1 but does not affect cAMP-dependent activation of CFTR.",
          "PMID": 15075202
        },
        "3": {
          "Verbs": "indicate encompass",
          "categorical_verbs": "undefined undefined",
          "sentences": "Our results indicate binding of Calu-3 RACK1 predominantly to the PDZ1 domain of NHERF1 at a  site encompassing the GYGF loop of the PDZ1 domain and a site on RACK1 distinct from a PKC-epsilon binding site.",
          "PMID": 15075202
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRRACK1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "RACK1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "RACK1",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "activate bind Us show do",
          "categorical_verbs": "activate neutral undefined undefined undefined",
          "sentences": "Using overlay  assay, immunoprecipitation, pulldown and binding assays, we show that PKC epsilon does not bind to CFTR, but does bind to a receptor for activated C kinase (RACK1), a 37-kDa scaffold protein, and that RACK1 binds to Na(+)/H(+) exchange regulatory factor (NHERF1), a binding partner of CFTR.",
          "PMID": 11956211
        },
        "1": {
          "Verbs": "bind express do",
          "categorical_verbs": "neutral activate undefined",
          "sentences": "A 4-amino acid sequence INAL (70-73) expressed in CFTR shares 50% homology  to the RACK1 inhibitory peptide, but it does not bind PKC epsilon.",
          "PMID": 11956211
        },
        "2": {
          "Verbs": "reveale",
          "categorical_verbs": "undefined",
          "sentences": "Immunofluorescence and confocal microscopy of RACK1 and CFTR revealed colocalization of the proteins to the apical and lateral regions of Calu-3 cells.",
          "PMID": 11956211
        },
        "3": {
          "Verbs": "bind indicate serv anchor",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "The results indicate the RACK1 binds PKC epsilon and NHERF1, thus serving as a scaffold protein to anchor the enzyme in proximity  to CFTR.",
          "PMID": 11956211
        },
        "4": {
          "Verbs": "activate demonstrate",
          "categorical_verbs": "activate undefined",
          "sentences": "In past studies, we demonstrated regulation of CFTR Cl channel function by protein kinase C (PKC)-epsilon through the binding of PKC-epsilon to RACK1 (a receptor for activated C-kinase) and of RACK1 to human Na(+)/H(+) exchanger regulatory factor (NHERF1).",
          "PMID": 15075202
        },
        "5": {
          "Verbs": "inhibit block bind block affect embedd pull do",
          "categorical_verbs": "inhibit inhibit neutral inhibit neutral undefined undefined undefined",
          "sentences": "An internal 11-amino acid motif embedding the GYGF carboxylate binding loop of PDZ1 binds to RACK1, inhibits binding of recombinant NHERF1 and RACK1, pulls down endogenous RACK1 from Calu-3 cell lysate, and blocks coimmunoprecipitation of endogenous RACK1 with endogenous NHERF1 but does not affect cAMP-dependent activation of CFTR.",
          "PMID": 15075202
        },
        "6": {
          "Verbs": "affect is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CFTR activation by cAMP-generating agent is not  affected by loss of RACK1-NHERF1 interaction.",
          "PMID": 15075202
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "Na+RACK1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Na+",
      "target": "RACK1",
      "bendPointPositions": [],
      "portSource": "Na+",
      "portTarget": "RACK1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "activate bind involves",
          "categorical_verbs": "activate neutral undefined",
          "sentences": "Protein kinase C epsilon-dependent regulation of cystic fibrosis transmembrane regulator involves binding to a receptor for activated C kinase (RACK1) and RACK1 binding to Na+/H+ exchange regulatory factor.",
          "PMID": 11956211
        },
        "1": {
          "Verbs": "activate bind involves",
          "categorical_verbs": "activate neutral undefined",
          "sentences": "Protein kinase C epsilon-dependent regulation of cystic fibrosis transmembrane regulator involves binding to a receptor for activated C kinase (RACK1) and RACK1 binding to Na+/H+ exchange regulatory factor.",
          "PMID": 11956211
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRRNF4",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "RNF4",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "RNF4",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "block block elicite",
          "categorical_verbs": "inhibit inhibit undefined",
          "sentences": "RNF4 overexpression elicited F508del degradation, whereas Hsp27 knockdown blocked RNF4's impact on mutant CFTR.",
          "PMID": 23155000
        },
        "1": {
          "Verbs": "degrade was lost",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Similarly, the ability of Hsp27 to degrade F508del CFTR was lost during overexpression of dominant-negative RNF4.",
          "PMID": 23155000
        },
        "2": {
          "Verbs": "collaborates degrade",
          "categorical_verbs": "neutral undefined",
          "sentences": "Hsp27 collaborates with Ubc9,  the E2 enzyme for protein SUMOylation, to selectively degrade F508del CFTR via the SUMO-targeted ubiquitin E3 ligase, RNF4 (RING finger protein 4) (1).",
          "PMID": 26627832
        },
        "3": {
          "Verbs": "block block reduce",
          "categorical_verbs": "inhibit inhibit inhibit",
          "sentences": "PIAS4 reduced endogenous ubiquitin conjugation to F508del CFTR by ∼50% and blocked the impact of RNF4 on mutant CFTR disposal.",
          "PMID": 30403549
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRS100A10",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "S100A10",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "S100A10",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "find acetylate correspond disrupt",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Furthermore, we find that the acetylated peptide (STVHEILCKLSLEG, Ac1-14), but not the nonacetylated equivalent N1-14, corresponding to the S100A10 binding site on anx 2, disrupts the anx 2-S100A10/CFTR complex.",
          "PMID": 17581860
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "ARF1SEC12",
      "class": "stimulation",
      "cardinality": 0,
      "source": "ARF1",
      "target": "SEC12",
      "bendPointPositions": [],
      "portSource": "ARF1",
      "portTarget": "SEC12",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "promotes",
          "categorical_verbs": "activate",
          "sentences": "mSec12 promotes efficient guanine nucleotide exchange on Sar1, but not Arf1 or Rab GTPases.",
          "PMID": 11422940
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "COPISEC12",
      "class": "stimulation",
      "cardinality": 0,
      "source": "COPI",
      "target": "SEC12",
      "bendPointPositions": [],
      "portSource": "COPI",
      "portTarget": "SEC12",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibit is",
          "categorical_verbs": "inhibit undefined",
          "sentences": "mSec12 is localized to the endoplasmic reticulum and an antibody to the cytosolic domain of mSec12 potently inhibits Sar1 recruitment and the formation of COPII vesicles in vitro.",
          "PMID": 11422940
        },
        "1": {
          "Verbs": "is shown be prevent",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "The dominant negative GDP-restricted mutant Sar1[T39N] is shown to be a potent inhibitor of mSec12 activity, consistent with its role in preventing COPII vesicle formation in vitro and during transient expression in vivo.",
          "PMID": 11422940
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "GDPSEC12",
      "class": "stimulation",
      "cardinality": 0,
      "source": "GDP",
      "target": "SEC12",
      "bendPointPositions": [],
      "portSource": "GDP",
      "portTarget": "SEC12",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "is shown be prevent",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "The dominant negative GDP-restricted mutant Sar1[T39N] is shown to be a potent inhibitor of mSec12 activity, consistent with its role in preventing COPII vesicle formation in vitro and during transient expression in vivo.",
          "PMID": 11422940
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "GTPSEC12",
      "class": "stimulation",
      "cardinality": 0,
      "source": "GTP",
      "target": "SEC12",
      "bendPointPositions": [],
      "portSource": "GTP",
      "portTarget": "SEC12",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "is direct",
          "categorical_verbs": "undefined undefined",
          "sentences": "The mammalian guanine nucleotide exchange factor mSec12 is essential for activation of the Sar1 GTPase directing endoplasmic reticulum export.",
          "PMID": 11422940
        },
        "1": {
          "Verbs": "is direct",
          "categorical_verbs": "undefined undefined",
          "sentences": "The mammalian guanine nucleotide exchange factor mSec12 is essential for activation of the Sar1 GTPase directing endoplasmic reticulum export.",
          "PMID": 11422940
        },
        "2": {
          "Verbs": "promotes",
          "categorical_verbs": "activate",
          "sentences": "mSec12 promotes efficient guanine nucleotide exchange on Sar1, but not Arf1 or Rab GTPases.",
          "PMID": 11422940
        },
        "3": {
          "Verbs": "propose is direct",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "We propose that mSec12 is an evolutionarily  distant guanine nucleotide exchange factor directing Sar1 GTPase activation in mammalian cells.",
          "PMID": 11422940
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "BAP31SEC61B",
      "class": "stimulation",
      "cardinality": 0,
      "source": "BAP31",
      "target": "SEC61B",
      "bendPointPositions": [],
      "portSource": "BAP31",
      "portTarget": "SEC61B",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "interact show",
          "categorical_verbs": "neutral undefined",
          "sentences": "Here, we show that a part of the BAP31 population interacts with two components of the Sec61 preprotein translocon, Sec61beta and TRAM.",
          "PMID": 18555783
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRSHANK2",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "SHANK2",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "SHANK2",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "report",
          "categorical_verbs": "undefined",
          "sentences": "In the present study, we report  on the biochemical and functional association between cystic fibrosis transmembrane conductance regulator (CFTR) and a PDZ domain-containing protein Shank2.",
          "PMID": 14679199
        },
        "1": {
          "Verbs": "showe",
          "categorical_verbs": "undefined",
          "sentences": "Shank2, one of these scaffolding proteins, showed a strong interaction with CFTR by yeast two-hybrid assays.",
          "PMID": 14679199
        },
        "2": {
          "Verbs": "were measure",
          "categorical_verbs": "undefined undefined",
          "sentences": "Protein phosphorylation, HCO(3)(-) transport and Cl(-) current by CFTR were measured in NIH 3T3 cells with heterologous expression of Shank2.",
          "PMID": 14679199
        },
        "3": {
          "Verbs": "suppress",
          "categorical_verbs": "inhibit",
          "sentences": "Of interest, expression of Shank2 suppressed cAMP-induced phosphorylation and activation of CFTR.",
          "PMID": 14679199
        },
        "4": {
          "Verbs": "express increase were",
          "categorical_verbs": "activate activate undefined",
          "sentences": "Importantly, loss of Shank2  by stable transfection of antisense-hShank2 plasmid strongly increased CFTR currents in colonic T84 cells, in which CFTR and Shank2 were natively expressed.",
          "PMID": 14679199
        },
        "5": {
          "Verbs": "regulates indicate play maintaine present",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "Our results indicate that Shank2 negatively regulates CFTR and that this may play a significant role in maintaining epithelial homeostasis under normal and diseased conditions such as those presented by secretory diarrhea.",
          "PMID": 14679199
        },
        "6": {
          "Verbs": "is establish are known",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Although a positive regulation by PDZ domain-based adaptors such as EBP50/NHERF1 is established, the mechanisms for negative regulation of the CFTR by Shank2, as well as the effects of multiple adaptor interactions, are not known.",
          "PMID": 17244609
        },
        "7": {
          "Verbs": "associate associate found recruit was be precludes",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined undefined",
          "sentences": "Furthermore whereas EBP50 recruits a cAMP-dependent protein kinase (PKA) complex to CFTR, Shank2 was found to be physically and functionally associated with the cyclic nucleotide phosphodiesterase PDE4D that precludes cAMP/PKA signals in epithelial cells and mouse brains.",
          "PMID": 17244609
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "PDE4DSHANK2",
      "class": "stimulation",
      "cardinality": 0,
      "source": "PDE4D",
      "target": "SHANK2",
      "bendPointPositions": [],
      "portSource": "PDE4D",
      "portTarget": "SHANK2",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "associate associate found recruit was be precludes",
          "categorical_verbs": "neutral neutral neutral undefined undefined undefined undefined",
          "sentences": "Furthermore whereas EBP50 recruits a cAMP-dependent protein kinase (PKA) complex to CFTR, Shank2 was found to be physically and functionally associated with the cyclic nucleotide phosphodiesterase PDE4D that precludes cAMP/PKA signals in epithelial cells and mouse brains.",
          "PMID": 17244609
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRSIN3A",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "SIN3A",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "SIN3A",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "regulates discovere",
          "categorical_verbs": "neutral undefined",
          "sentences": "We discovered that  miRNA-138 regulates CFTR expression through its interactions with the transcriptional regulatory protein SIN3A.",
          "PMID": 22853952
        },
        "1": {
          "Verbs": "reduces repress is accompany",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "NF-Y depletion also represses Sin3A and reduces its occupancy across the CFTR locus, which is accompanied by an increase in p300 enrichment at multiple sites.",
          "PMID": 23689137
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRSLC26A6",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "SLC26A6",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "SLC26A6",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "activates",
          "categorical_verbs": "activate",
          "sentences": "In contrast, CFTR markedly activates Cl(-) and OH(-)/HCO(3)(-) transport by members of the SLC26 family DRA, SLC26A6 and pendrin.",
          "PMID": 12411484
        },
        "1": {
          "Verbs": "report",
          "categorical_verbs": "undefined",
          "sentences": "We report here a reciprocal regulatory interaction between the SLC26T DRA, SLC26A6 and CFTR.",
          "PMID": 15048129
        },
        "2": {
          "Verbs": "upregulates",
          "categorical_verbs": "activate",
          "sentences": "Oestrogen upregulates the expression levels and functional activities of duodenal mucosal CFTR and SLC26A6.",
          "PMID": 27615377
        },
        "3": {
          "Verbs": "upregulates",
          "categorical_verbs": "activate",
          "sentences": "Oestrogen upregulates the expression levels and functional activities of duodenal mucosal CFTR and SLC26A6.",
          "PMID": 27615377
        },
        "4": {
          "Verbs": "mediate are",
          "categorical_verbs": "neutral undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) and the solute-linked carrier 26 gene family A6 (SLC26A6) are two key bicarbonate transport proteins that mediate duodenal mucosal bicarbonate secretion.",
          "PMID": 27615377
        },
        "5": {
          "Verbs": "upregulates demonstrate contributes",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "We demonstrate that endogenous oestrogen upregulates the expression levels and functional activities of duodenal mucosal CFTR and SLC26A6, which contributes to the sex difference in the prevalence of duodenal ulcer.",
          "PMID": 27615377
        },
        "6": {
          "Verbs": "mediate are is",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "The cystic fibrosis transmembrane conductance regulator (CFTR) and the solute-linked carrier 26 gene family A6 (SLC26A6) are two key bicarbonate transport proteins that mediate duodenal mucosal bicarbonate secretion, which is an important protective  factor against acid-induced duodenal injury.",
          "PMID": 27615377
        },
        "7": {
          "Verbs": "was investigate",
          "categorical_verbs": "undefined undefined",
          "sentences": "The aim of this study was to investigate the effect of oestrogen on the expressions and functional activities  of CFTR and SLC26A6 in duodenal mucosa.",
          "PMID": 27615377
        },
        "8": {
          "Verbs": "found were",
          "categorical_verbs": "neutral undefined",
          "sentences": "We found that the expression levels of duodenal CFTR and SLC26A6 were markedly higher in young (20- to 30-year-old) women than in young men and old (60- to 70-year-old) women and men.",
          "PMID": 27615377
        },
        "9": {
          "Verbs": "were was",
          "categorical_verbs": "undefined undefined",
          "sentences": "The expression levels of CFTR and SLC26A6 in young women were markedly higher in preovulatory phases than in premenstrual phases, which was consistent with the changes of serum estradiol concentrations.",
          "PMID": 27615377
        },
        "10": {
          "Verbs": "decrease showe were revers",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Further results showed that duodenal CFTR and SLC26A6 expression levels in female mice were markedly decreased after ovariectomy, and supplementation with estradiol reversed the changes in CFTR and  SLC26A6.",
          "PMID": 27615377
        },
        "11": {
          "Verbs": "increase",
          "categorical_verbs": "activate",
          "sentences": "17β-Estradiol increased CFTR and SLC26A6 expression levels of human duodenocytes in experiments in vitro.",
          "PMID": 27615377
        },
        "12": {
          "Verbs": "upregulates contribute explain",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "In conclusion, endogenous oestrogen upregulates the expressions and functional activities of CFTR and SLC26A6 in duodenal mucosa, which could contribute to protection of the duodenum and explain the sex difference in the prevalence of duodenal ulcer.",
          "PMID": 27615377
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "activate",
      "activate",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "inhibit",
      "undefined",
      "activate",
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "EstradiolSLC26A6",
      "class": "stimulation",
      "cardinality": 0,
      "source": "Estradiol",
      "target": "SLC26A6",
      "bendPointPositions": [],
      "portSource": "Estradiol",
      "portTarget": "SLC26A6",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "increase",
          "categorical_verbs": "activate",
          "sentences": "17β-Estradiol increased CFTR and SLC26A6 expression levels of human duodenocytes in experiments in vitro.",
          "PMID": 27615377
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "SLC26A3SLC26A6",
      "class": "stimulation",
      "cardinality": 0,
      "source": "SLC26A3",
      "target": "SLC26A6",
      "bendPointPositions": [],
      "portSource": "SLC26A3",
      "portTarget": "SLC26A6",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "[downregulate were detect",
          "categorical_verbs": "inhibit undefined undefined",
          "sentences": "mRNA and protein for the apical anion exchangers SLC26A3 [downregulated in adenoma (DRA)]  and SLC26A6 [putative anion transporter (PAT1)] were detected in T84 cells by RT-PCR and Northern and Western blots.",
          "PMID": 16002564
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRSLC26A9",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "SLC26A9",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "SLC26A9",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "inhibit associate associate has",
          "categorical_verbs": "inhibit neutral neutral undefined",
          "sentences": "Cl(-) channel inhibitors DIDS and NS 3623 inhibit SLC26A9 associated currents while the specific CFTR inhibitor (CFTR(inh)-172) or glybenclamide has little effect.",
          "PMID": 18769029
        },
        "1": {
          "Verbs": "block associate block increase associate is",
          "categorical_verbs": "inhibit neutral inhibit activate neutral undefined",
          "sentences": "Elevation of intracellular cAMP (a CFTR activator) is also ineffective whereas increasing intracellular calcium blocks the SLC26A9 associated currents.",
          "PMID": 18769029
        },
        "2": {
          "Verbs": "elucidate remain be",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "SLC26A9 physiological role in airway epithelia and its potential interaction with CFTR remain to be elucidated.",
          "PMID": 18769029
        },
        "3": {
          "Verbs": "stimulates",
          "categorical_verbs": "neutral",
          "sentences": "SLC26A9 stimulates CFTR expression and function in human bronchial cell lines.",
          "PMID": 20658517
        },
        "4": {
          "Verbs": "stimulates",
          "categorical_verbs": "neutral",
          "sentences": "SLC26A9 stimulates CFTR expression and function in human bronchial cell lines.",
          "PMID": 20658517
        },
        "5": {
          "Verbs": "investigate",
          "categorical_verbs": "undefined",
          "sentences": "We investigated the possible functional- and physical protein-interactions between two airway Cl(-) channels, SLC26A9 and CFTR.",
          "PMID": 20658517
        },
        "6": {
          "Verbs": "express were transduce",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "Bronchial CFBE41o- cell lines expressing CFTR(WT) or CFTR(ΔF508) were transduced with SLC26A9.",
          "PMID": 20658517
        },
        "7": {
          "Verbs": "increase were",
          "categorical_verbs": "activate undefined",
          "sentences": "CFTR levels were increased by the presence of SLC26A9 in both CFTR(WT) and CFTR(ΔF508) cell lines.",
          "PMID": 20658517
        },
        "8": {
          "Verbs": "associate associate transduce were detect",
          "categorical_verbs": "neutral neutral undefined undefined undefined",
          "sentences": "In CFBE41o- cells and CFBE41o-/CFTR(WT) cells transduced  with SLC26A9, currents associated to the protein expression were not detected.",
          "PMID": 20658517
        },
        "9": {
          "Verbs": "result )",
          "categorical_verbs": "undefined undefined",
          "sentences": "Therefore, the presence of SLC26A9 resulted in an increase in CFTR activity (same % of CFTR((inh)-172) or GlyH-101 inhibition in both groups).",
          "PMID": 20658517
        },
        "10": {
          "Verbs": "associate associate transduce was lack",
          "categorical_verbs": "neutral neutral undefined undefined undefined",
          "sentences": "In CFBE41o-/CFTR(ΔF508) cells transduced with SLC26A9 (at 27°C), a current associated to the protein expression was also lacking.",
          "PMID": 20658517
        },
        "11": {
          "Verbs": "express enhanc compare",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "The presence of SLC26A9 in Xenopus oocytes expressing CFTR also enhanced the FK-stimulated currents as compared to oocytes expressing CFTR alone.",
          "PMID": 20658517
        },
        "12": {
          "Verbs": "found co-express was",
          "categorical_verbs": "neutral activate undefined",
          "sentences": "An enhancement of FK-stimulated currents was not found in oocytes co-expressing SLC26A9 and CFTR(ΔF508).",
          "PMID": 20658517
        },
        "13": {
          "Verbs": "stimulates use",
          "categorical_verbs": "neutral undefined",
          "sentences": "In conclusion, in both protein expression systems used, SLC26A9 stimulates CFTR activity but not that of CFTR(ΔF508).",
          "PMID": 20658517
        },
        "14": {
          "Verbs": "stimulate propose favor leade",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "We propose as an alternative hypothesis (not exclusive) to the known SLC26A9-STAS domain/CFTR interaction, that SLC26A9 favors the biogenesis and/or stabilization  of CFTR, leading to stimulated currents.",
          "PMID": 20658517
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "undefined",
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "BECN1SQSTM1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "BECN1",
      "target": "SQSTM1",
      "bendPointPositions": [],
      "portSource": "BECN1",
      "portTarget": "SQSTM1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "reduce prevent recycl enhanc",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Addition of cystamine prevented the recycling defect of CFTR by enhancing BECN1 expression and reducing SQSTM1 accumulation.",
          "PMID": 23686137
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRSQSTM1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "SQSTM1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "SQSTM1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "Inhibite result favore",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Inhibiting CFTR function also resulted in its ubiquitination and interaction with SQSTM1/p62 at the PM, favoring its disposal.",
          "PMID": 23686137
        },
        "1": {
          "Verbs": "reduce prevent recycl enhanc",
          "categorical_verbs": "inhibit undefined undefined undefined",
          "sentences": "Addition of cystamine prevented the recycling defect of CFTR by enhancing BECN1 expression and reducing SQSTM1 accumulation.",
          "PMID": 23686137
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "inhibit",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRSTX8",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "STX8",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "STX8",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "increase is thought be show rescues allowe reach",
          "categorical_verbs": "activate undefined undefined undefined undefined undefined undefined undefined",
          "sentences": "RESULTS: Although the SNARE protein STX8 is thought to be functionally related and primarily localized to early endosomes, we show that silencing of STX8, particularly in the presence of the Vertex corrector molecule C18, rescues ΔF508-CFTR, allowing it to reach the cell surface and increasing CFTR-dependent chloride currents by approximately 2.5-fold over control values.",
          "PMID": 30485852
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRTNF",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "TNF",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "TNF",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "modulate Base be contain respond evaluate",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined",
          "sentences": "Based on the knowledge that expression of the cystic fibrosis transmembrane conductance regulator (CFTR) gene can be modulated at the transcriptional level,  and that the CFTR gene promoter contains sequences homologous to elements in other promoters that respond to tumor necrosis factor-alpha (TNF), we evaluated the hypothesis that TNF might modulate CFTR gene expression in epithelial cells.",
          "PMID": 1281791
        },
        "1": {
          "Verbs": "expres downregulate known demonstrate",
          "categorical_verbs": "activate inhibit undefined undefined",
          "sentences": "Studies with HT-29 cells, a colon epithelium-derived tumor cell line known to express the CFTR gene, demonstrated that TNF downregulated CFTR mRNA transcript levels in a dose- and time-dependent fashion.",
          "PMID": 1281791
        },
        "2": {
          "Verbs": "affect reduce demonstrate did modify result was rest",
          "categorical_verbs": "neutral inhibit undefined undefined undefined undefined undefined undefined",
          "sentences": "Interestingly, nuclear run-on analyses demonstrated that TNF did not affect the rate of transcription of CFTR gene, but exposure of the cells to TNF did modify the stability of CFTR mRNA transcripts, resulting in a mRNA half-life that was reduced to 65% of the resting level.",
          "PMID": 1281791
        },
        "3": {
          "Verbs": "modulate suggest be",
          "categorical_verbs": "neutral undefined undefined",
          "sentences": "These observations suggest that CFTR gene expression can be modulated by TNF, at least in part, at the posttranscriptional level.",
          "PMID": 1281791
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRUBC9",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "UBC9",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "UBC9",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "collaborates degrade",
          "categorical_verbs": "neutral undefined",
          "sentences": "Hsp27 collaborates with Ubc9,  the E2 enzyme for protein SUMOylation, to selectively degrade F508del CFTR via the SUMO-targeted ubiquitin E3 ligase, RNF4 (RING finger protein 4) (1).",
          "PMID": 26627832
        },
        "1": {
          "Verbs": "ask are sens examine mimic",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "Here, we ask what properties of CFTR are sensed by the Hsp27-Ubc9 pathway by examining the ability of NBD1 (locus of the F508del mutation) to mimic the disposal of full-length (FL) CFTR.",
          "PMID": 26627832
        },
        "2": {
          "Verbs": "bind is initiate cooperates modify",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "A pathway for cystic fibrosis transmembrane conductance regulator (CFTR) degradation is initiated by Hsp27, which cooperates with Ubc9 and binds to the common F508del mutant to modify it with SUMO-2/3.",
          "PMID": 30403549
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "RNF4UBC9",
      "class": "stimulation",
      "cardinality": 0,
      "source": "RNF4",
      "target": "UBC9",
      "bendPointPositions": [],
      "portSource": "RNF4",
      "portTarget": "UBC9",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "collaborates degrade",
          "categorical_verbs": "neutral undefined",
          "sentences": "Hsp27 collaborates with Ubc9,  the E2 enzyme for protein SUMOylation, to selectively degrade F508del CFTR via the SUMO-targeted ubiquitin E3 ligase, RNF4 (RING finger protein 4) (1).",
          "PMID": 26627832
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRUSP10",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "USP10",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "USP10",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "regulates Us identify demonstrate is",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "Using an activity-based chemical screen to identify active DUBs in human airway epithelial cells, we demonstrated that Ubiquitin Specific Protease-10 (USP10) is located in early endosomes and regulates the deubiquitination of CFTR and its trafficking in the post-endocytic compartment.",
          "PMID": 19398555
        },
        "1": {
          "Verbs": "increase reduce",
          "categorical_verbs": "activate inhibit",
          "sentences": "small interference RNA-mediated knockdown of USP10 increased the amount of ubiquitinated CFTR and its degradation in lysosomes, and  reduced both apical membrane CFTR and CFTR-mediated chloride secretion.",
          "PMID": 19398555
        },
        "2": {
          "Verbs": "increase decrease",
          "categorical_verbs": "activate inhibit",
          "sentences": "Moreover, a dominant negative USP10 (USP10-C424A) increased the amount of ubiquitinated CFTR and its degradation, whereas overexpression of wt-USP10 decreased the amount of ubiquitinated CFTR and increased the abundance of CFTR.",
          "PMID": 19398555
        },
        "3": {
          "Verbs": "demonstrate facilitate enhanc",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "These studies demonstrate a novel function for USP10 in facilitating the deubiquitination of CFTR in early endosomes and thereby enhancing the endocytic recycling of CFTR.",
          "PMID": 19398555
        },
        "4": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "The deubiquitinating enzyme USP10 regulates the endocytic recycling of CFTR in airway epithelial cells.",
          "PMID": 20215869
        },
        "5": {
          "Verbs": "regulates",
          "categorical_verbs": "neutral",
          "sentences": "The deubiquitinating enzyme USP10 regulates the endocytic recycling of CFTR in airway epithelial cells.",
          "PMID": 20215869
        },
        "6": {
          "Verbs": "regulates promotes report use identify deubiquitinate DUB is",
          "categorical_verbs": "neutral activate undefined undefined undefined undefined undefined undefined",
          "sentences": "We recently reported, using an activity-based chemical screen to identify active deubiquitinating enzymes (DUBs) in human airway epithelial cells, that Ubiquitin  Specific Protease-10 (USP10) is located and active in the early endosomal compartment and regulates the deubiquitination of CFTR and thereby promotes its endocytic recycling.",
          "PMID": 20215869
        },
        "7": {
          "Verbs": "increase decrease",
          "categorical_verbs": "activate inhibit",
          "sentences": "siRNA-mediated knockdown of USP10 increased the multi-ubiquitination and lysosomal degradation of CFTR and decreased the endocytic recycling and the half-life of CFTR in the apical membrane, as well as  CFTR-mediated chloride secretion.",
          "PMID": 20215869
        },
        "8": {
          "Verbs": "increase promote reduce",
          "categorical_verbs": "activate activate inhibit",
          "sentences": "Overexpression of wild-type USP10 reduced CFTR  multi-ubiquitination and degradation, while overexpression of a dominant-negative USP10 promoted increased multi-ubiquitination and lysosomal degradation of CFTR.",
          "PMID": 20215869
        },
        "9": {
          "Verbs": "show",
          "categorical_verbs": "undefined",
          "sentences": "In the current study, we show localization and activity of USP10 in the early endosomal compartment of primary bronchial epithelial cells, as well as an interaction between CFTR and USP10 in this compartment.",
          "PMID": 20215869
        },
        "10": {
          "Verbs": "demonstrate facilitate enhanc",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "These studies demonstrate a novel function for USP10 in facilitating the deubiquitination of CFTR in early  endosomes, thereby enhancing the endocytic recycling and cell surface expression  of CFTR.",
          "PMID": 20215869
        },
        "11": {
          "Verbs": "had abolish",
          "categorical_verbs": "undefined undefined",
          "sentences": "Although arsenic had no effect on the abundance or activity of USP10, a deubiquitinylating enzyme, siRNA-mediated knockdown of c-Cbl, an E3 ubiquitin ligase, abolished the arsenic-stimulated degradation of CFTR.",
          "PMID": 22467879
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "inhibit",
      "activate",
      "inhibit",
      "activate",
      "undefined",
      "neutral",
      "neutral",
      "neutral",
      "activate",
      "undefined",
      "inhibit",
      "activate",
      "inhibit",
      "activate",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "GTPUSP10",
      "class": "stimulation",
      "cardinality": 0,
      "source": "GTP",
      "target": "USP10",
      "bendPointPositions": [],
      "portSource": "GTP",
      "portTarget": "USP10",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "inhibit induce found culture",
          "categorical_verbs": "inhibit activate neutral undefined",
          "sentences": "In this study, we found that Ras GTPase-activating protein-binding protein 1 (G3BP1) inhibits ubiquitinated protein aggregations induced by p62 and USP10 in cultured cells.",
          "PMID": 31501480
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "inhibit",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRUSP19",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "USP19",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "USP19",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "rescues",
          "categorical_verbs": "undefined",
          "sentences": "USP19 rescues the ERAD substrates cystic fibrosis transmembrane conductance regulator (CFTR)DeltaF508 and T-cell receptor-alpha (TCRalpha) from proteasomal degradation.",
          "PMID": 19465887
        },
        "1": {
          "Verbs": "was rescue suggest exert",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "A catalytically inactive USP19 was still able to partly rescue TCRalpha but not CFTRDeltaF508, suggesting that USP19 might also exert a non-catalytic function on specific ERAD substrates.",
          "PMID": 19465887
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRVCP",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "VCP",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "VCP",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "show",
          "categorical_verbs": "undefined",
          "sentences": "We show here that p97/VCP and gp78 form complexes with CFTR during translocation from the ER for degradation by the cytosolic proteasome.",
          "PMID": 16621797
        },
        "1": {
          "Verbs": "associate associate confirm",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "Immunoprecipitation of DeltaF508-CFTR from primary CF bronchial epithelial cells confirmed the interaction with VCP and associated chaperones in  CF.",
          "PMID": 16621797
        },
        "2": {
          "Verbs": "facilitates enhanc",
          "categorical_verbs": "undefined undefined",
          "sentences": "gp78 facilitates the degradation of CFTRDeltaF508 by enhancing both its ubiquitination and interaction with p97/VCP.",
          "PMID": 19828134
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AMPVTI1B",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "VTI1B",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "VTI1B",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "belong contain",
          "categorical_verbs": "undefined undefined",
          "sentences": "Syntaxin 8 belongs to the endosomal SNARE complex which also contains syntaxin 7, vti1b and VAMP8.",
          "PMID": 18570918
        },
        "1": {
          "Verbs": "interact showe SNARE belong",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Moreover, co-immunoprecipitation experiments in LLC-PK1-CFTR cells showed that CFTR and SNARE proteins belong to a same complex and pull-down assays showed that VAMP8 and vti1b preferentially interact with CFTR N-terminus tail.",
          "PMID": 18570918
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRVTI1B",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "VTI1B",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "VTI1B",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "interact showe SNARE belong",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Moreover, co-immunoprecipitation experiments in LLC-PK1-CFTR cells showed that CFTR and SNARE proteins belong to a same complex and pull-down assays showed that VAMP8 and vti1b preferentially interact with CFTR N-terminus tail.",
          "PMID": 18570918
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRWNK1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "WNK1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "WNK1",
      "review_status": "to_review",
      "width": 2,
      "references": {
        "0": {
          "Verbs": "modulate",
          "categorical_verbs": "neutral",
          "sentences": "WNK1 and WNK4 modulate CFTR activity.",
          "PMID": 17194447
        },
        "1": {
          "Verbs": "modulate",
          "categorical_verbs": "neutral",
          "sentences": "WNK1 and WNK4 modulate CFTR activity.",
          "PMID": 17194447
        },
        "2": {
          "Verbs": "raise co-express are mutate alter be involv",
          "categorical_verbs": "activate activate undefined undefined undefined undefined undefined",
          "sentences": "WNK1 and WNK4, two WNK kinases that are mutated in familial hyperkalemic hypertension (FHHt), are co-expressed with CFTR in several organs, raising the possibility that WNK kinases might alter CFTR activity in vivo or that CFTR could be involved in the pathogenesis of FHHt.",
          "PMID": 17194447
        },
        "3": {
          "Verbs": "report co-localizes",
          "categorical_verbs": "undefined undefined",
          "sentences": "Here, we report that WNK1 co-localizes with CFTR protein in pulmonary epithelial cells.",
          "PMID": 17194447
        },
        "4": {
          "Verbs": "suppress",
          "categorical_verbs": "inhibit",
          "sentences": "Co-expression of WNK1 or WNK4 with CFTR in Xenopus laevis oocytes suppresses chloride channel activity.",
          "PMID": 17194447
        },
        "5": {
          "Verbs": "requires",
          "categorical_verbs": "undefined",
          "sentences": "In contrast, the effect of WNK1 on CFTR activity requires intact WNK1 kinase activity.",
          "PMID": 17194447
        },
        "6": {
          "Verbs": "exhibit",
          "categorical_verbs": "undefined",
          "sentences": "Moreover WNK1 and WNK4 exhibit additive CFTR inhibition.",
          "PMID": 17194447
        },
        "7": {
          "Verbs": "modulate Taken suggest be",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Taken together, these results suggest that WNK1 and WNK4 may modulate CFTR activity; they further suggest that WNK kinases may be potential therapeutic targets for cystic fibrosis.",
          "PMID": 17194447
        },
        "8": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "9": {
          "Verbs": "analyze",
          "categorical_verbs": "undefined",
          "sentences": "In addition, we analyzed the effects of pancreatitis-causing CFTR mutations on the WNK1-mediated regulation of CFTR.",
          "PMID": 31561038
        },
        "10": {
          "Verbs": "increase constitute was",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "RESULTS: Among the WNK1, SPAK, and OSR1 kinases that constitute a [Cl-]i-sensitive kinase cascade, the expression of WNK1 alone was sufficient to increase the CFTR bicarbonate permeability (PHCO3/PCl) and conductance (GHCO3) in patch clamp recordings.",
          "PMID": 31561038
        },
        "11": {
          "Verbs": "mediate reveale is surround",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Molecular dissection of the WNK1 domains revealed that the WNK1 kinase domain is responsible for CFTR PHCO3/PCl regulation by direct association with CFTR, while the surrounding N-terminal regions mediate the [Cl-]i-sensitivity of WNK1.",
          "PMID": 31561038
        },
        "12": {
          "Verbs": "reduce hampere",
          "categorical_verbs": "inhibit undefined",
          "sentences": "Furthermore, the pancreatitis-causing R74Q and R75Q mutations in the elbow helix 1 of CFTR hampered WNK1-CFTR physical associations and reduced WNK1-mediated CFTR PHCO3/PCl regulation.",
          "PMID": 31561038
        },
        "13": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CONCLUSION: The CFTR HCO3- channel activity is regulated by [Cl-]i and a WNK1-dependent mechanism.",
          "PMID": 31561038
        },
        "14": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "Regulation of CFTR Bicarbonate Channel Activity by WNK1: Implications for Pancreatitis and CFTR-Related Disorders.",
          "PMID": 31561038
        },
        "15": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "Regulation of CFTR Bicarbonate Channel Activity by WNK1: Implications for\nPancreatitis and CFTR-Related Disorders.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "neutral",
      "activate",
      "undefined",
      "undefined",
      "inhibit",
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "inhibit",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "HCO3-WNK1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "HCO3-",
      "target": "WNK1",
      "bendPointPositions": [],
      "portSource": "HCO3-",
      "portTarget": "WNK1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "examine use integrate include",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "METHODS: We examined the mechanisms of the CFTR HCO3- channel regulation by [Cl-]i-sensitive kinases using an integrated electrophysiological, molecular, and computational approach including whole-cell, outside-out, and inside-out patch clamp recordings and molecular dissection of WNK1 and CFTR proteins.",
          "PMID": 31561038
        },
        "1": {
          "Verbs": "regulate is",
          "categorical_verbs": "neutral undefined",
          "sentences": "CONCLUSION: The CFTR HCO3- channel activity is regulated by [Cl-]i and a WNK1-dependent mechanism.",
          "PMID": 31561038
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CFTRYY1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "YY1",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "YY1",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "transfect demonstrates containe creates",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Analysis of transiently transfected cell lines with wild-type and -102A variant human CFTR-directed luciferase reporter genes demonstrates that constructs containing the -102A variant (which creates a Yin Yang 1 (YY1) core element) increases CFTR  expression significantly.",
          "PMID": 10652351
        },
        "1": {
          "Verbs": "causes allow observ carry",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "The finding that the YY1-binding allele causes a significant increase in CFTR expression in vitro may allow a better understanding of the milder phenotype observed in patients who carry a severe CF mutation within the same gene.",
          "PMID": 10652351
        },
        "2": {
          "Verbs": "bind suggest shown are involv",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "Mutagenesis studies suggested that in addition to SRF other co-factors, such as Yin Yang 1 (YY1) previously shown to bind the CFTR promoter,  are potentially involved in the CFTR regulation.",
          "PMID": 16170155
        },
        "3": {
          "Verbs": "show provide characterize underly",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "Here, we show that functional interplay between SRF and YY1 might provide interesting perspectives to further characterize the underlying molecular mechanism of the basal CFTR transcriptional activity.",
          "PMID": 16170155
        },
        "4": {
          "Verbs": "increases promotes demonstrate",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We demonstrated that Nrf2 promotes YY1 nuclear localization and increases its binding to the CFTR promoter.",
          "PMID": 20309604
        },
        "5": {
          "Verbs": "is report contributes clarify leade",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "To our knowledge, this study is the first to report a repressor role of Nrf2 through the cooperation with YY1 and contributes to clarify the cascade events leading to the oxidative stress-suppressed CFTR expression.",
          "PMID": 20309604
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "NRF2YY1",
      "class": "stimulation",
      "cardinality": 0,
      "source": "NRF2",
      "target": "YY1",
      "bendPointPositions": [],
      "portSource": "NRF2",
      "portTarget": "YY1",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "decipher involv evaluate showe",
          "categorical_verbs": "undefined undefined undefined undefined",
          "sentences": "To decipher the molecular mechanisms involved, we evaluated the role of YY1 in the Nrf2-mediated transcriptional activity and showed cooperation between these two factors.",
          "PMID": 20309604
        },
        "1": {
          "Verbs": "increases promotes demonstrate",
          "categorical_verbs": "activate activate undefined",
          "sentences": "We demonstrated that Nrf2 promotes YY1 nuclear localization and increases its binding to the CFTR promoter.",
          "PMID": 20309604
        },
        "2": {
          "Verbs": "is report contributes clarify leade",
          "categorical_verbs": "undefined undefined undefined undefined undefined",
          "sentences": "To our knowledge, this study is the first to report a repressor role of Nrf2 through the cooperation with YY1 and contributes to clarify the cascade events leading to the oxidative stress-suppressed CFTR expression.",
          "PMID": 20309604
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "activate",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "AMPclathrin",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AMP",
      "target": "clathrin",
      "bendPointPositions": [],
      "portSource": "AMP",
      "portTarget": "clathrin",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "controll express controll is",
          "categorical_verbs": "neutral activate neutral undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl(-) channel expressed in the apical plasma membrane of fluid-transporting epithelia, where the plasma membrane abundance of CFTR is in part controlled by clathrin-mediated endocytosis.",
          "PMID": 22399289
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "activate",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "AP2clathrin",
      "class": "stimulation",
      "cardinality": 0,
      "source": "AP2",
      "target": "clathrin",
      "bendPointPositions": [],
      "portSource": "AP2",
      "portTarget": "clathrin",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "is comprise",
          "categorical_verbs": "undefined undefined",
          "sentences": "BACKGROUND: AP2 is a clathrin-based endocytic adaptor complex comprising α, β2, μ2 and σ2 subunits.",
          "PMID": 28438500
        },
        "1": {
          "Verbs": "were immunolabele quantify",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Clathrin-coated structures (CCS) were immunolabeled and quantified in AP2αKD intestinal Caco2BBe (C2BBe) cells.",
          "PMID": 28438500
        },
        "2": {
          "Verbs": "interact modulates participate",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "CONCLUSION: AP2 α interacts with and modulates CFTR function in the intestine by  participating in clathrin assembly and recruitment of CFTR to CCS.",
          "PMID": 28438500
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "CALUclathrin",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CALU",
      "target": "clathrin",
      "bendPointPositions": [],
      "portSource": "CALU",
      "portTarget": "clathrin",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "polarize ( form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Endogenous, apical membrane CFTR in polarized human airway epithelial cells (Calu-3) formed a complex with myosin VI, the myosin VI adaptor protein Disabled 2 (Dab2), and clathrin.",
          "PMID": 15247260
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRclathrin",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "clathrin",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "clathrin",
      "review_status": "to_review",
      "width": 7,
      "references": {
        "0": {
          "Verbs": "express test analyze purify",
          "categorical_verbs": "activate undefined undefined undefined",
          "sentences": "To test this, we analyzed CFTR in clathrin-coated vesicles (CCV) purified from cells constitutively expressing CFTR at high levels.",
          "PMID": 7510684
        },
        "1": {
          "Verbs": "interact",
          "categorical_verbs": "neutral",
          "sentences": "Moreover, CFTR interacts with the plasma membrane  specific adaptor complex during endocytosis through clathrin-coated pits.",
          "PMID": 7510684
        },
        "2": {
          "Verbs": "found is purify isolate culture investigate enter",
          "categorical_verbs": "neutral undefined undefined undefined undefined undefined undefined",
          "sentences": "Since functionally active CFTR is found in purified clathrin-coated vesicles isolated from both cultured epithelial cells and intact  epithelial tissues, we investigated the molecular mechanisms whereby CFTR could enter such endocytic clathrin-coated vesicles.",
          "PMID": 10652362
        },
        "3": {
          "Verbs": "interact suggest contain facilitate",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Together, these data suggest that the carboxyl terminus of CFTR contains a tyrosine-based internalization signal that interacts with the endocytic adaptor complex AP-2 to  facilitate efficient entry of CFTR into clathrin-coated vesicles.",
          "PMID": 10652362
        },
        "4": {
          "Verbs": "mediate have shown is exclude",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "We have shown that such internalization is mediated solely by clathrin-coated pathways, and that other pathways, such as caveolae, exclude CFTR.",
          "PMID": 11845307
        },
        "5": {
          "Verbs": "elucidate was facilitate",
          "categorical_verbs": "activate undefined undefined",
          "sentences": "The goal of our current studies was to elucidate further the molecular mechanisms that facilitate entry of CFTR into endocytic clathrin-coated vesicles.",
          "PMID": 11845307
        },
        "6": {
          "Verbs": "mediate demonstrate is endocytose",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Thus, we demonstrate that CFTR is endocytosed via clathrin-coated vesicles, and that targeting of CFTR to these structures is mediated by binding of the AP-2 adaptor complex to the C-terminal domain of CFTR.",
          "PMID": 11845307
        },
        "7": {
          "Verbs": "polarize ( form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Endogenous, apical membrane CFTR in polarized human airway epithelial cells (Calu-3) formed a complex with myosin VI, the myosin VI adaptor protein Disabled 2 (Dab2), and clathrin.",
          "PMID": 15247260
        },
        "8": {
          "Verbs": "controll express controll is",
          "categorical_verbs": "neutral activate neutral undefined",
          "sentences": "Cystic fibrosis transmembrane conductance regulator (CFTR) is a cAMP-activated Cl(-) channel expressed in the apical plasma membrane of fluid-transporting epithelia, where the plasma membrane abundance of CFTR is in part controlled by clathrin-mediated endocytosis.",
          "PMID": 22399289
        },
        "9": {
          "Verbs": "interact facilitates remain is",
          "categorical_verbs": "neutral undefined undefined undefined",
          "sentences": "Although AP-2 interacts directly with CFTR in vitro and facilitates CFTR endocytosis in some cell types, it remains unknown whether it is critical for CFTR uptake into clathrin-coated vesicles (CCVs).",
          "PMID": 22399289
        },
        "10": {
          "Verbs": "interact modulates participate",
          "categorical_verbs": "neutral neutral undefined",
          "sentences": "CONCLUSION: AP2 α interacts with and modulates CFTR function in the intestine by  participating in clathrin assembly and recruitment of CFTR to CCS.",
          "PMID": 28438500
        },
        "11": {
          "Verbs": "undefined",
          "categorical_verbs": "undefined",
          "sentences": "Endocytic motifs in the carboxyl terminus of cystic fibrosis transmembrane\nconductance regulator (CFTR) direct internalization from the plasma membrane by\nclathrin-mediated endocytosis.",
          "PMID": 12839834
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "activate",
      "undefined",
      "neutral",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "neutral",
      "activate",
      "undefined",
      "neutral",
      "undefined",
      "neutral",
      "undefined",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "DAB2clathrin",
      "class": "stimulation",
      "cardinality": 0,
      "source": "DAB2",
      "target": "clathrin",
      "bendPointPositions": [],
      "portSource": "DAB2",
      "portTarget": "clathrin",
      "review_status": "to_review",
      "width": 3,
      "references": {
        "0": {
          "Verbs": "polarize ( form",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Endogenous, apical membrane CFTR in polarized human airway epithelial cells (Calu-3) formed a complex with myosin VI, the myosin VI adaptor protein Disabled 2 (Dab2), and clathrin.",
          "PMID": 15247260
        },
        "1": {
          "Verbs": "is direct recognize",
          "categorical_verbs": "undefined undefined undefined",
          "sentences": "Dab2 (Disabled 2) is the binding partner for myosin VI, clathrin, and alpha-AP-2 and directs endocytosis of low density lipoprotein receptor family members by recognizing a phosphotyrosine-binding domain.",
          "PMID": 20351096
        },
        "2": {
          "Verbs": "is contributes",
          "categorical_verbs": "undefined undefined",
          "sentences": "Disabled-2 (Dab2)  is a clathrin-associated sorting protein (CLASP) that contributes to clathrin recruitment, vesicle formation, and cargo selection.",
          "PMID": 22399289
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "undefined",
      "undefined",
      "undefined",
      "unverified"
    ]
  },
  {
    "data": {
      "id": "CFTRfolded CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "CFTR",
      "target": "folded CFTR",
      "bendPointPositions": [],
      "portSource": "CFTR",
      "portTarget": "folded CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "bind direct triggere was stabilize",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "Thus, rather than mainly directing anchoring of F508del-CFTR to the actin cytoskeleton, induction of ezrin activation by Rac1 signaling triggered a conformational change in NHERF1, which was then able to bind and stabilize misfolded CFTR at the plasma membrane.",
          "PMID": 25990958
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "verified"
    ]
  },
  {
    "data": {
      "id": "EZRfolded CFTR",
      "class": "stimulation",
      "cardinality": 0,
      "source": "EZR",
      "target": "folded CFTR",
      "bendPointPositions": [],
      "portSource": "EZR",
      "portTarget": "folded CFTR",
      "review_status": "to_review",
      "width": 1,
      "references": {
        "0": {
          "Verbs": "bind direct triggere was stabilize",
          "categorical_verbs": "neutral undefined undefined undefined undefined",
          "sentences": "Thus, rather than mainly directing anchoring of F508del-CFTR to the actin cytoskeleton, induction of ezrin activation by Rac1 signaling triggered a conformational change in NHERF1, which was then able to bind and stabilize misfolded CFTR at the plasma membrane.",
          "PMID": 25990958
        }
      }
    },
    "position": {
      "x": 0,
      "y": 0
    },
    "group": "edges",
    "classes": [
      "neutral",
      "undefined",
      "unverified"
    ]
  }
]