<!DOCTYPE html>
<html>
	<head>
        
		<meta name="layout" content="main"/>
		<title>CandActBase Cytoscape</title>
		<asset:javascript src="sbgn_JSON.js"/>

	</head>
	<body>
    <div id = "cy">
		
		
	</div>
	<div id = filter_parent>
		<div id = "filter">
			<button id = "filter_hide" class="btn btn-success">Hide filter</button>
			<button id = "filter_show" class="btn btn-success">Show filter</button>

			<div id = "filter_body">

				<div id = "legend">
					<table>
					
						<tr id = "legend_active_row">
							<td id = "legend_activating" class = "arrow">	<label for="check_active"> &#x279A; </label> </td>
							<td> 	<label for="check_active"> activating  </label></td>
							<td> <input type = "checkbox" id="check_active" checked/> </td>
						</tr>
						<tr id = "legend_inhibit_row">
							<td id = "legend_inhibiting" class = "arrow"> <label for="check_inhibit"> &#x279A; </label> </td>
							<td> <label for="check_inhibit">inhibiting </label> </td>
							<td> <input id="check_inhibit" type = "checkbox" checked/> </td>
						</tr>
						<tr id = "legend_neutral_row">
							<td id = "legend_neutral" class = "arrow"> <label for="check_neutral"> &#x279A; </label> </td>
							<td> <label for="check_neutral">neutral  </label></td>
							<td> <input id="check_neutral" type = "checkbox" checked/> </td>
						</tr>
						<tr id = "legend_undefined_row">
							<td id = "legend_undefined" class = "arrow"> <label for="check_undefined"> &#x279A; </label></td>
							<td><label for="check_undefined"> undefined  </label></td>
							<td> <input id="check_undefined" type = "checkbox" checked/> </td>
						</tr>
						<tr id = "legend_accepted_row">
							<td id = "legend_accepted" class = "arrow"> <label for="check_accepted"> &#x2713; </label></td>
							<td><label for="check_accepted"> accepted edges </label></td>
							<td> <input id="check_accepted" type = "checkbox" checked/> </td>
						</tr>
						<tr id = "legend_declined_row">
							<td id = "legend_declined" class = "arrow"> <label for="check_declined"> &#x2715; </label></td>
							<td><label for="check_declined"> declined edges </label></td>
							<td> <input id="check_declined" type = "checkbox" checked/> </td>
						</tr>
						<tr id = "legend_further_inspection_row">
							<td id = "legend_further_inspection" class = "arrow"> <label for="check_further_inspection"> &#128270; </label></td>
							<td><label for="check_further_inspection"> further inspection </label></td>
							<td> <input id="check_further_inspection" type = "checkbox" checked/> </td>
						</tr>
						<tr id = "legend_to_review_row">
							<td id = "legend_to_review" class = "arrow"> <label for="check_to_review"> &#9887; </label></td>
							<td><label for="check_to_review"> to review </label></td>
							<td> <input id="check_to_review" type = "checkbox" checked/> </td>
						</tr>
						<tr id = "legend_coherent_row">
							<td id = "legend_coherent" class = "arrow"> <label for="check_coherent">&#9878; </label></td>
							<td><label for="check_coherent"> only coherent </label></td>
							<td> <input id="check_coherent" type = "checkbox"/> </td>
						</tr>
					</table>
				</div>

				<div id= "edge_filter">
				
					<input type="range" class="custom-range" name="edge_reference_range" min="1" max="10" value = "1" ><output>1</output>

				</div>
				<button id = "reset_edges" class="btn btn-danger"> Reset filters </button>
			</div>
		</div>


		<div class="btn-group btn-group-toggle" id= "timeline_buttons">
			<label class="btn btn-outline-dark">
				<input type="radio"  id="button_1990" autocomplete="off"> 1990
			</label>
			<label class="btn btn-outline-dark" >
				<input type="radio"  id="button_1995" autocomplete="off"> 1995
			</label>
			<label class="btn btn-outline-dark">
				<input type="radio" id="button_2000" autocomplete="off"> 2000
			</label>
			<label class="btn btn-outline-dark">
				<input type="radio"id="button_2005" autocomplete="off"> 2005
			</label>
			<label class="btn btn-outline-dark">
				<input type="radio"  id="button_2010" autocomplete="off"> 2010
			</label>
			<label class="btn btn-outline-dark">
				<input type="radio"  id="button_2015" autocomplete="off"> 2015
			</label>
			<label class="btn btn-outline-dark">
				<input type="radio"  id="button_2020" autocomplete="off"> 2020
			</label>
			<label class="btn btn-outline-dark active">
				<input type="radio"  id="button_complete" autocomplete="off"> recreation
			</label>
		</div>
	</div>
</div>
<div id = "view_div">
	
	<!-- Button trigger modal -->



	<div id = "view">
		<div id = node_view>
			<div id = "start_node_div" class = "node_div">
			</div> 	
			<div id = "edge_div" ><b>&#x279D;</b>
			</div> 	
			<div id = "target_node_div" class = "node_div">
			</div>
			<div id = "status_div" >
				<div id = "review_status"></div>
				<div id = "info_status"></div>			
			</div>
			<div id = "info_div"></div> 			
		</div>
	</div>
	<div id = "iteration_div">
		<button id = "next_edge" class="btn btn-secondary">Next edge to review >></button> 
		<div class="btn-group" role="group" aria-label="Basic example">
			<button id = "decline_edge" class ="btn btn-danger" disabled> Decline interaction </button>
			<button id = "further_inspetion_edge" class ="btn btn-warning" disabled> Further inspection needed </button>
			<button id = "accept_edge" class ="btn btn-success" disabled> Accept interaction </button>
		</div>
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#download_data" id = "view_annotated" >
  			Download data
		</button>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="download_data" tabindex="-1" role="dialog" aria-labelledby="download_data_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="download_data_label">Download backend data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Download the altered background dataset, displayed as the disease map. You can choose between our own JSON format, that can be displayed in with this disease map viewer and a smaller CSV file to view the review progress with the belonging literature. 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" style = "float:left" id="download_JSON">Download JSON</button>
        <button type="button" class="btn btn-success" style = "float:left" id="download_CSV">Download CSV</button>
      </div>
    </div>
  </div>
</div>
	
	</body>
</html>

